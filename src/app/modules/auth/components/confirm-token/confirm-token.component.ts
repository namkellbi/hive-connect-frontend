import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { STORAGE_KEY } from '@constant/settings';
import { LoginUserInfoResponse, ResponseStatus } from '@interfaces/response';
import { ICandidateProfile, ROLE } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { AuthService } from '@services/auth.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-confirm-token',
  templateUrl: './confirm-token.component.html',
  styleUrls: ['./confirm-token.component.scss']
})
export class ConfirmTokenComponent implements OnInit, OnDestroy {
  readonly COUNTDOWN_TIME = 30;
  count = this.COUNTDOWN_TIME;
  interval: any;
  loading = true;
  verified = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private helperService: HelperService,
    private router: Router,
    private authService: AuthService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    const { token } = this.activatedRoute.snapshot.queryParams;
    this.confirmEmail(token);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    clearInterval(this.interval);
  }

  startCountDown() {
    this.count = this.COUNTDOWN_TIME;
    this.interval = setInterval(() => {
      if (this.count === 0) {
        clearInterval(this.interval);
        this.router.navigate(['/home']);
      }
      this.count -= 1;
    }, 1000);
  }

  confirmEmail(token: string) {
    this.loading = true;
    this.apiService.confirmEmailToken(token)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            const { data, token } = response;
            if (data.roleId === ROLE.recruiter) {
              this.storeService.setKey(STORAGE_KEY.TOKEN_TEMP, token);
              this.router.navigate(['/auth', 'register'], {
                queryParams: {
                  step: 4,
                  id: data.id
                }
              });
            } else {
              this.authService.loginSuccess({ ...response, data: { user: data } as LoginUserInfoResponse });
              this.startCountDown();
              this.verified = true;
            }
            this.helperService.showSuccess('', 'Xác thực thành công');
          } else {
            this.helperService.showError('', 'Xác thực thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get failedMessage() {
    return 'Xác minh thất bại';
  }
}

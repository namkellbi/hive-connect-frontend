import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { STORAGE_KEY } from '@constant/settings';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterProfile } from '@interfaces/user';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { finalize, Observable, switchMap } from 'rxjs';

@Component({
  selector: 'hc-register-step4',
  templateUrl: './register-step4.component.html',
  styleUrls: ['./register-step4.component.scss']
})
export class RegisterStep4Component implements OnInit {
  profile$ = new Observable<IRecruiterProfile>();
  constructor(
    private storeService: StoreService,
    private userService: UserService,
    private router: Router,
    private helperService: HelperService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const { id } = this.activatedRoute.snapshot.queryParams;
    if (!id) {
      this.helperService.showError('', 'Không tìm thấy thông tin tài khoản');
      this.router.navigate(['auth', 'login']);
    }
    this.profile$ = this.userService.fetchRecruiterProfile(id)
      .pipe(
        switchMap(({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.storeService.recruiterProfile.next(data);
          }
          return this.storeService.observerRecruiterProfile();
        })
      );
  }
  joinCompanyFinish($event: any) {
    if ($event) {
      const { id } = this.activatedRoute.snapshot.queryParams;
      this.userService.fetchUserInfo(id)
        .pipe(
          switchMap(({ data, status }) => {
            this.storeService.updateUserInfo(data);
            this.storeService.setAccessToken(this.storeService.getKey(STORAGE_KEY.TOKEN_TEMP));
            return this.userService.fetchRecruiterProfile(data.id, true);
          })
        )
        .subscribe({
          next: ({ status, data, message }) => {
            if (status === ResponseStatus.success) {
              this.storeService.recruiterProfile.next(data);
              this.router.navigate(['/recruiter', 'account', 'company']);
            } else {
              this.helperService.showError('', message || 'Lấy thông tin tài khoản thất bại. Xin hãy đăng nhập lại');
            }
          },
          error: error => this.helperService.callApiFailedHandler(error)
        })
    }
  }
}

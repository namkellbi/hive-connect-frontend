import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ROLE } from '@interfaces/user';

@Component({
  selector: 'hc-register-step1',
  templateUrl: './register-step1.component.html',
  styleUrls: ['./register-step1.component.scss']
})
export class RegisterStep1Component implements OnInit {
  @Output() onNextStep = new EventEmitter();
  ROLE = ROLE;
  constructor() { }

  ngOnInit(): void {
  }

}

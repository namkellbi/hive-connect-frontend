import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { STORAGE_KEY } from '@constant/settings';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';

@Component({
  selector: 'hc-register-step3',
  templateUrl: './register-step3.component.html',
  styleUrls: ['./register-step3.component.scss']
})
export class RegisterStep3Component implements OnInit {
  @Output() onNextStep = new EventEmitter();
  type: 'system' | 'social' = 'system';
  loading = false;
  readonly COUNTDOWN_TIME = 30;
  count = this.COUNTDOWN_TIME;
  interval: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private storeService: StoreService,
    private helperService: HelperService,
    private router: Router
  ) {
    ({ type: this.type } = this.activatedRoute.snapshot.queryParams);
  }

  ngOnInit(): void {
    this.startCountDown();
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    clearInterval(this.interval);
  }
  startCountDown() {
    this.count = this.COUNTDOWN_TIME;
    this.interval = setInterval(() => {
      if (this.count === 0) {
        clearInterval(this.interval);
        return;
      }
      this.count -= 1;
    }, 1000);
  }

  resendEmail() {
    const username = this.storeService.getKey(STORAGE_KEY.EMAIL_TEMP);
    console.log(username)
    this.apiService.resendEmailVerify(username).subscribe({
      next: response => {
        if (response.status === ResponseStatus.success) {
          this.helperService.showSuccess('', 'Gửi email thành công');
          this.startCountDown();
        } else {
          this.helperService.showError('', response.message || 'Gửi email thất bại')
        }
      },
      error: error => this.helperService.callApiFailedHandler(error)
    })
  }
}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { getFullNameRegex, getRegexEmail, getRegexPassword } from '@constant/form';
import { STORAGE_KEY } from '@constant/settings';
import { LoginResponse, LoginUserInfoResponse, ResponseStatus } from '@interfaces/response';
import { ICandidateProfile, IRecruiterProfile } from '@interfaces/user';
import { AuthService } from '@services/auth.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';
import { checkPassword } from 'src/app/core/utils/form';


@Component({
  selector: 'hc-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  @Output() onSubmitSuccess = new EventEmitter<LoginResponse>();
  form!: FormGroup;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private helperService: HelperService,
    private router: Router,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(getRegexEmail()), Validators.maxLength(255)]],
      // username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(getRegexPassword())]],
      fullName: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(getFullNameRegex())]],
      matchPassword: ['', [Validators.required, Validators.maxLength(50)]]
    }, { validators: checkPassword });
  }
  submit() {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    const { role } = this.activatedRoute.snapshot.queryParams;
    const { matchPassword, ...form } = this.form.value;
    this.loading = true;
    this.authService.register({ roleId: role, confirmPassword: matchPassword, ...form })
      .pipe(finalize(() => this.loading = false))
      .subscribe({
        next: (response) => {
          if (response.status === ResponseStatus.success) {
            const { data, token, ...remaining } = response;
            data.verifiedEmail = false;
            this.storeService.setKey(STORAGE_KEY.EMAIL_TEMP, response.data.email);
            this.onSubmitSuccess.emit({
              token, ...remaining,
              data: {
                user: data,
                candidate: {} as ICandidateProfile,
                recruiter: {} as IRecruiterProfile,
              } as LoginUserInfoResponse
            });
            // let url = ['/'];
            // if (response.data.roleId === ROLE.recruiter) {
            //   url = ['/recruiter', 'jobs'];
            // }
            // this.router.navigate(url);
            this.helperService.showSuccess('', 'Đăng ký thành công');
          } else {
            this.helperService.showError('', response.message || 'Đăng ký thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error, 'Đăng ký thất bại')
      })
  }
}

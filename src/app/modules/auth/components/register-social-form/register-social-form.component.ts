import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { STORAGE_KEY } from '@constant/settings';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';
import { checkPassword } from 'src/app/core/utils/form';
import { parseJSON } from 'src/app/core/utils/resourceHandle';

interface GoogleAccountInfo {
  email: string;
  name: string;
  picture: string;
}
@Component({
  selector: 'hc-register-social-form',
  templateUrl: './register-social-form.component.html',
  styleUrls: ['./register-social-form.component.scss']
})
export class RegisterSocialFormComponent implements OnInit {
  @Output() onSubmitSuccess = new EventEmitter();
  form!: FormGroup;
  loading = false;
  accountInfo = {} as GoogleAccountInfo;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const accountInfo = parseJSON(this.storeService.getKey(STORAGE_KEY.GOOGLE_INFO_TEMP), {} as GoogleAccountInfo);
    if (!accountInfo || !accountInfo.email) {
      this.helperService.showError('', 'Không tìm thấy thông tin tài khoản, vui lòng thực hiện lại');
    }
    this.accountInfo = accountInfo;
    this.form = this.fb.group({
      // password: ['', [Validators.required]],
      // matchPassword: ['', [Validators.required]],
      username: [accountInfo.email.split('@')[0], [Validators.required, Validators.maxLength(50)]],
      name: [accountInfo.name, [Validators.maxLength(50)]],
      email: [accountInfo.email, [Validators.maxLength(50)]],
      picture: [accountInfo.picture, [Validators.maxLength(500)]],
      roleId: [this.activatedRoute.snapshot.queryParams['role']]
    }, { validators: checkPassword });
    this.submit();
  }

  submit() {
    // this.helperService.validateFormField(this.form);
    // if (this.form.invalid) return;
    this.loading = true;
    const { matchPassword, ...request } = this.form.value;
    this.apiService.loginGoogle({
      ...request,
      confirmPassword: matchPassword
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          this.helperService.callApiResponseHandler(response);
          this.storeService.setKey(STORAGE_KEY.EMAIL_TEMP, this.form.value.email)
          this.onSubmitSuccess.emit(response)
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}

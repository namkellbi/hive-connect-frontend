import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSocialFormComponent } from './register-social-form.component';

describe('RegisterSocialFormComponent', () => {
  let component: RegisterSocialFormComponent;
  let fixture: ComponentFixture<RegisterSocialFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterSocialFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSocialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

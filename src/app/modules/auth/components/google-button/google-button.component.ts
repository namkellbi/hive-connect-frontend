import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, NgZone, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { STORAGE_KEY } from '@constant/settings';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize, lastValueFrom } from 'rxjs';
import { decodeURIGoogleCredential } from 'src/app/core/utils/resourceHandle';
import { environment } from 'src/environments/environment';

declare var google: any;
@Component({
  selector: 'hc-google-button',
  templateUrl: './google-button.component.html',
  styleUrls: ['./google-button.component.scss']
})
export class GoogleButtonComponent implements OnInit, AfterViewInit {
  @Output() loadingChange = new EventEmitter();
  @Output() loginSuccess = new EventEmitter();
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService,
    private router: Router,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    google.accounts.id.initialize({
      client_id: environment.GOOGLE_CLIENT_ID,
      callback: (response: any) => this.handleGoogleSignIn(response),
      select_by: 'user'
    });
    google.accounts.id.renderButton(
      document.getElementById("google-btn"),
      { size: "large", type: "standard", theme: 'filled_blue', text: 'signin', locale: 'vi_VN' }  // customization attributes,
    );
  }

  async handleGoogleSignIn(response: any) {
    if (response.credential) {
      const { email, name, picture } = decodeURIGoogleCredential(response.credential);
      this.ngZone.run(async () => {
        this.loadingChange.emit(true);
        try {
          if (await this.isLogin(email)) {
            const response = await lastValueFrom(this.apiService.loginGoogle({ email, name, picture }));
            const result = this.helperService.callApiResponseHandler(response);
            this.loginSuccess.emit(response);
            this.loadingChange.emit(false)
            return;
          }
          this.storeService.setKey(STORAGE_KEY.GOOGLE_INFO_TEMP, JSON.stringify({ email, name, picture }));
          await this.router.navigate(['/auth', 'register'], {
            queryParams: {
              step: 1,
              type: 'social'
            }
          });
          this.loadingChange.emit(false)
        } catch (error: any) {
          this.helperService.callApiFailedHandler(error);
        }
        this.loadingChange.emit(false)
      })
    }
  }

  async isLogin(email: string) {
    const response = await lastValueFrom(this.apiService.checkExistEmail(email));
    if (response.status !== ResponseStatus.success) {
      throw new Error(response.message || 'Kiểm tra email bị lỗi!');
    }
    return response.data;
  }
}

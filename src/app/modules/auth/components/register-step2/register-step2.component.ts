import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginResponse } from '@interfaces/response';
import { AuthService } from '@services/auth.service';
import { HelperService } from '@services/helper.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-register-step2',
  templateUrl: './register-step2.component.html',
  styleUrls: ['./register-step2.component.scss']
})
export class RegisterStep2Component implements OnInit {
  @Output() onNextStep = new EventEmitter();
  loading = false;
  form!: FormGroup;
  destroy$ = new Subject();
  type: 'system' | 'social' = 'system';
  constructor(
    private fb: FormBuilder,
    private helperService: HelperService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(params => this.type = params['type'] || 'system');
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  registerSuccess($event: LoginResponse) {
    // this.authService.loginSuccess($event);
    // this.router.navigate(['/home']);
    this.onNextStep.emit({ step: 3, data: $event });
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './page/login/login.component';
import { RegisterComponent } from './page/register/register.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { RegisterStep1Component } from './components/register-step1/register-step1.component';
import { RegisterStep2Component } from './components/register-step2/register-step2.component';
import { RegisterStep3Component } from './components/register-step3/register-step3.component';
import { GoogleButtonComponent } from './components/google-button/google-button.component';
import { LinkedButtonComponent } from './components/linked-button/linked-button.component';
import { ComponentsModule } from '@components/components.module';
import { RegisterSocialFormComponent } from './components/register-social-form/register-social-form.component';
import { ConfirmTokenComponent } from './components/confirm-token/confirm-token.component';
import { RegisterStep4Component } from './components/register-step4/register-step4.component';
import { ForgotPasswordComponent } from './page/forgot-password/forgot-password.component';
import { FormModule } from '@components/form/form.module';
import { ModalModule } from '@components/modal/modal.module';



@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AuthComponent,
    RegisterFormComponent,
    RegisterStep1Component,
    RegisterStep2Component,
    RegisterStep3Component,
    GoogleButtonComponent,
    LinkedButtonComponent,
    RegisterSocialFormComponent,
    ConfirmTokenComponent,
    RegisterStep4Component,
    ForgotPasswordComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComponentsModule,
    FormModule,
    ModalModule,
    RouterModule.forChild(
      [
        {
          path: '', component: AuthComponent, children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'confirm-token', component: ConfirmTokenComponent },
            { path: 'forgot-password', component: ForgotPasswordComponent }
          ]
        },
      ]
    ),
  ]
})
export class AuthModule { }

import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { getRegexEmail, getRegexPassword } from '@constant/form';
import { ILoginRequest } from '@interfaces/request';
import { LoginResponse, ResponseStatus } from '@interfaces/response';
import { ROLE } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { AuthService } from '@services/auth.service';
import { HelperService } from '@services/helper.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private helperService: HelperService,
    private authService: AuthService,
    private router: Router,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(getRegexEmail()), Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(getRegexPassword())]]
    });
  }

  onSubmit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) {
      return;
    }

    const { email, password } = this.form.value;
    const params: ILoginRequest = {
      password,
      username: email
    }
    this.loading = true;
    this.apiService.login(params)
      .pipe(
        finalize(() => this.loading = false),
      )
      .subscribe({
        next: (data) => {
          if (data.status === ResponseStatus.success) {
            this.loginHandle(data);
          } else {
            this.helperService.showError('', data.message);
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
  }

  async loginHandle(data: LoginResponse) {
    if (!this.authService.validateLoginAccount(data)) return;
    await this.authService.loginSuccess(data);
    let url = ['/'];
    if (data.data.user.roleId === ROLE.recruiter) {
      url = ['/recruiter', 'jobs'];
    }
    if (data.data.user.roleId === ROLE.collaborator) {
      url = ['/collaborator'];
    }
    this.router.navigate(url);
  }
}

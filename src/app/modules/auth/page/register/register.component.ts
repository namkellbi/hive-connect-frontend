import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, Subscription, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  step: number = 1;
  destroy$ = new Subject();
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activatedRoute.queryParams.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.step = +params['step'] || 1;
    });
  }

  ngOnInit(): void {
  }

  onNextStep($event: { step: 1 | 2 | 3, data: any }) {
    const prevParams = this.activatedRoute.snapshot.queryParams;
    const queryParams: Params = {
      ...prevParams,
      step: $event.step
    }
    if ($event.data.role) {
      queryParams['role'] = $event.data.role;
    }
    if ($event.data.type) {
      queryParams['type'] = $event.data.type;
    }
    this.router.navigate([], {
      queryParams,
      relativeTo: this.activatedRoute,
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { getRegexEmail, getRegexPassword } from '@constant/form';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize, Subject, takeUntil } from 'rxjs';
import { checkPassword } from 'src/app/core/utils/form';

@Component({
  selector: 'hc-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  form!: FormGroup;
  isSend = true;
  loading = false;
  message = '';
  destroy$ = new Subject();
  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private helperService: HelperService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(params => {
        if (params['token']) {
          this.initFormChange(params['token']);
          this.isSend = false;
        } else {
          this.initFormSend();
          this.isSend = true;
        }
      })
  }

  initFormSend() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(getRegexEmail()), Validators.maxLength(50)]]
    })
    this.form.get('email')?.valueChanges.subscribe(() => {
      console.log(this.form.get('email')?.status)
    })
  }
  initFormChange(token: string) {
    this.form = this.fb.group({
      password: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(getRegexPassword())]],
      matchPassword: ['', [Validators.required, Validators.maxLength(50)]],
      resetPasswordToken: [token]
    }, { validators: checkPassword })
  }

  sendMail() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.apiService.sendEmailReset(this.form.value.email)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          this.helperService.callApiResponseHandler(response);
          this.message = response.message;
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  resetPassword() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const { matchPassword, password, resetPasswordToken } = this.form.value;
    this.apiService.resetPassword({
      resetPasswordToken,
      newPassword: password,
      confirmPassword: matchPassword
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          this.helperService.callApiResponseHandler(response);
          this.message = response.message;
          this.router.navigate(['/auth', 'login']);
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}

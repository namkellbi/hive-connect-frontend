import { Component, OnInit } from '@angular/core';
import { SearchService } from '@services/search.service';

@Component({
  selector: 'hc-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(
    private searchService: SearchService,
  ) { }

  ngOnInit(): void {
    this.searchService.fetchCompany();
    this.searchService.getAllField();
    this.searchService.getAllCountry();
  }

}

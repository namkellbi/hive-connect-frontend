import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobRoutingModule } from './job-routing.module';
import { JobLayoutComponent } from './job-layout.component';
import { JobDetailHeaderComponent } from './components/job-detail-header/job-detail-header.component';
import { SharedModule } from '../../shared/shared.module';
import { ComponentsModule } from '@components/components.module';
import { JobSameCompanyComponent } from './components/job-same-company/job-same-company.component';
import { JobSimilarComponent } from './components/job-similar/job-similar.component';
import { ApplyModalComponent } from './components/apply-modal/apply-modal.component';
import { JobInfoDetailComponent } from './components/job-info-detail/job-info-detail.component';
import { JobInfoCompanyComponent } from './components/job-info-company/job-info-company.component';
import { HomeModule } from '@pages/home/home.module';

@NgModule({
  declarations: [
    JobLayoutComponent,
    JobDetailHeaderComponent,
    JobSameCompanyComponent,
    JobSimilarComponent,
    ApplyModalComponent,
    JobInfoDetailComponent,
    JobInfoCompanyComponent
  ],
  imports: [
    CommonModule,
    JobRoutingModule,
    SharedModule,
    ComponentsModule,
    HomeModule
  ]
})
export class JobModule { }

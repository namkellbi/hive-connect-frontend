import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReportJobComponent } from '@components/modal/report-job/report-job.component';
import { MAP_JOB_STYLE_NAME } from '@constant/form';
import { JobDetail } from '@interfaces/job';
import { BannerPosition } from '@interfaces/payment';
import { ResponseStatus } from '@interfaces/response';
import { ICandidateProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject, takeUntil, finalize, combineLatest } from 'rxjs';
import { JobDetailHeaderComponent } from './components/job-detail-header/job-detail-header.component';

@Component({
  selector: 'hc-job-layout',
  templateUrl: './job-layout.component.html',
  styleUrls: ['./job-layout.component.scss']
})
export class JobLayoutComponent implements OnInit, OnDestroy {
  @ViewChild(JobDetailHeaderComponent) header!: JobDetailHeaderComponent;
  jobDetail = {} as JobDetail;
  loading = false;
  destroy$ = new Subject();
  profile = {} as ICandidateProfile;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private storeService: StoreService,
    private modalService: BsModalService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    combineLatest([
      this.activatedRoute.params,
      this.storeService.observerCandidateProfile()
    ]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(([params, profile]) => {
      if (params['id']) {
        this.profile = profile;
        this.getJobDetail(+params['id'], profile.id);
      }
    });
    document.body.classList.add('bg-grey');
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
    document.body.classList.remove('bg-grey');
  }

  getJobDetail(id: number, candidateId: number = -1) {
    this.loading = true;
    this.apiService.getJobDetail(id, candidateId)
      .pipe(
        finalize(() => this.loading = false),

      )
      .subscribe({
        next: (response) => {
          this.jobDetail = this.helperService.callApiResponseHandler(response);
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME;
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }
  apply() {
    this.header.applyJob();
  }
  cancelApply() {
    this.header.confirmCancelApplyJob();
  }
  get statusApply() {
    return this.header?.statusApply;
  }
  follow() {
    this.header.followJob();
  }
  unFollow() {
    this.header.unFollow();
  }

  get btnFollowTitle() {
    return this.header?.btnFollowTitle;
  }

  get position() {
    return BannerPosition;
  }
}

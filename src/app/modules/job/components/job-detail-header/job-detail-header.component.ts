import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { JobApplyState, JobDetail, JobFollowType } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ROLE } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import moment from 'moment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { finalize, Subject, takeUntil } from 'rxjs';
import { ApplyModalComponent } from '../apply-modal/apply-modal.component';

@Component({
  selector: 'hc-job-detail-header',
  templateUrl: './job-detail-header.component.html',
  styleUrls: ['./job-detail-header.component.scss']
})
export class JobDetailHeaderComponent implements OnInit {
  @Input() detail = {} as JobDetail;
  @Input() isApply = false;
  @ViewChild('confirmCancelApply') tempRef!: TemplateRef<any>;
  @Output() onApplyJob = new EventEmitter();
  isCandidate = false;
  candidateId = 0;
  destroy$ = new Subject();
  loadingFollow = false;
  loading = false;
  modalRef!: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private storeService: StoreService,
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.storeService.observerUser()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(info => {
        this.isCandidate = info != null && info.roleId === ROLE.candidate;
      })
  }

  applyJob() {
    if (moment().isAfter(moment(this.detail.endDate))) {
      this.helperService.showError('', 'Đã quá hạn nộp hồ sơ ứng tuyển!');
      return;
    }
    const modal = this.modalService.show(ApplyModalComponent, {
      initialState: {
        jobId: this.detail.id || this.detail.jobId
      }
    });
    modal.content?.close.subscribe((data) => {
      modal.hide();
      this.isApply = Boolean(data);
      this.detail.approvalStatus = JobApplyState.pending;
      this.onApplyJob.emit(Boolean(data));
    });
  }

  confirmCancelApplyJob() {
    this.modalRef = this.modalService.show(this.tempRef);
  }

  closeModalConfirm() {
    this.modalRef.hide();
  }

  cancelApplyJob() {
    const profile = this.storeService.getCandidateProfile();
    const request = {
      jobId: this.detail.id || this.detail.jobId,
      candidateId: profile.id,
      cvUrl: ''
    }
    this.loading = true;
    this.apiService.applyJob(request)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.modalRef.hide();
        }),
      )
      .subscribe({
        next: ({ status, message }) => {
          if (status === ResponseStatus.success) {
            this.isApply = false;
            this.onApplyJob.emit(this.isApply);
            this.helperService.showSuccess('', 'Hủy ứng tuyển thành công');
          } else {
            this.helperService.showError('', message || 'Hủy ứng tuyển thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  followJob() {
    const { id } = this.storeService.getCandidateProfile();
    this.loadingFollow = true;
    this.apiService.follow({ followedId: this.detail.id || this.detail.jobId, followerId: id, type: JobFollowType.job })
      .pipe(
        finalize(() => this.loadingFollow = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.detail.following = true;
            this.helperService.showSuccess('', 'Theo dõi công việc thành công');
          } else {
            this.helperService.showError('', 'Theo dõi thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  unFollow() {
    const { id } = this.storeService.getCandidateProfile();
    this.loadingFollow = true;
    this.apiService.follow({ followedId: this.detail.id, followerId: id, type: JobFollowType.job })
      .pipe(
        finalize(() => this.loadingFollow = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.detail.following = false;
            this.helperService.showSuccess('', 'Bỏ theo dõi công việc thành công');
          } else {
            this.helperService.showError('', 'Theo dõi thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get statusApply() {
    return {
      [JobApplyState.approve]: 'Đơn của bạn đã được chấp thuận',
      [JobApplyState.reject]: 'Đơn của bạn đã bị từ chối',
      [JobApplyState.pending]: 'Đơn của bạn đang được xử lý'
    }
  }

  get btnFollowTitle() {
    return this.detail.following ? 'Bỏ theo dõi' : 'Theo dõi công việc';
  }
  get endDateStatus() {
    return moment().isAfter(moment(this.detail.endDate)) ? '(Hết hạn)' : ''
  }

  get confirmApplyMessage() {
    return this.detail.approvalStatus === JobApplyState.approve ? 'Hiện đơn của bạn đã được chấp thuận, bạn có chắc muốn hủy?' : 'Bạn có chắc muốn hủy?';
  }

  get isShowCancelApplyButton() {
    return this.detail.approvalStatus !== JobApplyState.reject;
  }
}

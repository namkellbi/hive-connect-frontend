import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobDetail } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { filter, finalize, Subject, switchMap, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-job-similar',
  templateUrl: './job-similar.component.html',
  styleUrls: ['./job-similar.component.scss']
})
export class JobSimilarComponent implements OnInit {
  jobs: JobDetail[][] = [];
  destroy$ = new Subject();
  loading: boolean = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$),
        filter(params => !!params['id']),
        switchMap(params => {
          this.loading = true;
          return this.apiService.getSameJobs(+params['id'])
            .pipe(
              finalize(() => this.loading = false)
            )
        })
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            const page = Math.ceil(data.length / 4);
            Array.from({ length: page }).forEach((_, index) => this.jobs[index] = [...data.splice(0, 4)])
            console.log(this.jobs);
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }
  slideChange($event: number | number[] | void) {
    console.log('slide change', $event);
  }
}

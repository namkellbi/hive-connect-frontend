import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploadType } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ICandidateProfile, ROLE } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { switchMap, filter, Observable, Subject, takeUntil, map, finalize } from 'rxjs';
import { FileFinishPendingType, FileFinishUploadType } from 'src/app/shared/directives/file-handle.directive';

@Component({
  selector: 'hc-apply-modal',
  templateUrl: './apply-modal.component.html',
  styleUrls: ['./apply-modal.component.scss']
})
export class ApplyModalComponent implements OnInit, OnDestroy {
  @Input() jobId = 0;
  @Output() close = new EventEmitter();
  @ViewChild('confirmCreateCv') confirmCreateCvRef!: TemplateRef<any>;
  profile$ = new Observable<ICandidateProfile>();
  loading = false;
  destroy$ = new Subject();
  file = {} as FileFinishPendingType;
  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private userService: UserService,
    private helperService: HelperService,
    private router: Router,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.profile$ = this.storeService.observerUser()
      .pipe(
        takeUntil(this.destroy$),
        filter(data => !!data && data.roleId === ROLE.candidate),
        switchMap(data => this.userService.fetchCandidateProfile()),
        filter(({ status }) => status === ResponseStatus.success),
        map(({ data }) => data)
      )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  onClose(data?: any) {
    this.close.emit(data);
  }

  confirmCreateCv() {
    this.modalService.show(this.confirmCreateCvRef, {
      class: 'modal-dialog-centered'
    });
  }

  async apply({ id }: ICandidateProfile, uploadCv = false) {
    if (uploadCv && !this.file.file) {
      this.helperService.showError('', 'Bạn phải chọn cv để tiếp tục');
      return;
    }
    if (!this.storeService.getCV()?.id && !uploadCv) {
      this.onClose();
      this.confirmCreateCv();
      return;
    }
    const request = {
      jobId: this.jobId,
      candidateId: id,
      cvUrl: ''
    }
    this.loading = true;
    try {
      if (uploadCv && this.file.file) {
        const uploadFile = await this.helperService.uploadImage(this.file.file, FileUploadType.cv);
        request.cvUrl = uploadFile.url;
      }
      this.loading = true;
      this.apiService.applyJob(request)
        .pipe(
          finalize(() => this.loading = false),
        )
        .subscribe({
          next: ({ status, message }) => {
            if (status === ResponseStatus.success) {
              this.helperService.showSuccess('', 'Ứng tuyển thành công');
              this.onClose(true);
            } else {
              this.helperService.showError('', message || 'Ứng tuyển thất bại');
            }
          },
          error: error => this.helperService.callApiFailedHandler(error)
        })
    } catch (error: any) {
      this.helperService.showError('', error?.message || 'Ứng tuyển thất bại');
      this.loading = false;
    }
  }

  fileReadFinishHandle($event: FileFinishPendingType[]) {
    if ($event && $event[0]) {
      this.file = $event[0]
    }
  }

  get fileUploadType() {
    return FileUploadType;
  }
}

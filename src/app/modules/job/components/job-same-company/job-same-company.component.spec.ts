import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobSameCompanyComponent } from './job-same-company.component';

describe('JobSameCompanyComponent', () => {
  let component: JobSameCompanyComponent;
  let fixture: ComponentFixture<JobSameCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobSameCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobSameCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

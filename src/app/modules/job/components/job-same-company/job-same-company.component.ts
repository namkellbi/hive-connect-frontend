import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Job } from '@interfaces/job';
import { IRequestPagination } from '@interfaces/request';
import { IResponsePagination, ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { combineLatest, filter, finalize, startWith, Subject, switchMap, takeUntil } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-job-same-company',
  templateUrl: './job-same-company.component.html',
  styleUrls: ['./job-same-company.component.scss']
})
export class JobSameCompanyComponent implements OnInit {
  @Input() companyId = 0;
  jobs: Job[] = [];
  pagination$ = new Subject<IRequestPagination>();
  total = 0;
  page = 1;
  destroy$ = new Subject();
  loading = false;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    combineLatest([
      this.activatedRoute.params,
      this.pagination$.asObservable().pipe(startWith({ ...DEFAULT_PAGINATION, pageSize: 8 }))
    ])
      .pipe(
        takeUntil(this.destroy$),
        filter(([params, pagination]) => !!params['id'] && !isNaN(+params['id'])),
        switchMap(([params, pagination]) => {
          this.loading = true;
          this.page = pagination.pageNo;
          return this.getJob(+params['id'], pagination)
            .pipe(finalize(() => this.loading = false))
        })
      ).subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.jobs.push(...data.data);
            this.total = data.pagination.totalRecords;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  getJob(id: number, pagination: IRequestPagination) {
    return this.apiService.getJobByCompany(id, pagination);
  }
  next() {
    this.pagination$.next({
      pageNo: this.page + 1,
      pageSize: 8
    })
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobInfoCompanyComponent } from './job-info-company.component';

describe('JobInfoCompanyComponent', () => {
  let component: JobInfoCompanyComponent;
  let fixture: ComponentFixture<JobInfoCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobInfoCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobInfoCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { JobDetail } from '@interfaces/job';

@Component({
  selector: 'hc-job-info-company',
  templateUrl: './job-info-company.component.html',
  styleUrls: ['./job-info-company.component.scss']
})
export class JobInfoCompanyComponent implements OnInit {
  @Input() jobDetail = {} as JobDetail;
  @Input() loading = false;
  constructor() { }

  ngOnInit(): void {
  }

}

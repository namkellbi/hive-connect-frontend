import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ReportJobComponent } from '@components/modal/report-job/report-job.component';
import { MAP_JOB_STYLE_NAME } from '@constant/form';
import { JobApplyState, JobDetail } from '@interfaces/job';
import { ICandidateProfile } from '@interfaces/user';
import { IUserObserver, StoreService } from '@services/store.service';
import moment from 'moment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';

@Component({
  selector: 'hc-job-info-detail',
  templateUrl: './job-info-detail.component.html',
  styleUrls: ['./job-info-detail.component.scss']
})
export class JobInfoDetailComponent implements OnInit {
  @Input() jobDetail = {} as JobDetail;
  @Input() loading = false;
  @Input() profile = {} as ICandidateProfile;
  @Output() onApply = new EventEmitter();
  @Output() cancelApply = new EventEmitter();
  user$ = new Observable<IUserObserver>();
  constructor(
    private modalService: BsModalService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.user$ = this.storeService.observerUser();
  }
  get statusApply() {
    return {
      [JobApplyState.approve]: 'Đơn của bạn đã được chấp thuận',
      [JobApplyState.reject]: 'Đơn của bạn đã bị từ chối',
      [JobApplyState.pending]: 'Đơn của bạn đang được xử lý'
    }
  }
  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME;
  }
  report() {
    const modal = this.modalService.show(ReportJobComponent, {
      ignoreBackdropClick: true,
      initialState: {
        job: this.jobDetail
      }
    })
    modal.content?.onClose.subscribe(() => {
      modal.hide();
    })
  }

  get endDateStatus() {
    return moment().isAfter(moment(this.jobDetail.endDate)) ? '(Hết hạn)' : ''
  }

  get experience() {
    const experience = +this.jobDetail?.experience.split(' ')[0];
    return experience !== 0 ? this.jobDetail.experience : 'Không yêu cầu'
  }

  get isShowCancelApplyButton() {
    return this.jobDetail.approvalStatus !== JobApplyState.reject;
  }
}

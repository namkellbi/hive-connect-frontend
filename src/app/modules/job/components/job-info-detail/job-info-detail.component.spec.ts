import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobInfoDetailComponent } from './job-info-detail.component';

describe('JobInfoDetailComponent', () => {
  let component: JobInfoDetailComponent;
  let fixture: ComponentFixture<JobInfoDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobInfoDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobInfoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobLayoutComponent } from './job-layout.component';

const routes: Routes = [
  { path: ':id', component: JobLayoutComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule { }

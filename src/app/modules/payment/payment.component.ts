import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseMessage, ResponseStatus } from '@interfaces/response';
import { PaymentService } from '@services/payment.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  readonly COUNTDOWN_TIME = 30;
  count = this.COUNTDOWN_TIME;
  interval: any;
  loading = true;
  isSuccess = false;
  message = 'Giao dịch thất bại';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private paymentService: PaymentService
  ) { }

  ngOnInit(): void {
    const query = this.activatedRoute.snapshot.queryParams;
    this.paymentService.savePaymentResult(query)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.isSuccess = true;
          } else {
            this.message = message || this.message;
          }
          this.startCountDown();
        }
      });
  }
  startCountDown() {
    this.count = this.COUNTDOWN_TIME;
    this.interval = setInterval(() => {
      if (this.count === 0) {
        clearInterval(this.interval);
        this.router.navigate(['/recruiter/jobs']);
      }
      this.count -= 1;
    }, 1000);
  }
}

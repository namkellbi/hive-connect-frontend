import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorMessageBubbleComponent } from './collaborator-message-bubble.component';

describe('CollaboratorMessageBubbleComponent', () => {
  let component: CollaboratorMessageBubbleComponent;
  let fixture: ComponentFixture<CollaboratorMessageBubbleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorMessageBubbleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorMessageBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

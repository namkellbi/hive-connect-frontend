import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hc-collaborator-message-bubble',
  templateUrl: './collaborator-message-bubble.component.html',
  styleUrls: ['./collaborator-message-bubble.component.scss']
})
export class CollaboratorMessageBubbleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

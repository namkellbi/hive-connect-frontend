import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorSidebarComponent } from './collaborator-sidebar.component';

describe('CollaboratorSidebarComponent', () => {
  let component: CollaboratorSidebarComponent;
  let fixture: ComponentFixture<CollaboratorSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

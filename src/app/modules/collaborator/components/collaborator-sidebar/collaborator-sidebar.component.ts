import { Component, OnInit } from '@angular/core';
import { IRoom, RoomStatus } from '@interfaces/chat';
import { ChatService } from '@services/chat.service';
import { map, Observable } from 'rxjs';

@Component({
  selector: 'hc-collaborator-sidebar',
  templateUrl: './collaborator-sidebar.component.html',
  styleUrls: ['./collaborator-sidebar.component.scss']
})
export class CollaboratorSidebarComponent implements OnInit {
  rooms$ = new Observable<IRoom[]>();
  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.rooms$ = this.chatService.observableRooms()
      .pipe(
        map(rooms => rooms.filter(room => room.status === RoomStatus.created || room.supporterSocket === this.chatService.getSocketId()))
      );
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorChatWindowComponent } from './collaborator-chat-window.component';

describe('CollaboratorChatWindowComponent', () => {
  let component: CollaboratorChatWindowComponent;
  let fixture: ComponentFixture<CollaboratorChatWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorChatWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorChatWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

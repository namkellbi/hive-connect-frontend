import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hc-collaborator-chat-window',
  templateUrl: './collaborator-chat-window.component.html',
  styleUrls: ['./collaborator-chat-window.component.scss']
})
export class CollaboratorChatWindowComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

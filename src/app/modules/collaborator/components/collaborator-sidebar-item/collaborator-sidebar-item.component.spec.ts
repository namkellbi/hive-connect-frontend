import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorSidebarItemComponent } from './collaborator-sidebar-item.component';

describe('CollaboratorSidebarItemComponent', () => {
  let component: CollaboratorSidebarItemComponent;
  let fixture: ComponentFixture<CollaboratorSidebarItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorSidebarItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorSidebarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

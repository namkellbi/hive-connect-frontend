import { Component, Input, OnInit } from '@angular/core';
import { IRoom } from '@interfaces/chat';

@Component({
  selector: 'hc-collaborator-sidebar-item',
  templateUrl: './collaborator-sidebar-item.component.html',
  styleUrls: ['./collaborator-sidebar-item.component.scss']
})
export class CollaboratorSidebarItemComponent implements OnInit {
  @Input() room = {} as IRoom;
  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorChatInputComponent } from './collaborator-chat-input.component';

describe('CollaboratorChatInputComponent', () => {
  let component: CollaboratorChatInputComponent;
  let fixture: ComponentFixture<CollaboratorChatInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorChatInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorChatInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

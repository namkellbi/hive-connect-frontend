import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hc-collaborator-chat-input',
  templateUrl: './collaborator-chat-input.component.html',
  styleUrls: ['./collaborator-chat-input.component.scss']
})
export class CollaboratorChatInputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

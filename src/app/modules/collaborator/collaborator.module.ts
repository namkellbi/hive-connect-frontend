import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollaboratorRoutingModule } from './collaborator-routing.module';
import { CollaboratorComponent } from './collaborator.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CollaboratorChatWindowComponent } from './components/collaborator-chat-window/collaborator-chat-window.component';
import { CollaboratorSidebarComponent } from './components/collaborator-sidebar/collaborator-sidebar.component';
import { CollaboratorSidebarItemComponent } from './components/collaborator-sidebar-item/collaborator-sidebar-item.component';
import { CollaboratorMessageBubbleComponent } from './components/collaborator-message-bubble/collaborator-message-bubble.component';
import { CollaboratorChatInputComponent } from './components/collaborator-chat-input/collaborator-chat-input.component';


@NgModule({
  declarations: [
    CollaboratorComponent,
    CollaboratorChatWindowComponent,
    CollaboratorSidebarComponent,
    CollaboratorSidebarItemComponent,
    CollaboratorMessageBubbleComponent,
    CollaboratorChatInputComponent
  ],
  imports: [
    CommonModule,
    CollaboratorRoutingModule,
    SharedModule
  ]
})
export class CollaboratorModule { }

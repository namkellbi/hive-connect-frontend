import { Component, OnInit } from '@angular/core';
import { AuthService } from '@services/auth.service';
import { ChatService } from '@services/chat.service';

@Component({
  selector: 'hc-collaborator',
  templateUrl: './collaborator.component.html',
  styleUrls: ['./collaborator.component.scss']
})
export class CollaboratorComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.chatService.fetchCreatedRoom();
  }

  logout() {
    this.authService.logout();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChatPopupComponent } from './chat-popup/chat-popup.component';
import { ChatViewBubbleComponent } from './components/chat-view-bubble/chat-view-bubble.component';
import { ChatViewPopupComponent } from './components/chat-view-popup/chat-view-popup.component';
import { ChatConversationComponent } from './components/chat-conversation/chat-conversation.component';
import { ChatFormComponent } from './components/chat-form/chat-form.component';
import { FormModule } from '@components/form/form.module';



@NgModule({
  declarations: [
    ChatPopupComponent,
    ChatViewBubbleComponent,
    ChatViewPopupComponent,
    ChatConversationComponent,
    ChatFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormModule
  ],
  exports: [
    ChatPopupComponent
  ]
})
export class ChatModule { }

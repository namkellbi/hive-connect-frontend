import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getRegexEmail, getRegexPhone } from '@constant/form';
import { RoomStatus } from '@interfaces/chat';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { ChatService } from '@services/chat.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.scss']
})
export class ChatFormComponent implements OnInit {
  @Output() onFormSubmitted = new EventEmitter();
  form!: FormGroup;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private helpService: HelperService,
    private storeService: StoreService,
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(getRegexEmail()), Validators.maxLength(255)]],
      phone: ['', [Validators.required, Validators.pattern(getRegexPhone()), Validators.maxLength(20)]],
      title: ['', [Validators.required]],
      questionerId: [this.storeService.getUserInfo().id],
      status: RoomStatus.created,
      questionerSocket: this.chatService.getSocketId()
    })
  }

  submitForm() {
    this.form.markAsTouched();
    if (this.form.invalid) return;
    const params = this.form.value;
    this.loading = true;
    this.apiService.submitFormQuestion(params)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.onFormSubmitted.emit(response.data);
          } else {
            this.helpService.showError('', 'Gửi yêu cầu thất bại')
          }
        },
        error: error => this.helpService.callApiFailedHandler(error)
      })
  }
}

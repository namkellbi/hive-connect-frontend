import { Component, Input, OnInit } from '@angular/core';
import { IRoom } from '@interfaces/chat';
import { ChatService } from '@services/chat.service';

@Component({
  selector: 'hc-chat-conversation',
  templateUrl: './chat-conversation.component.html',
  styleUrls: ['./chat-conversation.component.scss']
})
export class ChatConversationComponent implements OnInit {
  @Input() room = {} as IRoom;
  constructor(
    private chatService: ChatService,
  ) { }

  ngOnInit(): void {
  }

}

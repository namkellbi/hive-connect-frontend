import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatViewPopupComponent } from './chat-view-popup.component';

describe('ChatViewPopupComponent', () => {
  let component: ChatViewPopupComponent;
  let fixture: ComponentFixture<ChatViewPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatViewPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatViewPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { IMessageSend, IRoom, RoomStatus } from '@interfaces/chat';
import { ChatService } from '@services/chat.service';
import { StoreService } from '@services/store.service';
enum ViewMode {
  form = 'form',
  chat = 'chat'
}
@Component({
  selector: 'hc-chat-view-popup',
  templateUrl: './chat-view-popup.component.html',
  styleUrls: ['./chat-view-popup.component.scss']
})
export class ChatViewPopupComponent implements OnInit, OnDestroy {
  mode = ViewMode.form;
  room = {} as IRoom;
  messageText = '';
  @Output() onClosePopup = new EventEmitter();
  constructor(
    private chatService: ChatService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.chatService.clearMessage();
  }

  closePopup() {
    this.onClosePopup.emit();
  }

  changeViewMode(mode: ViewMode, room: IRoom) {
    this.mode = mode;
    this.room = room;
  }

  get getViewMode() {
    return ViewMode;
  }

  get roomStatus() {
    return RoomStatus;
  }

  sendMessage() {
    if (!this.messageText) return;
    const params = {
      content: this.messageText,
      from: this.room.questionerSocket,
      ownerId: this.storeService.getUserInfo().id,
      ownerSocketId: this.room.questionerSocket,
      to: this.room.supporterSocket,
      roomId: this.room.id,
    } as IMessageSend;
    this.chatService.sendMessage(params);
    this.messageText = '';
  }
}

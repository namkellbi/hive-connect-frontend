import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'hc-chat-view-bubble',
  templateUrl: './chat-view-bubble.component.html',
  styleUrls: ['./chat-view-bubble.component.scss']
})
export class ChatViewBubbleComponent implements OnInit {
  @Output() onOpenChat = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}

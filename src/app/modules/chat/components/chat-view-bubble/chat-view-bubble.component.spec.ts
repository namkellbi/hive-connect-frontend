import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatViewBubbleComponent } from './chat-view-bubble.component';

describe('ChatViewBubbleComponent', () => {
  let component: ChatViewBubbleComponent;
  let fixture: ComponentFixture<ChatViewBubbleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatViewBubbleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatViewBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

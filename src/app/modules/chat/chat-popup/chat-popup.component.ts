import { Component, OnInit } from '@angular/core';

enum ViewState {
  bubble = 'bubble',
  popup = 'popup'
}

@Component({
  selector: 'hc-chat-popup',
  templateUrl: './chat-popup.component.html',
  styleUrls: ['./chat-popup.component.scss']
})
export class ChatPopupComponent implements OnInit {
  viewState = ViewState.bubble;
  constructor() { }

  ngOnInit(): void {
  }

  get getViewState() {
    return ViewState;
  }

  openPopup() {
    this.viewState = ViewState.popup;
  }

  closePopup() {
    this.viewState = ViewState.bubble;
  }
}

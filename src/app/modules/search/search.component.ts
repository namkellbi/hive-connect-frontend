import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Job } from '@interfaces/job';
import { IFindJobRequest } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { SearchService } from '@services/search.service';
import { StoreService } from '@services/store.service';
import { combineLatest, finalize, startWith, Subject, takeUntil } from 'rxjs';
import { DEFAULT_PAGINATION, DEFAULT_SEARCH } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  jobs = [] as Job[];
  pagination = { ...DEFAULT_PAGINATION, pageSize: 20 };
  loadMore$ = new Subject();
  sort$ = new Subject<string>();
  loading = false;
  totals = 0;
  destroy$ = new Subject();
  isLogin = false;
  constructor(
    private searchService: SearchService,
    private activatedRoute: ActivatedRoute,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.searchService.setStateSearchBar(true);
    combineLatest([
      this.activatedRoute.queryParams,
      this.sort$.asObservable().pipe(startWith('createdAt'))
    ])
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(([params, sortField]) => {
        this.jobs = [];
        this.pagination.pageNo = 1;
        this.searchJob(params, sortField);
      });
    this.storeService.observerUser()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => this.isLogin = data != null && !!data.id)
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  searchJob(params: Params, sortBy = '') {
    const request: IFindJobRequest = {
      ...DEFAULT_SEARCH,
      ...this.pagination,
      fieldId: params['career'] || DEFAULT_SEARCH.fieldId,
      workForm: params['workingForm'] || '',
      countryId: params['location'] || '',
      jobName: params['searchText'] || '',
      sortBy
    }
    this.loading = true;
    this.searchService.searchJob(request)
      .pipe(
        finalize(() => this.loading = false))
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.jobs.push(...data.data);
            this.totals = data.pagination.totalRecords;
          }
        }
      });
  }
  loadMore() {
    if (this.totals > this.jobs.length && !this.loading) {
      this.pagination.pageNo += 1;

    }
  }
  sortChange(event: Event) {
    this.sort$.next((event.target as HTMLInputElement).value);
  }
}

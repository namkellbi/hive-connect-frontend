import { Component, Input, OnInit } from '@angular/core';
import { MAP_JOB_STYLE_NAME } from '@constant/form';
import { Job } from '@interfaces/job';
import moment from 'moment';

@Component({
  selector: 'hc-job-item-search-card',
  templateUrl: './job-item-search-card.component.html',
  styleUrls: ['./job-item-search-card.component.scss']
})
export class JobItemSearchCardComponent implements OnInit {
  @Input() job = {} as Job;
  constructor() { }

  ngOnInit(): void {
  }

  get remainingTime() {
    return moment(this.job.endDate).diff(moment(), 'days') + ' ngày';
  }

  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME[this.job.workForm];
  }
}

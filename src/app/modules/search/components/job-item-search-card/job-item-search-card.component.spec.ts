import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobItemSearchCardComponent } from './job-item-search-card.component';

describe('JobItemSearchCardComponent', () => {
  let component: JobItemSearchCardComponent;
  let fixture: ComponentFixture<JobItemSearchCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobItemSearchCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobItemSearchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hc-recommendation-job-slider',
  templateUrl: './recommendation-job-slider.component.html',
  styleUrls: ['./recommendation-job-slider.component.scss']
})
export class RecommendationJobSliderComponent implements OnInit {
  itemsPerSlide = 5;
  items = Array.from({ length: 20 });
  constructor() { }

  ngOnInit(): void {
  }

  slideChange($event: number | number[] | void) {
    console.log('slide change', $event);
  }
}

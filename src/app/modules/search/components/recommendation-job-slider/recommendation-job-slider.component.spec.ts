import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendationJobSliderComponent } from './recommendation-job-slider.component';

describe('RecommendationJobSliderComponent', () => {
  let component: RecommendationJobSliderComponent;
  let fixture: ComponentFixture<RecommendationJobSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecommendationJobSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendationJobSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

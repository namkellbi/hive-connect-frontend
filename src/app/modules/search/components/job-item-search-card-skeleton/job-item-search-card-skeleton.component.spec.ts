import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobItemSearchCardSkeletonComponent } from './job-item-search-card-skeleton.component';

describe('JobItemSearchCardSkeletonComponent', () => {
  let component: JobItemSearchCardSkeletonComponent;
  let fixture: ComponentFixture<JobItemSearchCardSkeletonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobItemSearchCardSkeletonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobItemSearchCardSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hc-job-item-search-card-skeleton',
  templateUrl: './job-item-search-card-skeleton.component.html',
  styleUrls: ['./job-item-search-card-skeleton.component.scss']
})
export class JobItemSearchCardSkeletonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

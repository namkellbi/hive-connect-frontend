import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { RecommendationJobSliderComponent } from './components/recommendation-job-slider/recommendation-job-slider.component';
import { ComponentsModule } from '@components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { JobItemSearchCardComponent } from './components/job-item-search-card/job-item-search-card.component';
import { JobItemSearchCardSkeletonComponent } from './components/job-item-search-card-skeleton/job-item-search-card-skeleton.component';
import { HomeModule } from '@pages/home/home.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [
    SearchComponent,
    RecommendationJobSliderComponent,
    JobItemSearchCardComponent,
    JobItemSearchCardSkeletonComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ComponentsModule,
    SharedModule,
    HomeModule,
    InfiniteScrollModule
  ]
})
export class SearchModule { }

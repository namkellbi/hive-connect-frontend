import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateJobGuard } from 'src/app/core/guards/create-job.guard';
import { RecruiterGuard } from 'src/app/core/guards/recruiter.guard';
import { CreateJobResolver } from 'src/app/core/resolver/create-job.resolver';
import { RecruiterAccountLayoutComponent } from './components/recruiter-account-layout/recruiter-account-layout.component';
import { RecruiterCreateJobComponent } from './components/recruiter-create-job/recruiter-create-job.component';
import { RecruiterCvAppliedComponent } from './components/recruiter-cv-applied/recruiter-cv-applied.component';
import { RecruiterLayoutComponent } from './components/recruiter-layout/recruiter-layout.component';
import { FindCvComponent } from './pages/find-cv/find-cv.component';
import { RecrtuiterHomepagePackageComponent } from './pages/recrtuiter-homepage-package/recrtuiter-homepage-package.component';
import { RecruiterAccountCompanyLayoutComponent } from './pages/recruiter-account-company-layout/recruiter-account-company-layout.component';
import { RecruiterAccountInfoComponent } from './pages/recruiter-account-info/recruiter-account-info.component';
import { RecruiterAccountPasswordComponent } from './pages/recruiter-account-password/recruiter-account-password.component';
import { RecruiterCvDetailComponent } from './pages/recruiter-cv-detail/recruiter-cv-detail.component';
import { RecruiterIntroductionComponent } from './pages/recruiter-introduction/recruiter-introduction.component';
import { RecruiterJobsComponent } from './pages/recruiter-jobs/recruiter-jobs.component';
import { RecruiterPaymentConfigComponent } from './pages/recruiter-payment-config/recruiter-payment-config.component';
import { RecruiterPaymentPackageDetailComponent } from './pages/recruiter-payment-package-detail/recruiter-payment-package-detail.component';
import { RecruiterPaymentPackageComponent } from './pages/recruiter-payment-package/recruiter-payment-package.component';
import { RecruiterPaymentTransactionComponent } from './pages/recruiter-payment-transaction/recruiter-payment-transaction.component';

const routes: Routes = [
  { path: 'introduction', component: RecruiterIntroductionComponent },
  // { path: 'feature', component: RecruiterHomepageFeatureComponent },
  { path: 'package', component: RecrtuiterHomepagePackageComponent },
  {
    path: '', component: RecruiterLayoutComponent, canActivate: [RecruiterGuard], children: [
      { path: '', redirectTo: 'account', pathMatch: 'full' },
      {
        path: 'jobs', children: [
          { path: '', component: RecruiterJobsComponent },
          { path: ':id/cv', component: RecruiterCvAppliedComponent }
        ]
      },
      { path: 'dashboard', redirectTo: 'account', pathMatch: 'full' },
      { path: 'job/create', component: RecruiterCreateJobComponent, canActivate: [CreateJobGuard], },
      { path: 'job/:id/edit', component: RecruiterCreateJobComponent, canActivate: [CreateJobGuard], },
      {
        path: 'account', component: RecruiterAccountLayoutComponent, children: [
          { path: 'info', component: RecruiterAccountInfoComponent },
          { path: 'company', component: RecruiterAccountCompanyLayoutComponent },
          { path: 'password', component: RecruiterAccountPasswordComponent },
          // { path: 'update', component: RecruiterInfoUpdateComponent },
          { path: '', redirectTo: 'info', pathMatch: 'full' }
        ]
      },
      {
        path: 'payment', children: [
          { path: 'package/:id/:rentalId', component: RecruiterPaymentPackageDetailComponent },
          { path: 'package', component: RecruiterPaymentPackageComponent },
          { path: 'transaction', component: RecruiterPaymentTransactionComponent },
          // { path: 'order/:id', component: RecruiterPaymentOrderComponent },
          { path: 'config', component: RecruiterPaymentConfigComponent },
          { path: 'config/:id', component: RecruiterPaymentConfigComponent }
        ]
      },
      { path: 'find-cv', component: FindCvComponent },
      { path: ':cvId/cv', component: RecruiterCvDetailComponent }
    ]
  },
  { path: '*', redirectTo: 'introduction', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecruiterRoutingModule { }

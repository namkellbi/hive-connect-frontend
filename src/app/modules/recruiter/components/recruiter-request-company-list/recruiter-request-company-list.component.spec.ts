import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterRequestCompanyListComponent } from './recruiter-request-company-list.component';

describe('RecruiterRequestCompanyListComponent', () => {
  let component: RecruiterRequestCompanyListComponent;
  let fixture: ComponentFixture<RecruiterRequestCompanyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterRequestCompanyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterRequestCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

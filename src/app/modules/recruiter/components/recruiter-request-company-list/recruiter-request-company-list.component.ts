import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IRecruiterProfile } from '@interfaces/user';

@Component({
  selector: 'hc-recruiter-request-company-list',
  templateUrl: './recruiter-request-company-list.component.html',
  styleUrls: ['./recruiter-request-company-list.component.scss']
})
export class RecruiterRequestCompanyListComponent implements OnInit {
  @Input() profile = {} as IRecruiterProfile;
  @Input() showHeader = false;
  @Output() onClose = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  joinCompanyFinish($event: any) {
    this.onClose.emit($event);
  }
}

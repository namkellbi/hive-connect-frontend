import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterConfigBodyBannerComponent } from './recruiter-config-body-banner.component';

describe('RecruiterConfigBodyBannerComponent', () => {
  let component: RecruiterConfigBodyBannerComponent;
  let fixture: ComponentFixture<RecruiterConfigBodyBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterConfigBodyBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterConfigBodyBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobApplyState } from '@interfaces/job';
import { BannerPackageDetail, BannerPosition, BannerRequestPosition, IBannerRequest, PaymentPackageDetail } from '@interfaces/payment';
import { FileUploadType } from '@interfaces/request';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize, map, of, switchMap } from 'rxjs';
import { RecruiterConfigBannerItemComponent } from '../recruiter-config-baner-item/recruiter-config-baner-item.component';
type Config = { [key in BannerPosition]: boolean };
@Component({
  selector: 'hc-recruiter-config-body-banner',
  templateUrl: './recruiter-config-body-banner.component.html',
  styleUrls: ['./recruiter-config-body-banner.component.scss']
})
export class RecruiterConfigBodyBannerComponent implements OnInit {
  @Input() detail = {} as PaymentPackageDetail;
  @Output() reloadPackage = new EventEmitter();
  @ViewChildren(RecruiterConfigBannerItemComponent) itemBanner!: QueryList<RecruiterConfigBannerItemComponent>;
  config: Config = {} as Config;
  loading = false;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private activatedRoute: ActivatedRoute,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.config = Object.entries(BannerRequestPosition).reduce((acc, [key, value]) => ({ ...acc, [value]: this.detail.infoPPRes.bannerPaymentPackage[value] }), {} as Config);
  }

  get bannerPosition() {
    return BannerPosition;
  }

  get banner(): { [key: string]: IBannerRequest } {
    return this.detail.banner || Object.entries(BannerPosition).reduce((acc, [key, value]) => ({ ...acc, [value]: {} as IBannerRequest }), {});
  }

  async submit() {
    if (!this.itemBanner) return;
    const bannerObj = this.itemBanner.reduce((acc, cur) => cur.getUploadFileAndPosition() ? acc.concat(cur.getUploadFileAndPosition()) : acc, [] as any[]);
    if (!bannerObj.length) {
      this.helperService.showError('', 'Bạn phải chọn ảnh tải lên');
      return;
    }
    this.loading = true;
    try {
      // const uploadResult = await Promise.all(bannerObj.map(async ({ file, position }) => {
      //   const result = await this.helperService.uploadImage(file, FileUploadType.image);
      //   return {
      //     url: result.url,
      //     position
      //   }
      // }));
      bannerObj.map(({ file, position }) => this.helperService.uploadImageObservable(file, FileUploadType.image)
        .pipe(
          map(result => ({
            url: result.url,
            position
          }))
        ))
        .reduce((acc, cur) => acc.pipe(
          switchMap(data => cur.pipe(
            map(result => ([...data, result]))
          ))
        ), of([] as { url: string, position: string }[]))
        .subscribe({
          next: uploadResult => {
            const request = {
              paymentId: this.activatedRoute.snapshot.params['id'],
              ...uploadResult.reduce((acc, cur) => ({ ...acc, [cur.position]: cur.url }), {} as any)
            }
            const { id } = this.storeService.getRecruiterProfile();
            this.apiService.requestUploadBanner(id, request)
              .pipe(
                finalize(() => this.loading = false)
              )
              .subscribe({
                next: response => {
                  this.helperService.callApiResponseHandler(response);
                  this.detail.banner = {
                    ...this.detail.banner,
                    ...uploadResult.reduce((acc, cur) => ({
                      ...acc, [cur.position]: {
                        approvalStatus: JobApplyState.pending,
                        displayPosition: cur.position,
                        imageUrl: cur.url
                      } as IBannerRequest
                    }), {} as BannerPackageDetail)
                  }
                  this.reloadPackage.emit();
                  this.loading = false;
                },
                error: error => this.helperService.callApiFailedHandler(error)
              })
          },
        });
    } catch (error: any) {
      this.helperService.callApiFailedHandler(error);
      this.loading = false;
    }
  }
}

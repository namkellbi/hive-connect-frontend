import { Component, OnDestroy, OnInit } from '@angular/core';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterProfile } from '@interfaces/user';
import { NavigationService } from '@services/navigation.service';
import { SearchService } from '@services/search.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { filter, map, Observable, tap } from 'rxjs';

@Component({
  selector: 'hc-recruiter-layout',
  templateUrl: './recruiter-layout.component.html',
  styleUrls: ['./recruiter-layout.component.scss']
})
export class RecruiterLayoutComponent implements OnInit, OnDestroy {
  profile$ = new Observable<IRecruiterProfile>();
  constructor(
    private searchService: SearchService,
    private userService: UserService,
    private storeService: StoreService,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.searchService.setStateSearchBar(false);
    this.profile$ = this.userService.fetchRecruiterProfile().pipe(
      filter(data => data.status === ResponseStatus.success),
      map(({ data }) => data),
      tap(data => this.storeService.recruiterProfile.next(data))
    );
    document.body.classList.add('bg-grey');
    this.navigationService.check$.next(null);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    document.body.classList.remove('bg-grey');
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterConfigBanerItemComponent } from './recruiter-config-baner-item.component';

describe('RecruiterConfigBanerItemComponent', () => {
  let component: RecruiterConfigBanerItemComponent;
  let fixture: ComponentFixture<RecruiterConfigBanerItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterConfigBanerItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterConfigBanerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

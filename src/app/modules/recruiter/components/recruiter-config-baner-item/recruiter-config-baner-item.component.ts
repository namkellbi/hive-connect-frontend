import { Component, Input, OnInit } from '@angular/core';
import { MAP_JOB_APPLY_STATE } from '@constant/form';
import { JobApplyState } from '@interfaces/job';
import { BannerPosition, IBannerRequest } from '@interfaces/payment';
import { FileFinishPendingType } from 'src/app/shared/directives/file-handle.directive';

@Component({
  selector: 'hc-recruiter-config-banner-item',
  templateUrl: './recruiter-config-baner-item.component.html',
  styleUrls: ['./recruiter-config-baner-item.component.scss']
})
export class RecruiterConfigBannerItemComponent implements OnInit {
  @Input() banner = {} as IBannerRequest;
  @Input() position = '';
  file: FileFinishPendingType = {} as FileFinishPendingType;
  constructor() { }

  ngOnInit(): void {
  }

  get bannerStateLabel() {
    if (!this.banner?.approvalStatus) {
      return 'Bạn hãy tải ảnh lên'
    }
    return {
      [JobApplyState.approve]: 'Chấp thuận',
      [JobApplyState.reject]: 'Bị từ chối',
      [JobApplyState.pending]: 'Đang đợi quản trị viên duyệt - Bạn có thể cập nhật ảnh mới'
    }[this.banner.approvalStatus];
  }

  get isAllowUpload() {
    return this.banner?.approvalStatus === JobApplyState.approve ? false : true;
  }

  get bannerTitle() {
    return {
      [BannerPosition.homepageBannerA]: 'Trang chủ khu vực A',
      [BannerPosition.homepageBannerB]: 'Trang chủ khu vực B',
      [BannerPosition.homepageBannerC]: 'Trang chủ khu vực C',
      [BannerPosition.jobBannerA]: 'Trang công việc khu vực A',
      [BannerPosition.jobBannerB]: 'Trang công việc khu vực B',
      [BannerPosition.jobBannerC]: 'Trang công việc khu vực C',
      [BannerPosition.spotlight]: 'Popup khi vào trang trủ',
    }[this.position || BannerPosition.spotlight];
  }

  get bannerResolution() {
    return {
      [BannerPosition.homepageBannerA]: '(16:9)',
      [BannerPosition.homepageBannerB]: '(16:9)',
      [BannerPosition.homepageBannerC]: '(16:9)',
      [BannerPosition.jobBannerA]: '(16:9)',
      [BannerPosition.jobBannerB]: '(16:9)',
      [BannerPosition.jobBannerC]: '(16:9)',
      [BannerPosition.spotlight]: '(9:16)',
    }[this.position || BannerPosition.spotlight];
  }

  handleImageFinish($event: FileFinishPendingType[]) {
    if ($event && $event[0]) {
      this.file = $event[0];
    }
  }
  removeFile() {
    this.file = {} as FileFinishPendingType;
  }

  getPositionUpload(position: string) {
    return {
      [BannerPosition.homepageBannerA]: 'homepageBannerAImage',
      [BannerPosition.homepageBannerB]: 'homepageBannerBImage',
      [BannerPosition.homepageBannerC]: 'homepageBannerCImage',
      [BannerPosition.jobBannerA]: 'jobBannerAImage',
      [BannerPosition.jobBannerB]: 'jobBannerBImage',
      [BannerPosition.jobBannerC]: 'jobBannerCImage',
      [BannerPosition.spotlight]: 'spotLightImage',
    }[position] || '';
  }

  getUploadFileAndPosition() {
    if (this.file.file) {
      return {
        file: this.file.file,
        position: this.getPositionUpload(this.position)
      }
    }
    return null
  }
}

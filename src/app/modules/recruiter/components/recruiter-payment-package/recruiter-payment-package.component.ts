import { Component, Input, OnInit } from '@angular/core';
import { MAP_RENTAL_PACKAGE } from '@constant/form';
import { IPackage, PackageType, PaymentRental } from '@interfaces/payment';

@Component({
  selector: 'hc-recruiter-payment-package-item[packageInfo]',
  templateUrl: './recruiter-payment-package.component.html',
  styleUrls: ['./recruiter-payment-package.component.scss']
})
export class RecruiterPaymentPackageComponent implements OnInit {
  @Input() packageInfo = {} as IPackage;
  constructor() { }

  ngOnInit(): void {
  }

  get packageType() {
    return {
      [PackageType.basic]: "Basic",
      [PackageType.classic]: "Classic",
      [PackageType.gold]: "Gold",
    }
  }
  get rentalName() {
    return MAP_RENTAL_PACKAGE[this.packageInfo.rentalPackageId];
  }

  get wrapperClass() {
    return {
      [PaymentRental.advertisement]: 'advertisement',
      [PaymentRental.banner]: 'banner',
      [PaymentRental.candidate]: 'candidate'
    }[this.packageInfo.rentalPackageId]
  }

  get resolveUrl() {
    return !location.pathname.includes('recruiter/package') ? [this.packageInfo.id, this.packageInfo.rentalPackageId] : ['/recruiter', 'package']
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCreateJobComponent } from './recruiter-create-job.component';

describe('RecruiterCreateJobComponent', () => {
  let component: RecruiterCreateJobComponent;
  let fixture: ComponentFixture<RecruiterCreateJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecruiterCreateJobComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCreateJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

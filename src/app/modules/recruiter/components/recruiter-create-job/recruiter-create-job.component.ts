import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DROPDOWN_GENDER, DROPDOWN_JOB_STYLE, DROPDOWN_LEVEL } from '@constant/form';
import { IDropDownItem } from '@interfaces/form';
import { ICompany, Job, JobDetail, JobState } from '@interfaces/job';
import { IResponse, ResponseStatus } from '@interfaces/response';
import { IRecruiterProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { SearchService } from '@services/search.service';
import { StoreService } from '@services/store.service';
import moment from 'moment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { filter, finalize, map, Subject, switchMap, takeUntil } from 'rxjs';
import { checkRangeSalary } from 'src/app/core/utils/form';

@Component({
  selector: 'hc-recruiter-create-job',
  templateUrl: './recruiter-create-job.component.html',
  styleUrls: ['./recruiter-create-job.component.scss']
})
export class RecruiterCreateJobComponent implements OnInit, OnDestroy {
  @Input() job = {} as JobDetail;
  profile = {} as IRecruiterProfile;
  loading = false;
  loadingJob = false;

  fieldJob: IDropDownItem<number>[] = [];
  form!: FormGroup;
  company: IDropDownItem<number>[] = [];
  country: IDropDownItem<number>[] = [];
  companyDetail = {} as ICompany;

  readonly DROPDOWN_JOB_STYLE = DROPDOWN_JOB_STYLE;
  readonly DROPDOWN_LEVEL = DROPDOWN_LEVEL;
  readonly DROPDOWN_GENDER = DROPDOWN_GENDER;

  step = 0;

  date = new Date();
  destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService,
    private searchService: SearchService,
    private router: Router,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.initForm();
    if (this.job && this.job.companyName) {
      this.setFormData(this.job);
    }
    this.activatedRoute.params.pipe(
      takeUntil(this.destroy$)
    ).subscribe(params => {
      if (params['id']) {
        this.resetVariable();
        this.getJobDetail(+params['id']);
      }
    });
    this.storeService.observerRecruiterProfile()
      .pipe(
        takeUntil(this.destroy$),
        filter(data => (!!data.id || !!data.recruiterId) && !!data.companyId),
        switchMap(profile => {
          this.profile = profile;
          this.getAccountInfo(profile.id);
          this.form.patchValue({ companyId: profile.companyId, recruiterId: profile.id || profile.recruiterId });
          return this.getCompanyInfo(profile.companyId, profile);
        })
      ).subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.companyDetail = response.data;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
    this.searchService.observerJobField()
      .pipe(
        takeUntil(this.destroy$),
        map(data =>
          data.map(item =>
            ({ value: item.id, label: item.fieldName })
          ))
      )
      .subscribe(data => this.fieldJob = data);
    this.searchService.observerCountry()
      .pipe(
        takeUntil(this.destroy$),
        map(data =>
          data.map(item =>
            ({ value: item.id, label: item.countryName })
          ))
      )
      .subscribe(data => this.country = data);
  }

  getCompanyInfo(companyId: number, recruiter: IRecruiterProfile) {
    return this.apiService.getCompanyDetail(companyId, recruiter.id)
  }

  initForm() {
    this.form = this.fb.group({
      companyId: ['', [Validators.required]],
      recruiterId: ['', [Validators.required]],
      fieldId: ['', [Validators.required]],
      countryId: ['', [Validators.required]],
      jobName: ['', [Validators.required, Validators.maxLength(255)]],
      workPlace: ['', [Validators.required, Validators.maxLength(255)]],
      workForm: ['', [Validators.required, Validators.maxLength(50)]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      fromSalary: ['', [Validators.required, Validators.min(10000)]],
      toSalary: ['', [Validators.required, Validators.min(10000)]],
      numberRecruits: ['', [Validators.required, Validators.min(1)]],
      rank: ['Nhân viên', [Validators.required, Validators.maxLength(50)]],
      experience: ['5', [Validators.required, Validators.min(0), Validators.maxLength(255)]],
      gender: ['', []],
      jobDescription: ['', [Validators.required, Validators.maxLength(5000)]],
      jobRequirement: ['', [Validators.required, Validators.maxLength(5000)]],
      benefit: ['', [Validators.required, Validators.maxLength(5000)]],
      weekday: [5, [Validators.required, Validators.min(1), Validators.maxLength(20)]],
      academicLevel: ['Đại học', Validators.maxLength(100)],
      popularJob: true,
      newJob: true,
      urgentJob: true,
      hashtag: '',
      jobId: '',
      weekdayUnit: 'Ngày/Tuần',
      experienceUnit: 'Năm',
      flag: [JobState.posted, Validators.maxLength(10)],
    }, {
      validators: checkRangeSalary
    })
  }

  getFormValue() {
    const params = { ...this.form.value };
    const paramUrl = this.activatedRoute.snapshot.params;
    if (+paramUrl['id']) {
      params.id = +paramUrl['id'];
    }
    params.weekday = `${params.weekday} ${params.weekdayUnit}`;
    params.experience = params.experience ? `${params.experience} ${params.experienceUnit}` : 0;
    params.startDate = moment(params.startDate).utcOffset(0, true).toISOString();
    params.endDate = moment(params.endDate).utcOffset(0, true).toISOString();
    return params;
  }
  getAccountInfo(id: number) {
    this.apiService.getRecruiterAccountInfo(id)
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.step = +data.verifyStep.split('/')[0];
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  createOrUpdate(callback: (params: IResponse<Job>) => void) {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    const params = this.getFormValue();
    this.loading = true;
    const api = this.job.jobId ? this.apiService.updateJob(params) : this.apiService.createJob(params);
    api
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: (response) => {
          callback(response);
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  postJob() {
    this.createOrUpdate(({ status, data, message }) => {
      if (status === ResponseStatus.success) {
        this.router.navigate(['/recruiter', 'jobs']);
        this.helperService.showSuccess('', `${this.job.jobId ? 'Sửa' : 'Tạo'} công việc thành công`);
      } else {
        this.helperService.showError('', message || `${this.job.jobId ? 'Sửa' : 'Tạo'} công việc thất bại`);
      }
    })
  }

  saveDraft() {
    this.helperService.validateFormField(this.form);
    this.form.patchValue({ flag: JobState.draft });
    if (this.form.invalid) return;
    this.createOrUpdate(({ status, data, message }) => {
      if (status === ResponseStatus.success) {
        this.helperService.showSuccess('', `${this.job.jobId ? 'Sửa' : 'Tạo'} công việc thành công`);
        this.loading = true;
        this.apiService.saveDraftJob(data.jobId || this.job.jobId)
          .pipe(
            finalize(() => this.loading = false)
          )
          .subscribe({
            next: ({ status, data, message }) => {
              if (status === ResponseStatus.success) {
                this.router.navigate(['/recruiter', 'jobs']);
                this.helperService.showSuccess('', 'Lưu nháp thành công');
              } else {
                this.helperService.showError('', message || 'Lưu nháp thất bại');
              }
            },
            error: error => this.helperService.callApiFailedHandler(error)
          });
      } else {
        this.helperService.showError('', message || `${this.job.jobId ? 'Sửa' : 'Tạo'} công việc thất bại`);
      }
    });
  }

  getJobDetail(id: number) {
    this.loadingJob = true;
    this.apiService.getJobDetail(id)
      .pipe(
        finalize(() => this.loadingJob = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.setFormData(data);
            this.job = data;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  setFormData(job: JobDetail) {
    const weekday = job.weekday.split(' ')[0];
    const weekdayUnit = job.weekday.split(' ')[1];
    const experience = job.experience.split(' ')[0];
    const experienceUnit = job.experience.split(' ')[1] || 'Năm';
    this.form.patchValue({
      ...job,
      weekday,
      weekdayUnit,
      countryId: job.countryId || '',
      fieldId: job.fieldId || '',
      workForm: job.workForm || '',
      gender: job.gender ?? '',
      experience,
      experienceUnit,
      flag: JobState.posted,
      startDate: new Date(job.startDate),
      endDate: new Date(job.endDate)
    });
  }

  resetVariable() {
    this.form.reset();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}

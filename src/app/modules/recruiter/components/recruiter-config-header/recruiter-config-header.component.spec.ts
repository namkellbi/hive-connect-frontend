import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterConfigHeaderComponent } from './recruiter-config-header.component';

describe('RecruiterConfigHeaderComponent', () => {
  let component: RecruiterConfigHeaderComponent;
  let fixture: ComponentFixture<RecruiterConfigHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterConfigHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterConfigHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

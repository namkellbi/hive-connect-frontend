import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IPackage, PaymentPackage, PurchasedPackage } from '@interfaces/payment';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { catchError, finalize, map, Observable, of, switchMap, tap } from 'rxjs';

@Component({
  selector: 'hc-recruiter-config-header',
  templateUrl: './recruiter-config-header.component.html',
  styleUrls: ['./recruiter-config-header.component.scss']
})
export class RecruiterConfigHeaderComponent implements OnInit {
  @Input() loadingPayment = false;
  @Output() onLoadingPackage = new EventEmitter();
  @Output() purchasedPackageChange = new EventEmitter();
  payment$ = new Observable<PurchasedPackage[]>();
  selectedPayment = {} as PurchasedPackage;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.payment$ = this.storeService.observerUserInfoRecruiter()
      .pipe(
        switchMap(({ profile }) => {
          this.loadingPayment = true;
          return this.apiService.getPurchasedPackage(profile.id)
            .pipe(
              finalize(() => this.loadingPayment = false)
            );
        }),
        map((response) => {
          return this.helperService.callApiResponseHandler(response);
        }),
        tap(data => this.purchasedPackageChange.emit(data)),
        tap(data => {
          if (data && data.length) {
            const { id } = this.activatedRoute.snapshot.params;
            const selectedPackage = data.find(item => item.paymentId === +id);
            if (id && selectedPackage) {
              this.selectedPayment = selectedPackage;
            }
          }
        }),
        catchError(err => {
          this.helperService.callApiFailedHandler(err);
          this.loadingPayment = false;
          return of([]);
        })
      )
  }

  search() {
    this.router.navigate(['/recruiter', 'payment', 'config', this.selectedPayment.paymentId]);
  }

  get labelDD() {
    return this.selectedPayment.detailPackageName || 'Chọn gói đã mua';
  }
}

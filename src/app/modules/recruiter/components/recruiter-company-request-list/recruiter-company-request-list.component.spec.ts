import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCompanyRequestListComponent } from './recruiter-company-request-list.component';

describe('RecruiterCompanyRequestListComponent', () => {
  let component: RecruiterCompanyRequestListComponent;
  let fixture: ComponentFixture<RecruiterCompanyRequestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterCompanyRequestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCompanyRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

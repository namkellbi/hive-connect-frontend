import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmModalComponent } from '@components/confirm-modal/confirm-modal.component';
import { JobApplyState } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { IRequestJoinCompany, RequestJoinCompany } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-recruiter-company-request-list',
  templateUrl: './recruiter-company-request-list.component.html',
  styleUrls: ['./recruiter-company-request-list.component.scss']
})
export class RecruiterCompanyRequestListComponent implements OnInit {
  @Input() approverId = 0;
  loading = false;
  list: IRequestJoinCompany[] = [];
  pagination = {
    ...DEFAULT_PAGINATION
  }
  filter = {
    fullName: '',
    email: '',
    phone: '',
    status: RequestJoinCompany.pending
  }
  total = 0;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.getRequest();
  }

  getRequest() {
    this.loading = true;
    this.apiService.getRequestJoinCompany({
      approverId: this.approverId,
      ...this.filter,
      ...this.pagination
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ data, }) => {
          this.list = data.data;
          this.total = data.pagination.totalRecords;
          // this.listRequestFilter();
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  openModalConfirm(request: IRequestJoinCompany, statusChange: RequestJoinCompany) {
    let message = `Bạn có chắc muốn <b>${this.statusLabel[statusChange]}</b> không?`;
    if (statusChange === RequestJoinCompany.approve) {
      message += '<br/> Bạn phải đảm bảo rằng người này thuộc công ty của bạn.'
    }
    this.modalService.show(ConfirmModalComponent, {
      initialState: {
        message
      }
    })
  }

  changeStatus(request: IRequestJoinCompany, statusChange: RequestJoinCompany) {
    const params = {
      id: request.id,
      status: statusChange,
      senderId: request.senderId,
      companyId: request.companyId,
      approverId: this.approverId
    }
    this.loading = true;
    this.apiService.updateRequestJoinCompany(params)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, message }) => {
          if (status === ResponseStatus.success) {
            request.status = statusChange;
            const item = this.list.find(item => item.id === request.id);
            if (item) {
              item.status = statusChange;
            }
          } else {
            this.helperService.showError('', message || 'Phê duyệt thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get statusLabel() {
    return {
      [RequestJoinCompany.pending]: 'Đang xử lý',
      [RequestJoinCompany.approve]: 'Đồng ý',
      [RequestJoinCompany.deny]: 'Từ chối'
    }
  }

  get status() {
    return RequestJoinCompany;
  }

  search() {
    this.pagination.pageNo = 1;
    this.getRequest();
  }

  pageChanged($event: PageChangedEvent) {
    this.pagination.pageNo = $event.page;
    this.getRequest();
  }
}

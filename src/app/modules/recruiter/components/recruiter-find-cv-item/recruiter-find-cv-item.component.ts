import { Component, Input, OnInit } from '@angular/core';
import { IRecruiterFindCv } from '@interfaces/post';

@Component({
  selector: 'hc-recruiter-find-cv-item',
  templateUrl: './recruiter-find-cv-item.component.html',
  styleUrls: ['./recruiter-find-cv-item.component.scss']
})
export class RecruiterFindCvItemComponent implements OnInit {
  @Input() cv = {} as IRecruiterFindCv;
  constructor() { }

  ngOnInit(): void {
  }

}

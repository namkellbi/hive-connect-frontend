import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterFindCvItemComponent } from './recruiter-find-cv-item.component';

describe('RecruiterFindCvItemComponent', () => {
  let component: RecruiterFindCvItemComponent;
  let fixture: ComponentFixture<RecruiterFindCvItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterFindCvItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterFindCvItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

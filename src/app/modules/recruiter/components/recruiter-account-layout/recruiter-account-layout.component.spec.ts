import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterAccountLayoutComponent } from './recruiter-account-layout.component';

describe('RecruiterAccountLayoutComponent', () => {
  let component: RecruiterAccountLayoutComponent;
  let fixture: ComponentFixture<RecruiterAccountLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterAccountLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterAccountLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

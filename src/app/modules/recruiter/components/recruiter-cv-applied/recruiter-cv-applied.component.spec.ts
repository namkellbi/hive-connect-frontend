import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCvAppliedComponent } from './recruiter-cv-applied.component';

describe('RecruiterCvAppliedComponent', () => {
  let component: RecruiterCvAppliedComponent;
  let fixture: ComponentFixture<RecruiterCvAppliedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterCvAppliedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCvAppliedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

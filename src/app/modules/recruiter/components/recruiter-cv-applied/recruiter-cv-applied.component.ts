import { Location } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MAP_JOB_APPLY_STATE, MAP_JOB_STYLE_NAME } from '@constant/form';
import { JobApplyState, JobDetail } from '@interfaces/job';
import { IChangeApplyRequestState, IRequestPagination } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ICVApply, IRecruiterProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BehaviorSubject, combineLatest, filter, Observable, Subject, takeUntil, switchMap, map, finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';
import { RecruiterCvConfirmModalComponent } from '../recruiter-cv-confirm-modal/recruiter-cv-confirm-modal.component';

@Component({
  selector: 'hc-recruiter-cv-applied',
  templateUrl: './recruiter-cv-applied.component.html',
  styleUrls: ['./recruiter-cv-applied.component.scss']
})
export class RecruiterCvAppliedComponent implements OnInit, OnDestroy {
  @Input() jobId = 0;
  @Input() profile = {} as IRecruiterProfile;
  readonly DEFAULT_PAGINATION = DEFAULT_PAGINATION;
  request = new BehaviorSubject<IRequestPagination>({ ...DEFAULT_PAGINATION });
  id = new BehaviorSubject<number>(0);
  totalPage = 0;
  cv$ = new Observable<ICVApply[]>();
  loading = false;
  applyLoading = false;
  jobDetail = {} as JobDetail;
  readonly APPLY_STATE = JobApplyState;
  destroy$ = new Subject();
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private activatedRoute: ActivatedRoute,
    private storeService: StoreService,
    private _location: Location,
    private router: Router,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.cv$ = combineLatest([
      this.request.asObservable(),
      this.activatedRoute.params,
      this.storeService.observerUserInfoRecruiter()
    ])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, params]) => !!params['id']),
        switchMap(([request, params, profile]) => {
          this.profile = profile.profile;
          this.loading = true;
          this.getJobDetail(+params['id']);
          return this.apiService.getCvAppliedJob(params['id'], request)
            .pipe(
              finalize(() => this.loading = false)
            );
        }),
        map(response => response.data?.data || [])
      )
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  getJobDetail(id: number, candidateId: number = -1) {
    this.loading = true;
    this.apiService.getJobDetail(id, candidateId)
      .pipe(
        finalize(() => this.loading = false),

      )
      .subscribe({
        next: (response) => {
          this.jobDetail = this.helperService.callApiResponseHandler(response);
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  pageChanged({ itemsPerPage, page }: PageChangedEvent) {
    if (this.loading) {
      return;
    }

    this.request.next({ pageNo: page, pageSize: itemsPerPage });
  }

  openDialogMessage(cv: ICVApply, state: JobApplyState) {
    const modal = this.modalService.show(RecruiterCvConfirmModalComponent, {
      class: '',
      initialState: {
        cv,
        state
      }
    });
    modal.content?.onClose.subscribe(data => {
      if (data) {
        cv.approvalStatus = state;
      }
      modal.hide();
    })
  }

  previewCV(item: ICVApply) {
    if (item.cvUrl && item.cvUrl.includes('*')) {
      this.previewFileCv(item);
      return;
    }
    const params = {
      "cvId": item.candidateId,
      "viewerId": this.profile.id,
      "candidateId": item.candidateId
    };
    this.apiService.updateViewerCV(params).subscribe({
      next: response => {
        this.router.navigate(['/recruiter', item.cvId, 'cv']);
      },
      error: error => console.log(error)
    })
  }

  previewFileCv(item: ICVApply) {
    this.loading = true;
    this.apiService.seeUploadCvDetail({
      candidateId: item.candidateId,
      jobId: item.jobId,
      recruiterId: this.profile.id
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            console.log(response.data);
            const el = document.createElement('a');
            el.href = response.data;
            el.target = '_blank';
            el.click();
          } else {
            this.helperService.showError('', response.message || 'Xem cv thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get map() {
    return MAP_JOB_APPLY_STATE
  }
  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME;
  }

  back() {
    this._location.back();
  }
}

import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobRecruiterView } from '@interfaces/job';
import { PaymentPackageDetail } from '@interfaces/payment';
import { IRequestPagination } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { BehaviorSubject, catchError, combineLatest, filter, finalize, map, Observable, of, startWith, Subject, switchMap, takeUntil, tap } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';
import { RecruiterConfigBodyJobComponent } from '../recruiter-config-body-job/recruiter-config-body-job.component';

@Component({
  selector: 'hc-recruiter-config-body',
  templateUrl: './recruiter-config-body.component.html',
  styleUrls: ['./recruiter-config-body.component.scss']
})
export class RecruiterConfigBodyComponent implements OnInit {
  @ViewChild(RecruiterConfigBodyJobComponent) jobBody!: RecruiterConfigBodyJobComponent;
  @Output() onLoading = new EventEmitter();
  @Output() selectedPackageId = new EventEmitter();
  detail$ = new Observable<PaymentPackageDetail>();
  request$ = new BehaviorSubject<IRequestPagination>({ ...DEFAULT_PAGINATION });
  jobs: JobRecruiterView[] = [];
  totalJob = 0;
  loadingJob = false;
  loadingPackage = false;
  jobName = '';
  selectedJob = {} as JobRecruiterView;
  idPackage = 0;
  reload$ = new Subject();
  destroy$ = new Subject();
  constructor(
    private activatedRoute: ActivatedRoute,
    private helperService: HelperService,
    private apiService: ApiService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.initDetail();
    this.initJob();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  initDetail() {
    this.detail$ = combineLatest([
      this.activatedRoute.params.pipe(tap(data => console.log(data['id']))),
      this.storeService.observerUserInfoRecruiter(),
      this.reload$.pipe(startWith(''))
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([{ id }, { profile: { id: recruiterId } }]) => {
          if (!id) return of({} as PaymentPackageDetail);
          this.onLoading.emit(true);
          this.loadingPackage = true;
          return this.apiService.getDetailPurchasePackage(id, recruiterId)
            .pipe(
              finalize(() => {
                this.onLoading.emit(false);
                this.loadingPackage = false;
              }),
              map(this.helperService.callApiResponseHandler),
              map(({ jobPurchasedPPRes, ...data }) => jobPurchasedPPRes ? ({
                ...data,
                jobPurchasedPPRes: {
                  ...jobPurchasedPPRes,
                  jobId: jobPurchasedPPRes.id || jobPurchasedPPRes.jobId || 0
                }
              }) : ({
                ...data,
                jobPurchasedPPRes
              })),
              catchError(err => {
                this.helperService.callApiFailedHandler(err);
                this.onLoading.emit(false);
                this.loadingPackage = false;
                return of({} as PaymentPackageDetail);
              })
            );
        }),
        tap(data => {
          if (data.jobPurchasedPPRes) {
            this.selectedJob = data.jobPurchasedPPRes;
          }
        }),
      );
  }

  initJob() {
    combineLatest([
      this.storeService.observerUserInfoRecruiter(),
      this.request$
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([{ profile }, request]) => {
          this.loadingJob = true;
          return this.apiService.getRecruiterJobCreate({ ...request, recruiterId: profile.id, jobName: this.jobName })
            .pipe(
              finalize(() => this.loadingJob = false)
            );
        })
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.jobs.push(...data.data);
            this.totalJob = data.pagination.totalRecords;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  nextPageJob() {
    const request = this.request$.getValue();
    request.pageNo += 1;
    this.request$.next(request);
  }

  searchJob(name: string) {
    this.jobName = name;
    this.jobs = [];
    this.request$.next({ ...DEFAULT_PAGINATION });
  }

  selectJob(job: JobRecruiterView) {
    this.selectedJob = job;
  }
}

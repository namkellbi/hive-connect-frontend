import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterConfigBodyComponent } from './recruiter-config-body.component';

describe('RecruiterConfigBodyComponent', () => {
  let component: RecruiterConfigBodyComponent;
  let fixture: ComponentFixture<RecruiterConfigBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterConfigBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterConfigBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

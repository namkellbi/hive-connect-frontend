import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCvConfirmModalComponent } from './recruiter-cv-confirm-modal.component';

describe('RecruiterCvConfirmModalComponent', () => {
  let component: RecruiterCvConfirmModalComponent;
  let fixture: ComponentFixture<RecruiterCvConfirmModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterCvConfirmModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCvConfirmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

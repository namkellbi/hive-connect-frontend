import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { JobApplyState } from '@interfaces/job';
import { IChangeApplyRequestState } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ICVApply } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-recruiter-cv-confirm-modal',
  templateUrl: './recruiter-cv-confirm-modal.component.html',
  styleUrls: ['./recruiter-cv-confirm-modal.component.scss']
})
export class RecruiterCvConfirmModalComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  cv = {} as ICVApply;
  state: JobApplyState = JobApplyState.approve;
  loading = false;
  message = 'Cảm ơn bạn đã ứng tuyển vào công việc của chúng tôi';
  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
  }

  close(data?: any) {
    this.onClose.emit(data);
  }

  changeRequestApplyState(cv: ICVApply, state: JobApplyState) {
    this.loading = true;
    this.apiService.approveJob(this.getRequestApprove(cv, state))
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            cv.approvalStatus = state;
            let message = 'Chấp nhận thành công';
            if (state === JobApplyState.reject) {
              message = 'Từ chối thành công'
            }
            this.helperService.showSuccess('', message);
            this.close(response)
          } else {
            let message = 'Chấp nhận thất bại';
            if (state === JobApplyState.reject) {
              message = 'Từ chối thất bại'
            }
            this.helperService.showError('', response.message || message)
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })

  }
  getRequestApprove(cv: ICVApply, state: JobApplyState): IChangeApplyRequestState {
    return {
      approvalStatus: state,
      candidateId: cv.candidateId,
      createdAt: new Date().toISOString(),
      jobId: cv.jobId,
      updatedAt: new Date().toISOString(),
      message: this.message
    }
  }
  get messageConfirm() {
    return {
      [JobApplyState.approve]: 'Bạn có chắc muốn đồng ý với hồ sơ ứng tuyển này không?',
      [JobApplyState.pending]: '',
      [JobApplyState.reject]: 'Bạn có chắc muốn từ chối hồ sơ ứng tuyển này không?',
    }[this.cv.approvalStatus]
  }
}

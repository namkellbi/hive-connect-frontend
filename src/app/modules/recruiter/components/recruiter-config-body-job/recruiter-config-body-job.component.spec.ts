import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterConfigBodyJobComponent } from './recruiter-config-body-job.component';

describe('RecruiterConfigBodyJobComponent', () => {
  let component: RecruiterConfigBodyJobComponent;
  let fixture: ComponentFixture<RecruiterConfigBodyJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterConfigBodyJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterConfigBodyJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobRecruiterView } from '@interfaces/job';
import { PaymentPackageDetail, PaymentRental } from '@interfaces/payment';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-recruiter-config-body-job',
  templateUrl: './recruiter-config-body-job.component.html',
  styleUrls: ['./recruiter-config-body-job.component.scss']
})
export class RecruiterConfigBodyJobComponent implements OnInit {
  @Input() selectedJob = {} as JobRecruiterView;
  @Input() jobs: JobRecruiterView[] = [];
  @Input() detail = {} as PaymentPackageDetail;
  @Output() selectedJobChange = new EventEmitter<JobRecruiterView>();
  @Output() nextPage = new EventEmitter();
  @Output() searchJob = new EventEmitter<string>();
  modalRef!: BsModalRef;
  loading = false;
  constructor(
    private modalService: BsModalService,
    private apiService: ApiService,
    private helperService: HelperService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  openConfirmModal(temp: any) {
    this.modalRef = this.modalService.show(temp, {
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  applyPackageToJob() {
    this.modalRef.hide();
    this.loading = true;
    const request = {
      id: this.activatedRoute.snapshot.params['id'],
      jobId: this.selectedJob.jobId
    }
    this.apiService.applyPackageToJob(request)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, message }) => {
          if (status === ResponseStatus.success) {
            this.detail.jobPurchasedPPRes = this.selectedJob;
            this.helperService.showSuccess('', 'Sử dụng gói thành công');
          } else {
            this.helperService.showError('', message || 'Sử dụng gói thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get paymentRental() {
    return PaymentRental;
  }

  get normalPaymentPackage() {
    return this.detail.infoPPRes?.normalPaymentPackage;
  }

  get wrapperClass() {
    return {
      [PaymentRental.advertisement]: 'advertisement',
      [PaymentRental.banner]: 'banner',
      [PaymentRental.candidate]: 'candidate'
    }[this.normalPaymentPackage.groupPackageId]
  }
}

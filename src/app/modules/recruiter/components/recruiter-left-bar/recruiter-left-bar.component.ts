import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterAccountInfo, IRecruiterProfile, RecruiterInfoCheck } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { IUserObserver, StoreService } from '@services/store.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { finalize, Observable, tap } from 'rxjs';

@Component({
  selector: 'hc-recruiter-left-bar',
  templateUrl: './recruiter-left-bar.component.html',
  styleUrls: ['./recruiter-left-bar.component.scss']
})
export class RecruiterLeftBarComponent implements OnInit {
  @ViewChild('verifyTemp') tempNoti!: TemplateRef<any>;
  accountInfo = {} as IRecruiterAccountInfo;
  user$ = new Observable<IUserObserver>();
  profile$ = new Observable<IRecruiterProfile>();
  loadingInfo = false;
  message = '';
  firstTime = true;
  isOpen = false;
  constructor(
    private storeService: StoreService,
    private apiService: ApiService,
    private helperService: HelperService,
    private router: Router,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.user$ = this.storeService.observerUser();
    this.profile$ = this.storeService.observerRecruiterProfile().pipe(
      tap(({ id }) => id && (this.getAccountInfo(id)))
    );
  }

  getAccountInfo(id: number) {
    this.loadingInfo = true;
    this.apiService.getRecruiterAccountInfo(id)
      .pipe(
        finalize(() => this.loadingInfo = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.accountInfo = data;
            this.message = data.message;
            if (this.firstTime) {
              this.isOpen = true;
              this.firstTime = false;
              setTimeout(() => {
                this.isOpen = false;
              }, 5000);
            }
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get notiClass() {
    if (!this.accountInfo.verifyStep) return 'error-msg';
    const step = +this.accountInfo.verifyStep.split('/')[0];
    return step === 4 ? 'success-msg' : 'error-msg'
  }

  get btnLabel() {
    if (!this.accountInfo.uncheck) return 'Đăng tin';
    return {
      [RecruiterInfoCheck.email]: 'Xác thực email',
      [RecruiterInfoCheck.company]: 'Xác thực công ty',
      [RecruiterInfoCheck.phone]: 'Xác thực số điện thoại',
      [RecruiterInfoCheck.license]: 'Cập nhật giấy phép kinh doanh',
      [RecruiterInfoCheck.verified]: 'Đăng tin'
    }[this.accountInfo.uncheck] || ''
  }

  resolveUrl() {
    return {
      [RecruiterInfoCheck.email]: '/auth/register?step=3',
      [RecruiterInfoCheck.company]: '/recruiter/account/company',
      [RecruiterInfoCheck.phone]: '/recruiter/account/info',
      [RecruiterInfoCheck.license]: '/recruiter/account/company',
      [RecruiterInfoCheck.verified]: '/recruiter/job/create'
    }[this.accountInfo.uncheck] || '/recruiter/job/create'
  }

  verify() {
    this.router.navigateByUrl(this.resolveUrl());
  }
}

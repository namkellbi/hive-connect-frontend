import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterLeftBarComponent } from './recruiter-left-bar.component';

describe('RecruiterLeftBarComponent', () => {
  let component: RecruiterLeftBarComponent;
  let fixture: ComponentFixture<RecruiterLeftBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterLeftBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterLeftBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

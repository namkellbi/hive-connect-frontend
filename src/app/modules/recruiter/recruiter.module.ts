import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormModule } from '@components/form/form.module';
import { ModalModule } from '@components/modal/modal.module';
import { ExportCvModule } from '@pages/user-profile/page/export-cv/export-cv.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ComponentsModule } from 'src/app/core/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RecruiterAccountLayoutComponent } from './components/recruiter-account-layout/recruiter-account-layout.component';
import { RecruiterCompanyRequestListComponent } from './components/recruiter-company-request-list/recruiter-company-request-list.component';
import { RecruiterConfigBannerItemComponent } from './components/recruiter-config-baner-item/recruiter-config-baner-item.component';
import { RecruiterConfigBodyBannerComponent } from './components/recruiter-config-body-banner/recruiter-config-body-banner.component';
import { RecruiterConfigBodyJobComponent } from './components/recruiter-config-body-job/recruiter-config-body-job.component';
import { RecruiterConfigBodyComponent } from './components/recruiter-config-body/recruiter-config-body.component';
import { RecruiterConfigHeaderComponent } from './components/recruiter-config-header/recruiter-config-header.component';
import { RecruiterCreateJobComponent } from './components/recruiter-create-job/recruiter-create-job.component';
import { RecruiterCvAppliedComponent } from './components/recruiter-cv-applied/recruiter-cv-applied.component';
import { RecruiterFindCvItemComponent } from './components/recruiter-find-cv-item/recruiter-find-cv-item.component';
import { RecruiterLayoutComponent } from './components/recruiter-layout/recruiter-layout.component';
import { RecruiterLeftBarComponent } from './components/recruiter-left-bar/recruiter-left-bar.component';
import { RecruiterPaymentPackageComponent as RecruiterPaymentPackageItemComponent } from './components/recruiter-payment-package/recruiter-payment-package.component';
import { RecruiterRequestCompanyListComponent } from './components/recruiter-request-company-list/recruiter-request-company-list.component';
import { FindCvComponent } from './pages/find-cv/find-cv.component';
import { RecrtuiterHomepagePackageComponent } from './pages/recrtuiter-homepage-package/recrtuiter-homepage-package.component';
import { RecruiterAccountCompanyComponent } from './pages/recruiter-account-company/recruiter-account-company.component';
import { RecruiterAccountInfoComponent } from './pages/recruiter-account-info/recruiter-account-info.component';
import { RecruiterAccountPasswordComponent } from './pages/recruiter-account-password/recruiter-account-password.component';
import { RecruiterCvDetailComponent } from './pages/recruiter-cv-detail/recruiter-cv-detail.component';
import { RecruiterDashboardComponent } from './pages/recruiter-dashboard/recruiter-dashboard.component';
import { RecruiterHomepageFeatureComponent } from './pages/recruiter-homepage-feature/recruiter-homepage-feature.component';
import { RecruiterInfoUpdateComponent } from './pages/recruiter-info-update/recruiter-info-update.component';
import { RecruiterIntroductionComponent } from './pages/recruiter-introduction/recruiter-introduction.component';
import { RecruiterJobsComponent } from './pages/recruiter-jobs/recruiter-jobs.component';
import { RecruiterPaymentConfigComponent } from './pages/recruiter-payment-config/recruiter-payment-config.component';
import { RecruiterPaymentDetailBannerComponent } from './pages/recruiter-payment-detail-banner/recruiter-payment-detail-banner.component';
import { RecruiterPaymentPackageDetailMainComponent } from './pages/recruiter-payment-package-detail-main/recruiter-payment-package-detail-main.component';
import { RecruiterPaymentPackageDetailComponent } from './pages/recruiter-payment-package-detail/recruiter-payment-package-detail.component';
import { RecruiterPaymentPackageComponent } from './pages/recruiter-payment-package/recruiter-payment-package.component';
import { RecruiterPaymentTransactionComponent } from './pages/recruiter-payment-transaction/recruiter-payment-transaction.component';
import { RecruiterRoutingModule } from './recruiter-routing.module';
import { RecruiterCvConfirmModalComponent } from './components/recruiter-cv-confirm-modal/recruiter-cv-confirm-modal.component';
import { RecruiterAccountCompanyLayoutComponent } from './pages/recruiter-account-company-layout/recruiter-account-company-layout.component';
import { RecruiterCompanyPersonalComponent } from './pages/recruiter-company-personal/recruiter-company-personal.component';
import { CreateJobResolver } from 'src/app/core/resolver/create-job.resolver';
@NgModule({
  declarations: [
    RecruiterLayoutComponent,
    RecruiterDashboardComponent,
    RecruiterLeftBarComponent,
    RecruiterJobsComponent,
    RecruiterCvAppliedComponent,
    RecruiterCreateJobComponent,
    RecruiterHomepageFeatureComponent,
    RecruiterAccountInfoComponent,
    RecruiterAccountCompanyComponent,
    RecruiterAccountPasswordComponent,
    RecruiterAccountLayoutComponent,
    RecruiterPaymentPackageComponent,
    RecruiterPaymentTransactionComponent,
    RecruiterPaymentPackageItemComponent,
    RecruiterPaymentPackageDetailComponent,
    RecruiterInfoUpdateComponent,
    RecruiterIntroductionComponent,
    RecruiterRequestCompanyListComponent,
    RecruiterPaymentConfigComponent,
    RecruiterConfigHeaderComponent,
    RecruiterConfigBodyComponent,
    RecruiterConfigBodyJobComponent,
    RecruiterConfigBodyBannerComponent,
    RecruiterConfigBannerItemComponent,
    FindCvComponent,
    RecruiterFindCvItemComponent,
    RecruiterCompanyRequestListComponent,
    RecruiterPaymentPackageDetailMainComponent,
    RecruiterPaymentDetailBannerComponent,
    RecruiterCvDetailComponent,
    RecrtuiterHomepagePackageComponent,
    RecruiterCvConfirmModalComponent,
    RecruiterAccountCompanyLayoutComponent,
    RecruiterCompanyPersonalComponent,
  ],
  imports: [
    CommonModule,
    RecruiterRoutingModule,
    SharedModule,
    ComponentsModule,
    FormModule,
    ModalModule,
    InfiniteScrollModule,
    ExportCvModule
  ],
  providers: [CreateJobResolver]
})
export class RecruiterModule { }

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobRecruiterView } from '@interfaces/job';
import { Banner, IPackage, PackageType, PaymentRental } from '@interfaces/payment';
import { IRequestPagination } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { PaymentService } from '@services/payment.service';
import { StoreService } from '@services/store.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BehaviorSubject, combineLatest, filter, finalize, map, Subject, switchMap, takeUntil } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-recruiter-payment-package-detail',
  templateUrl: './recruiter-payment-package-detail.component.html',
  styleUrls: ['./recruiter-payment-package-detail.component.scss']
})
export class RecruiterPaymentPackageDetailComponent implements OnInit, OnDestroy {
  request = { ...DEFAULT_PAGINATION };
  request$ = new BehaviorSubject<IRequestPagination>({ ...DEFAULT_PAGINATION });
  totalPage = 0;
  listJobs: JobRecruiterView[] = [];
  selectedJob = {} as JobRecruiterView;
  totalJob = 0;
  package = {} as IPackage;
  banner = {} as Banner;
  loading = true;
  destroy$ = new Subject();
  jobId = 0;
  loadingJob = false;
  jobName = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private helperService: HelperService,
    private paymentService: PaymentService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$),
        filter(params => !!params['id'] && !!params['rentalId']),
        map(params => [+params['id'], +params['rentalId']])
      )
      .subscribe(([id, rentalId]) => this.getDetailPackage(id, rentalId));
    ({ jobId: this.jobId } = this.activatedRoute.snapshot.queryParams);
    this.initJob();
  }



  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  initJob() {
    combineLatest([
      this.storeService.observerUserInfoRecruiter(),
      this.request$
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([{ profile }, request]) => {
          this.loadingJob = true;
          return this.apiService.getRecruiterJobCreate({ ...request, recruiterId: profile.id, jobName: this.jobName })
            .pipe(
              finalize(() => this.loadingJob = false)
            );
        })
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.listJobs.push(...data.data);
            this.totalJob = data.pagination.totalRecords;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  searchJob(name: string) {
    this.jobName = name;
    this.listJobs = [];
    this.request$.next({ ...DEFAULT_PAGINATION });
  }

  getDetailPackage(id: number, rentalId: number) {
    this.loading = true;
    this.apiService.getDetailPackage(id, rentalId)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.package = data.normalPackage;
            this.banner = data.bannerPackage;
          } else {
            this.helperService.showError('', message || 'Không thể lấy được thông tin gói hàng');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get paymentRental() {
    return PaymentRental;
  }

  get packageType() {
    return {
      [PackageType.basic]: "Basic",
      [PackageType.classic]: "Classic",
      [PackageType.gold]: "Gold",
    }
  }

  get wrapperClass() {
    return {
      [PaymentRental.advertisement]: 'advertisement',
      [PaymentRental.banner]: 'banner',
      [PaymentRental.candidate]: 'candidate'
    }[this.package?.rentalPackageId || this.banner?.rentalPackageId]
  }
  isRequiredCheckJob() {
    return this.package?.id && this.package.rentalPackageId === PaymentRental.advertisement;
  }
  buyPackage() {
    if (this.isRequiredCheckJob()) {
      if ((!this.jobId && !this.selectedJob.jobId)) {
        this.helperService.showError('', 'Bạn phải chọn công việc muốn áp dụng để tiếp tục');
        return;
      }
    }
    this.loading = true;
    this.getBuyRequest()
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => console.log(response)
      });
  }

  getBuyRequest() {
    const { jobId } = this.activatedRoute.snapshot.queryParams;
    return this.banner?.id ? this.paymentService.buyBannerPackage(this.banner) : this.paymentService.buyPackage({ ...this.package, jobId: jobId || this.selectedJob.jobId });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterPaymentPackageDetailComponent } from './recruiter-payment-package-detail.component';

describe('RecruiterPaymentPackageDetailComponent', () => {
  let component: RecruiterPaymentPackageDetailComponent;
  let fixture: ComponentFixture<RecruiterPaymentPackageDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterPaymentPackageDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterPaymentPackageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

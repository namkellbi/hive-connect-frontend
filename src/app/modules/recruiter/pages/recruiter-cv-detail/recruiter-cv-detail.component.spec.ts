import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCvDetailComponent } from './recruiter-cv-detail.component';

describe('RecruiterCvDetailComponent', () => {
  let component: RecruiterCvDetailComponent;
  let fixture: ComponentFixture<RecruiterCvDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterCvDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCvDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

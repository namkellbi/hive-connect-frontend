import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseStatus } from '@interfaces/response';
import { ICandidateCV, ICandidateProfile, IUser } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { catchError, combineLatest, finalize, map, Observable, of, Subject, switchMap, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'hc-recruiter-cv-detail',
  templateUrl: './recruiter-cv-detail.component.html',
  styleUrls: ['./recruiter-cv-detail.component.scss']
})
export class RecruiterCvDetailComponent implements OnInit {
  loading = false;
  user = {} as IUser;
  profile = {} as ICandidateProfile;
  cv$ = new Observable<ICandidateCV>();
  destroy$ = new Subject;
  message = 'Xem đầy đủ thông tin CV';
  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute,
    private helperService: HelperService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.cv$ = combineLatest([
      this.activatedRoute.params,
      this.storeService.observerUserInfoRecruiter()
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([params, { info, profile }]) => {
          this.loading = true;
          return this.apiService.getCvForRecruiter(profile.id, params['cvId'])
            .pipe(
              finalize(() => this.loading = false),
              tap(data => this.message = data.message),
              map(response => this.helperService.callApiResponseHandler(response)),
              tap((data) => {
                if (data.candidateId) {
                  this.user = {
                    avatar: data.avatarUrl,
                    email: data.email,
                    phone: data.phoneNumber
                  } as IUser;
                  this.profile = {
                    fullName: data.fullName,
                    birthDate: data.birthDate,
                    socialLink: data.socialLink,
                    address: data.address
                  } as ICandidateProfile
                }
              }),
              catchError(error => {
                this.helperService.callApiFailedHandler(error);
                return of({} as ICandidateCV);
              })
            );
        })
      )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  toPaymentPage() {
    if (this.message === 'Mua gói để xem thông tin liên hệ của ứng viên') {
      this.router.navigate(['/recruiter', 'payment', 'package']);
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterPaymentTransactionComponent } from './recruiter-payment-transaction.component';

describe('RecruiterPaymentTransactionComponent', () => {
  let component: RecruiterPaymentTransactionComponent;
  let fixture: ComponentFixture<RecruiterPaymentTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterPaymentTransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterPaymentTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { ITransaction } from '@interfaces/payment';
import { ResponseStatus } from '@interfaces/response';
import { HelperService } from '@services/helper.service';
import { PaymentService } from '@services/payment.service';
import { PageChangedEvent, PagerComponent, PaginationComponent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-recruiter-payment-transaction',
  templateUrl: './recruiter-payment-transaction.component.html',
  styleUrls: ['./recruiter-payment-transaction.component.scss']
})
export class RecruiterPaymentTransactionComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent!: PagerComponent;
  loading = false;
  transactions: ITransaction[] = [];
  filter = {
    transactionCode: '',
    ...DEFAULT_PAGINATION,
    pageSize: 10
  };
  numPages = 1;
  totals = 0;
  constructor(
    private paymentService: PaymentService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.getTransaction();
  }

  getTransaction() {
    this.loading = true;
    this.paymentService.getTransaction(this.filter)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.transactions = data.data;
            this.totals = data.pagination.totalRecords;
          } else {
            this.helperService.showError('', message || 'Lấy lịch sử giao dịch thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
  }
  pageChanged({ itemsPerPage, page }: PageChangedEvent) {
    this.filter.pageNo = page;
    this.getTransaction();
  }
}

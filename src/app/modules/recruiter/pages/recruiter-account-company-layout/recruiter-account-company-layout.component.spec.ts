import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterAccountCompanyLayoutComponent } from './recruiter-account-company-layout.component';

describe('RecruiterAccountCompanyLayoutComponent', () => {
  let component: RecruiterAccountCompanyLayoutComponent;
  let fixture: ComponentFixture<RecruiterAccountCompanyLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterAccountCompanyLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterAccountCompanyLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ICompany } from '@interfaces/job';
import { StoreService } from '@services/store.service';

@Component({
  selector: 'hc-recruiter-account-company-layout',
  templateUrl: './recruiter-account-company-layout.component.html',
  styleUrls: ['./recruiter-account-company-layout.component.scss']
})
export class RecruiterAccountCompanyLayoutComponent implements OnInit {
  company = {} as ICompany;
  loadingCompany = true;
  recruiterId = 0;
  constructor(
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.recruiterId = this.storeService.getRecruiterProfile()?.id;
  }

}

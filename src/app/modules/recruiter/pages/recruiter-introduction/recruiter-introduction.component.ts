import { Component, OnInit } from '@angular/core';
import { NavigationService } from '@services/navigation.service';
import { SearchService } from '@services/search.service';

@Component({
  selector: 'hc-recruiter-introduction',
  templateUrl: './recruiter-introduction.component.html',
  styleUrls: ['./recruiter-introduction.component.scss']
})
export class RecruiterIntroductionComponent implements OnInit {

  constructor(
    private navigationService: NavigationService,
    private searchService: SearchService
  ) { }

  ngOnInit(): void {
    this.navigationService.check$.next(null);
    this.searchService.setStateSearchBar(false);
  }

}

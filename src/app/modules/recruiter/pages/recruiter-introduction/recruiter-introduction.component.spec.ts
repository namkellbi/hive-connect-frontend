import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterIntroductionComponent } from './recruiter-introduction.component';

describe('RecruiterIntroductionComponent', () => {
  let component: RecruiterIntroductionComponent;
  let fixture: ComponentFixture<RecruiterIntroductionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterIntroductionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterIntroductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

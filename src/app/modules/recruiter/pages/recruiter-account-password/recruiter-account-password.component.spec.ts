import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterAccountPasswordComponent } from './recruiter-account-password.component';

describe('RecruiterAccountPasswordComponent', () => {
  let component: RecruiterAccountPasswordComponent;
  let fixture: ComponentFixture<RecruiterAccountPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterAccountPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterAccountPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit } from '@angular/core';
import { SearchService } from '@services/search.service';

@Component({
  selector: 'hc-recrtuiter-homepage-package',
  templateUrl: './recrtuiter-homepage-package.component.html',
  styleUrls: ['./recrtuiter-homepage-package.component.scss']
})
export class RecrtuiterHomepagePackageComponent implements OnInit, OnDestroy {

  constructor(
    private searchService: SearchService
  ) { }

  ngOnInit(): void {
    this.searchService.setStateSearchBar(false);
    document.body.classList.add('bg-grey');
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    document.body.classList.remove('bg-grey');
  }
}

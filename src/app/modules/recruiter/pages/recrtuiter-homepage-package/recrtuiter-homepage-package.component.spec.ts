import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecrtuiterHomepagePackageComponent } from './recrtuiter-homepage-package.component';

describe('RecrtuiterHomepagePackageComponent', () => {
  let component: RecrtuiterHomepagePackageComponent;
  let fixture: ComponentFixture<RecrtuiterHomepagePackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecrtuiterHomepagePackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecrtuiterHomepagePackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

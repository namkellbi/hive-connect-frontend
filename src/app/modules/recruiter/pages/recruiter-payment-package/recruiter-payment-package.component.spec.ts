import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterPaymentPackageComponent } from './recruiter-payment-package.component';

describe('RecruiterPaymentPackageComponent', () => {
  let component: RecruiterPaymentPackageComponent;
  let fixture: ComponentFixture<RecruiterPaymentPackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterPaymentPackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterPaymentPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

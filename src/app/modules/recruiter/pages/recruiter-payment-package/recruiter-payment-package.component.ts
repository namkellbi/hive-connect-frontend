import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPackage, IRentalPackage, PaymentRental } from '@interfaces/payment';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-recruiter-payment-package',
  templateUrl: './recruiter-payment-package.component.html',
  styleUrls: ['./recruiter-payment-package.component.scss']
})
export class RecruiterPaymentPackageComponent implements OnInit {
  listRentalPackage: IRentalPackage[] = [];
  filter = {
    name: '',
    rentalPackageId: PaymentRental.candidate,
    ...DEFAULT_PAGINATION,
  }
  package: IPackage[] = [];
  loading = false;
  totals = 0;
  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.configFilter();
    this.getListPackage();
    this.getListRentalPackage();
  }

  configFilter() {
    const { jobId } = this.activatedRoute.snapshot.queryParams;
    if (jobId) {
      this.filter.rentalPackageId = PaymentRental.advertisement;
    }
  }

  getListRentalPackage() {
    this.apiService.listRentalPackage().subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.listRentalPackage = data;
        }
      }
    })
  }

  getListPackage() {
    this.loading = true;
    this.apiService.listPackage(this.filter)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.package.push(...data.data);
            this.totals = data.pagination.totalRecords;
          }
        }
      });
  }
  search() {
    this.filter.pageNo = 1;
    this.package = [];
    this.getListPackage();
  }

  loadMore() {
    if (this.totals > this.package.length && !this.loading) {
      this.filter.pageNo += 1;
      this.getListPackage();
    }
  }
  get isShowTitle() {
    return !location.pathname.includes('recruiter/package');
  }
}

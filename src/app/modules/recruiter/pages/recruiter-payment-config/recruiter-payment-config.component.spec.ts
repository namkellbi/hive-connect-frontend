import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterPaymentConfigComponent } from './recruiter-payment-config.component';

describe('RecruiterPaymentConfigComponent', () => {
  let component: RecruiterPaymentConfigComponent;
  let fixture: ComponentFixture<RecruiterPaymentConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterPaymentConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterPaymentConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { PurchasedPackage } from '@interfaces/payment';
import { RecruiterConfigHeaderComponent } from '@pages/recruiter/components/recruiter-config-header/recruiter-config-header.component';

@Component({
  selector: 'hc-recruiter-payment-config',
  templateUrl: './recruiter-payment-config.component.html',
  styleUrls: ['./recruiter-payment-config.component.scss']
})
export class RecruiterPaymentConfigComponent implements OnInit {
  @ViewChild(RecruiterConfigHeaderComponent) header!: RecruiterConfigHeaderComponent;
  loadingBody = false;
  package: PurchasedPackage[] = [];
  constructor() { }

  ngOnInit(): void {
  }
  packageBodyEventHandler($event: number) {
    if (this.header) {
      const selected = this.package.find(item => item.paymentId === $event);
      selected && (this.header.selectedPayment = selected);
    }
  }
}

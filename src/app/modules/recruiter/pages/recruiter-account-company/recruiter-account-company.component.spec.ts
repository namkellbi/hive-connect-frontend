import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterAccountCompanyComponent } from './recruiter-account-company.component';

describe('RecruiterAccountCompanyComponent', () => {
  let component: RecruiterAccountCompanyComponent;
  let fixture: ComponentFixture<RecruiterAccountCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterAccountCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterAccountCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

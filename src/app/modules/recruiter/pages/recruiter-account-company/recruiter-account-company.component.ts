import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CreateCompanyModalComponent } from '@components/modal/create-company-modal/create-company-modal.component';
import { IDropDownItem } from '@interfaces/form';
import { ICompany, JobApplyState } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { IFieldJob, IRecruiterProfile, IRequestJoinCompany, RequestJoinCompany } from '@interfaces/user';
import { RecruiterCompanyRequestListComponent } from '@pages/recruiter/components/recruiter-company-request-list/recruiter-company-request-list.component';
import { RecruiterRequestCompanyListComponent } from '@pages/recruiter/components/recruiter-request-company-list/recruiter-request-company-list.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { SearchService } from '@services/search.service';
import { UserService } from '@services/user.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { finalize, Observable, Subject, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'hc-recruiter-account-company',
  templateUrl: './recruiter-account-company.component.html',
  styleUrls: ['./recruiter-account-company.component.scss']
})
export class RecruiterAccountCompanyComponent implements OnInit {
  @Output() onLoadingCompany = new EventEmitter();
  @Output() companyInfo = new EventEmitter();
  profile$ = new Observable<IRecruiterProfile>();
  loading = false;
  loadingFile = false;
  company = {} as ICompany;
  requestJoinCompany = {} as IRequestJoinCompany;
  isView = true;
  fieldJob: IDropDownItem[] = [];
  destroy$ = new Subject();
  field = {} as IFieldJob;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private userService: UserService,
    private modalService: BsModalService,
    private searchService: SearchService,
  ) { }

  ngOnInit(): void {
    this.profile$ = this.userService.getRecruiterProfile()
      .pipe(
        tap(data => {
          if (data && data.id) {
            this.getCompanyInfo(data.companyId, data, true);
            this.getSentRequest(data.id);

          }
        })
      );
    this.searchService.observerJobField()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => this.fieldJob = data.map(item => ({ label: item.fieldName, value: item.id })));
  }

  getSentRequest(id: number) {
    this.apiService.checkSendRequestJoinCompany(id).subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.requestJoinCompany = data;
        }
      },
      error: error => this.helperService.callApiFailedHandler(error)
    })
  }

  get isJoinCompany() {
    return this.company?.creator || (!this.company?.creator && this.requestJoinCompany?.status === RequestJoinCompany.approve);
  }

  get getField() {
    return this.fieldJob.find(item => +item.value === +this.company.fieldWork)?.label || '';
  }

  get statusJoinLabel() {
    if (!this.requestJoinCompany || !this.requestJoinCompany.senderId || this.company.creator) return '';
    return {
      [RequestJoinCompany.approve]: 'Thành viên',
      [RequestJoinCompany.deny]: 'Bị từ chối',
      [RequestJoinCompany.pending]: 'Đang chờ xác nhận'
    }[this.requestJoinCompany.status]
  }

  get requestMessage() {
    return this.requestJoinCompany?.id ? `Hiện yêu cầu liên kết đến công ty <b>${this.requestJoinCompany.company.name}</b> đã được gửi, khi người phụ trách công ty kiểm duyệt bạn sẽ được liên kết đến
    công ty đó` : `Bạn chưa yêu liên kết công ty, hãy chọn công ty của bận hoặc tạo mới công ty.`
  }

  get requestCompanyStatus() { return RequestJoinCompany }

  getCompanyInfo(companyId: number, recruiter: IRecruiterProfile, checkView = false) {
    this.loading = true;
    this.onLoadingCompany.emit(true);
    this.apiService.getCompanyDetail(companyId, recruiter.id)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.onLoadingCompany.emit(false);
        })
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.company = data;
            this.companyInfo.emit(data);
            if (data.creator && !recruiter.approvedBusinessLicense && checkView) {
              this.isView = false;
            }
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  editCompanyInfo(profile: IRecruiterProfile) {
    this.isView = false;
  }

  newRequestJoinCompany(profile: IRecruiterProfile) {
    const modal = this.modalService.show(CreateCompanyModalComponent, {
      ignoreBackdropClick: true,
      initialState: {
        profile
      }
    });
    modal.content?.joinCompany.subscribe((data: IRequestJoinCompany) => {
      if (data) {
        this.getCompanyInfo(data.companyId, profile);
      }
      modal.hide();
    })
  }
  openCreateCompanyModal(profile: IRecruiterProfile) {
    const modal = this.modalService.show(RecruiterRequestCompanyListComponent, {
      ignoreBackdropClick: true,
      class: 'fit-content',
      initialState: {
        profile
      }
    });
    modal.content?.onClose.subscribe(value => {
      modal.hide();
      if (value && value.id && value.name) {
        this.getCompanyInfo(value.id, profile);
        this.userService.reloadRecruiterProfile();
      }
    })
  }
  createCompanyEventHandle($event: any, profile: IRecruiterProfile) {
    this.isView = true;
    if ($event) {
      this.getCompanyInfo(profile.companyId, profile);
    }
  }
}

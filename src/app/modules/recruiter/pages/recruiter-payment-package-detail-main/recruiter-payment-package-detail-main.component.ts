import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPackage, PaymentRental } from '@interfaces/payment';

@Component({
  selector: 'hc-recruiter-payment-package-detail-main',
  templateUrl: './recruiter-payment-package-detail-main.component.html',
  styleUrls: ['./recruiter-payment-package-detail-main.component.scss']
})
export class RecruiterPaymentPackageDetailMainComponent implements OnInit {
  @Input() package = {} as IPackage;
  @Input() loading = false;
  @Input() jobId = 0;
  @Output() onBuy = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  buyPackage() {
    this.onBuy.emit();
  }
  get paymentRental() {
    return PaymentRental;
  }
  get wrapperClass() {
    return {
      [PaymentRental.advertisement]: 'advertisement',
      [PaymentRental.banner]: 'banner',
      [PaymentRental.candidate]: 'candidate'
    }[this.package?.rentalPackageId]
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterPaymentPackageDetailMainComponent } from './recruiter-payment-package-detail-main.component';

describe('RecruiterPaymentPackageDetailMainComponent', () => {
  let component: RecruiterPaymentPackageDetailMainComponent;
  let fixture: ComponentFixture<RecruiterPaymentPackageDetailMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterPaymentPackageDetailMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterPaymentPackageDetailMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterAccountInfoComponent } from './recruiter-account-info.component';

describe('RecruiterAccountInfoComponent', () => {
  let component: RecruiterAccountInfoComponent;
  let fixture: ComponentFixture<RecruiterAccountInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterAccountInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterAccountInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

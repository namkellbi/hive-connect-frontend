import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@components/base-component/base.component';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { IUserObserver, StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { combineLatest, filter, finalize, map, Observable, switchMap, takeUntil, tap } from 'rxjs';
import { FileFinishPendingType } from 'src/app/shared/directives/file-handle.directive';

enum FileTypeUpload {
  avatar, license, additionalLicense
}

@Component({
  selector: 'hc-recruiter-account-info',
  templateUrl: './recruiter-account-info.component.html',
  styleUrls: ['./recruiter-account-info.component.scss']
})
export class RecruiterAccountInfoComponent extends BaseComponent implements OnInit {
  profile$ = new Observable<IRecruiterProfile>();
  user$ = new Observable<IUserObserver>();
  info$ = new Observable<{ profile: IRecruiterProfile, user: IUserObserver }>();
  loading = false;
  isEdit = false;
  fileUpload: { [key in FileTypeUpload]: FileFinishPendingType } = {
    [FileTypeUpload.additionalLicense]: {} as FileFinishPendingType,
    [FileTypeUpload.license]: {} as FileFinishPendingType,
    [FileTypeUpload.avatar]: {} as FileFinishPendingType
  }
  constructor(
    private storeService: StoreService,
    private userService: UserService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.profile$ = this.userService.getRecruiterProfile()
      .pipe(
        takeUntil(this.destroy$),
      );
    this.user$ = this.storeService.observerUser().pipe(
      takeUntil(this.destroy$),
      filter(data => data != null),
    );
    this.info$ = combineLatest([this.profile$, this.user$])
      .pipe(
        takeUntil(this.destroy$),
        map(([profile, user]) => ({ profile, user }))

      )
  }

  get fileTypeUpload() {
    return FileTypeUpload;
  }

  verifyPhone(phone: string) {
    this.userService.verifyPhone(phone);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterPaymentDetailBannerComponent } from './recruiter-payment-detail-banner.component';

describe('RecruiterPaymentDetailBannerComponent', () => {
  let component: RecruiterPaymentDetailBannerComponent;
  let fixture: ComponentFixture<RecruiterPaymentDetailBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterPaymentDetailBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterPaymentDetailBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

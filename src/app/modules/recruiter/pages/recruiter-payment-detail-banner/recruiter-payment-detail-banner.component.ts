import { Component, Input, OnInit } from '@angular/core';
import { Banner } from '@interfaces/payment';

@Component({
  selector: 'hc-recruiter-payment-detail-banner',
  templateUrl: './recruiter-payment-detail-banner.component.html',
  styleUrls: ['./recruiter-payment-detail-banner.component.scss']
})
export class RecruiterPaymentDetailBannerComponent implements OnInit {
  @Input() banner = {} as Banner;
  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@components/base-component/base.component';
import { getFullNameRegex, getRegexPhone } from '@constant/form';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { IUserObserver, StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { combineLatest, filter, finalize, map, Observable, switchMap, takeUntil, tap } from 'rxjs';
import { FileFinishPendingType } from 'src/app/shared/directives/file-handle.directive';
enum FileTypeUpload {
  avatar, license, additionalLicense
}

@Component({
  selector: 'hc-recruiter-info-update',
  templateUrl: './recruiter-info-update.component.html',
  styleUrls: ['./recruiter-info-update.component.scss']
})
export class RecruiterInfoUpdateComponent extends BaseComponent implements OnInit {
  @Output() onFinish = new EventEmitter();
  profile$ = new Observable<IRecruiterProfile>();
  user$ = new Observable<IUserObserver>();
  info$ = new Observable<{ profile: IRecruiterProfile, user: IUserObserver }>();
  form!: FormGroup;
  avatar = '';
  businessLicense = '';
  additionalLicense = '';
  loading = false;
  fileUpload: { [key in FileTypeUpload]: FileFinishPendingType } = {
    [FileTypeUpload.additionalLicense]: {} as FileFinishPendingType,
    [FileTypeUpload.license]: {} as FileFinishPendingType,
    [FileTypeUpload.avatar]: {} as FileFinishPendingType
  }
  constructor(
    private storeService: StoreService,
    private userService: UserService,
    private fb: FormBuilder,
    private helperService: HelperService,
    private apiService: ApiService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initForm();
    this.profile$ = this.userService.fetchRecruiterProfile()
      .pipe(
        takeUntil(this.destroy$),
        switchMap((response) => {
          if (response.status === ResponseStatus.success) {
            this.storeService.recruiterProfile.next(response.data);
          }
          return this.storeService.observerRecruiterProfile();
        }),
        tap(data => {
          if (data) {
            this.businessLicense = data.businessLicense;
            this.form.patchValue(data, { emitEvent: false });
          }
        })
      );
    this.user$ = this.storeService.observerUser().pipe(
      takeUntil(this.destroy$),
      filter(data => data != null),
      tap(data => {
        if (data) {
          this.avatar = data.avatar;
          this.form.patchValue({
            avatarUrl: data.avatar
          });
          if (data.verifiedPhone) {
            this.form.controls['phone']?.disable();
          }
        }
      })
    );
    this.info$ = combineLatest([this.profile$, this.user$])
      .pipe(
        takeUntil(this.destroy$),
        map(([profile, user]) => ({ profile, user }))
      )
  }

  initForm() {
    this.form = this.fb.group({
      id: ['', [Validators.required]],
      additionalLicense: ['', Validators.maxLength(500)],
      fullName: ['', [Validators.required, Validators.pattern(getFullNameRegex()), Validators.maxLength(50)]],
      phone: ['', [Validators.required, Validators.pattern(getRegexPhone()), Validators.maxLength(20)]],
      gender: ['', Validators.required],
      position: ['', [Validators.required, Validators.maxLength(50)]],
      linkedinAccount: ['', Validators.maxLength(255)],
      businessLicense: ['', Validators.maxLength(500)],
      avatarUrl: ['', Validators.maxLength(255)],
    });
  }

  uploadFinish($event: FileFinishPendingType[], type: FileTypeUpload) {
    const [file] = $event;
    if (!file || !file.result) {
      return;
    }
    if (type === FileTypeUpload.avatar) {
      this.avatar = file.result as string;
    }

    if (type === FileTypeUpload.license) {
      this.businessLicense = file.result as string;
    }
    if (type === FileTypeUpload.additionalLicense) {
      this.additionalLicense = file.result as string;
    }
    this.fileUpload[type] = file;
  }

  async submit({ profile, user }: { profile: IRecruiterProfile, user: IUserObserver }) {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    this.loading = true;
    const params = { ...this.form.getRawValue() };
    const avatarFile = this.fileUpload[FileTypeUpload.avatar].file;
    if (avatarFile) {
      params.avatarUrl = (await this.handleUploadAvatar(avatarFile)).url;
      this.apiService.updateProfile({
        id: user?.id,
        avatar: params.avatarUrl
      })
        .subscribe({
          next: response => {
            if (response.status === ResponseStatus.success) {
              this.helperService.showSuccess('', 'Cập nhật avatar thành công');
              this.storeService.saveAvatar(params.avatarUrl);
              this.userService.reloadRecruiterAccountVerify();
              this.onFinish.emit();
            }
          }
        })
    }
    params.businessLicense = '';
    this.apiService.updateRecruiterProfile(params)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ data, status, message }) => {
          if (status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Cập nhật thành công');
            this.storeService.updateRecruiterBasicInfo(data);
            this.onFinish.emit();
          } else {
            this.helperService.showError('', message || 'Cập nhật thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  async handleUploadAvatar(file: File) {
    return await this.helperService.uploadImage(file);
  }

  get fileTypeUpload() {
    return FileTypeUpload;
  }

}

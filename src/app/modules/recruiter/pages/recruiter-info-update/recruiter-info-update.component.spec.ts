import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterInfoUpdateComponent } from './recruiter-info-update.component';

describe('RecruiterInfoUpdateComponent', () => {
  let component: RecruiterInfoUpdateComponent;
  let fixture: ComponentFixture<RecruiterInfoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterInfoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterInfoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

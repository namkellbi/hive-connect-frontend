import { Component, OnInit } from '@angular/core';
import { IDropDownItem } from '@interfaces/form';
import { IRecruiterFindCv } from '@interfaces/post';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-find-cv',
  templateUrl: './find-cv.component.html',
  styleUrls: ['./find-cv.component.scss']
})
export class FindCvComponent implements OnInit {
  pagination = { ...DEFAULT_PAGINATION, pageSize: 9 };
  filter = {
    experienceYear: '',
    candidateAddress: '',
    techStacks: '',
    isSeen: false
  }
  loading = false;
  totals = 0;
  listExpYear: IDropDownItem<string>[] = Array.from({ length: 20 })
    .map((_, index) => index + 1)
    .map(value => ({ value: `${value} năm`, label: `${value} năm` }));
  listCv: IRecruiterFindCv[] = [];
  totalCv = 0;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.findCv();
    this.getTotalCv();
  }

  getTotalCv() {
    const { id } = this.storeService.getRecruiterProfile();
    this.apiService.getTotalCvView(id).subscribe({
      next: response => {
        if (response.status === ResponseStatus.success) {
          this.totalCv = response.data;
        }
      },
      error: error => this.helperService.callApiFailedHandler(error)
    })
  }

  findCv() {
    this.loading = true;
    this.apiService.findCV({
      ...this.pagination,
      ...this.filter
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data: { data, pagination }, message }) => {
          if (status === ResponseStatus.success) {
            this.listCv.push(...data);
            this.totals = pagination.totalRecords;
          } else {
            this.helperService.showError('', message || 'Lấy hồ sơ bị lỗi');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  search() {
    this.pagination.pageNo = 1;
    this.listCv = [];
    this.findCv();
  }
  loadMore() {
    if (this.totals > this.listCv.length && !this.loading) {
      this.pagination.pageNo += 1;
      this.findCv();
    }
  }
}

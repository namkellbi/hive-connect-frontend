import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterHomepageFeatureComponent } from './recruiter-homepage-feature.component';

describe('RecruiterHomepageFeatureComponent', () => {
  let component: RecruiterHomepageFeatureComponent;
  let fixture: ComponentFixture<RecruiterHomepageFeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterHomepageFeatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterHomepageFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

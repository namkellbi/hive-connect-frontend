import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmModalComponent } from '@components/confirm-modal/confirm-modal.component';
import { JobRecruiterView, JobState } from '@interfaces/job';
import { IRequestPagination } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterAccountInfo, IRecruiterProfile, RecruiterInfoCheck } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BehaviorSubject, combineLatest, filter, finalize, map, Observable, startWith, Subject, switchMap, tap } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-recruiter-jobs',
  templateUrl: './recruiter-jobs.component.html',
  styleUrls: ['./recruiter-jobs.component.scss']
})
export class RecruiterJobsComponent implements OnInit, OnDestroy {
  readonly DEFAULT_PAGINATION = DEFAULT_PAGINATION;
  request = new BehaviorSubject<IRequestPagination>({ ...DEFAULT_PAGINATION, });
  totalPage = 0;
  listJobs$ = new Observable<JobRecruiterView[]>();
  search$ = new Subject();
  loading = false;
  loadingInfo = false;
  selectedJobId = 0;
  destroy$ = new Subject();
  profile = {} as IRecruiterProfile;
  accountInfo = {} as IRecruiterAccountInfo;
  currentStep = 1;
  jobName = ''
  constructor(
    private storeService: StoreService,
    private apiService: ApiService,
    private modalService: BsModalService,
    private helperService: HelperService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.listJobs$ = combineLatest([
      this.storeService.observerRecruiterProfile(),
      this.request.asObservable(),
      this.search$.asObservable().pipe(startWith(''))
    ])
      .pipe(
        filter(([profile]) => !!profile.id),
        switchMap(([profile, paginate]) => {
          this.loading = true;
          this.profile = profile;
          this.getAccountInfo(profile.id);
          return this.apiService.getRecruiterJobCreate({ ...paginate, recruiterId: profile.id, jobName: this.jobName })
            .pipe(
              finalize(() => this.loading = false)
            )
        }),
        filter(({ status }) => status === ResponseStatus.success),
        tap(({ data: { pagination: { totalPage } } }) => this.totalPage = totalPage),
        map(({ data: { data } }) => data)
      )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  pageChanged({ itemsPerPage, page }: PageChangedEvent) {
    if (this.loading) {
      return;
    }

    this.request.next({ pageNo: page, pageSize: itemsPerPage });
  }

  search() {
    this.search$.next(this.jobName);
  }

  openDetail(id: number) {

  }

  getAccountInfo(id: number) {
    this.loadingInfo = true;
    this.apiService.getRecruiterAccountInfo(id)
      .pipe(
        finalize(() => this.loadingInfo = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.accountInfo = data;
            this.currentStep = +this.accountInfo.verifyStep.split('/')[0];
            this.storeService.setRecruiterVerifyInfo(data);
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  get canCreateJob() {
    return this.storeService.observerRecruiterVerifyInfo()
      .pipe(
        map(({ uncheck }) => uncheck === RecruiterInfoCheck.verified),
      );
  }

  get btnLabel() {
    return {
      [0]: 'Xác thực email',
      [1]: 'Xác thực công ty',
      [2]: 'Xác thực số điện thoại',
      [3]: 'Cập nhật giấy phép kinh doanh',
      [4]: 'Đăng tin'
    }[this.currentStep] || ''
  }

  get jobFlag() {
    return JobState;
  }

  get jobFlagLabel() {
    return {
      [JobState.draft]: 'Nháp',
      [JobState.posted]: 'Đăng'
    }
  }

  resolveUrl(step: number) {
    return {
      [0]: '/auth/register?step=3',
      [1]: '/recruiter/account/company',
      [2]: '/recruiter/account/info',
      [3]: '/recruiter/account/company',
      [4]: '/recruiter/job/create'
    }[step] || '/recruiter/job/create'
  }

  verify() {
    const step = +this.accountInfo.verifyStep.split('/')[0];
    this.router.navigateByUrl(this.resolveUrl(step));
  }

  changeJobStatus(item: JobRecruiterView, flag: JobState) {
    const modal = this.modalService.show(ConfirmModalComponent, {
      initialState: {
        message: flag === JobState.draft
          ? 'Bạn có chắc muốn gỡ công việc này không?'
          : 'Bạn có chắc đăng công việc này không?\n Khi bài đăng được ứng tuyển sẽ không chỉnh sửa được nữa!'
      },
      ignoreBackdropClick: true
    })
    modal.content?.onClose.subscribe((data) => {
      modal.hide();
      if (data) {
        this.draftJob(item);
      }
    })
  }

  draftJob(item: JobRecruiterView) {
    this.loading = true;
    this.apiService.draftJob(item.jobId)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            item.flag = JobState.draft;
          } else {
            this.helperService.showError('', response.message || 'Gỡ bài thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}

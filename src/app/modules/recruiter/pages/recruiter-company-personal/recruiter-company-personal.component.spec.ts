import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCompanyPersonalComponent } from './recruiter-company-personal.component';

describe('RecruiterCompanyPersonalComponent', () => {
  let component: RecruiterCompanyPersonalComponent;
  let fixture: ComponentFixture<RecruiterCompanyPersonalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterCompanyPersonalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCompanyPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { ICompany } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ICompanyPersonal } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';
import { ConfirmModalComponent } from '@components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'hc-recruiter-company-personal',
  templateUrl: './recruiter-company-personal.component.html',
  styleUrls: ['./recruiter-company-personal.component.scss']
})
export class RecruiterCompanyPersonalComponent implements OnInit {
  @Input() company = {} as ICompany;
  @Input() recruiterId = 0;
  loading = false;
  personalFilter = {
    ...DEFAULT_PAGINATION,
    hasMore: true,
    companyId: 0,
    pageSize: 10,
    fullName: '',
    email: '',
    phone: ''
  };
  personal: ICompanyPersonal[] = [];
  totals = 0;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.getPersonal();
  }

  getPersonal() {
    this.loading = true;
    this.apiService.getCompanyPersonal({
      ...this.personalFilter,
      companyId: this.company.id
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            const { data: personal, message, pagination: { totalPage } } = data;
            this.personal = personal;
            this.personalFilter.hasMore = totalPage > this.personalFilter.pageNo;
            this.totals = totalPage;
          } else {
            this.helperService.showError('', message || 'Lấy thông tin nhân viên thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
  }

  pageChanged($event: PageChangedEvent) {
    this.personalFilter.pageNo = $event.page;
    this.getPersonal();
  }

  search() {
    this.personalFilter.pageNo = 1;
    this.getPersonal();
  }

  openConfirmModal(item: ICompanyPersonal) {
    const modal = this.modalService.show(ConfirmModalComponent, {
      initialState: {
        message: `Bạn có chắc muốn xóa nhân viên ${item.fullName || item.email} này?`
      }
    });
    modal.content?.onClose.subscribe(result => {
      if (result) {
        this.deletePersonal(item);
      }
      modal.hide();
    })
  }

  deletePersonal(item: ICompanyPersonal) {
    this.loading = true;
    this.apiService.deleteCompanyPersonal(item.recruiterId)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Xóa nhân viên thành công');
            this.getPersonal();
          } else {
            this.helperService.showError('', response.message || 'Xóa nhân viên thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}

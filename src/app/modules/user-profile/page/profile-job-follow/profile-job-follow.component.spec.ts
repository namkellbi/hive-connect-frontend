import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileJobFollowComponent } from './profile-job-follow.component';

describe('ProfileJobFollowComponent', () => {
  let component: ProfileJobFollowComponent;
  let fixture: ComponentFixture<ProfileJobFollowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileJobFollowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileJobFollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

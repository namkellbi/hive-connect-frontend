import { Component, OnInit } from '@angular/core';
import { JobDetail, JobFollowType } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { combineLatest, filter, finalize, map, Observable, startWith, Subject, switchMap } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-profile-job-follow',
  templateUrl: './profile-job-follow.component.html',
  styleUrls: ['./profile-job-follow.component.scss']
})
export class ProfileJobFollowComponent implements OnInit {
  jobFollow$ = new Observable<JobDetail[]>();
  loading = false;
  pagination = { ...DEFAULT_PAGINATION, pageSize: 6 };
  nextPage = new Subject<number>();
  total = 0;
  candidateId = 0;
  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.jobFollow$ = combineLatest([
      this.storeService.observerCandidateProfile(),
      this.nextPage.asObservable().pipe(startWith(1))
    ])
      .pipe(
        filter(([profile]) => !!profile.id),
        switchMap(([{ id }, page]) => {
          this.loading = true;
          this.candidateId = id;
          return this.apiService.getFollowJob(id, { pageNo: page, pageSize: this.pagination.pageSize }).pipe(
            finalize(() => this.loading = false)
          );
        }),
        map(({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.total = data.pagination.totalRecords;
            return data.data;
          }
          return [];
        })
      )
  }

  pageChanged($event: PageChangedEvent) {
    this.pagination.pageNo = $event.page;
    this.nextPage.next($event.page);
  }

  unFollow(item: JobDetail) {
    this.loading = true;
    this.apiService.unFollow({ followedId: item.id || item.jobId, followerId: this.candidateId, type: JobFollowType.job })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Bỏ theo dõi thành công');
            this.nextPage.next(this.pagination.pageNo);
          } else {
            this.helperService.showError('', message || 'Bỏ theo dõi thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}

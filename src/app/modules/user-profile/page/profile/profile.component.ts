import { Component, OnInit, ViewChild } from '@angular/core';
import { DEFAULT_CANDIDATE_CV } from '@constant/form';
import { ResponseStatus } from '@interfaces/response';
import { ICandidateCV, ICandidateProfile } from '@interfaces/user';
import { ProfileCvCertificateComponent } from '@pages/user-profile/components/profile-cv-certificate/profile-cv-certificate.component';
import { ProfileCvDescriptionComponent } from '@pages/user-profile/components/profile-cv-description/profile-cv-description.component';
import { ProfileCvEducationComponent } from '@pages/user-profile/components/profile-cv-education/profile-cv-education.component';
import { ProfileCvLanguageComponent } from '@pages/user-profile/components/profile-cv-language/profile-cv-language.component';
import { ProfileCvOtherSkillComponent } from '@pages/user-profile/components/profile-cv-other-skill/profile-cv-other-skill.component';
import { ProfileCvSkillComponent } from '@pages/user-profile/components/profile-cv-skill/profile-cv-skill.component';
import { ProfileCvWorkExperienceComponent } from '@pages/user-profile/components/profile-cv-work-experience/profile-cv-work-experience.component';
import { ProfileOverviewCvInfoComponent } from '@pages/user-profile/components/profile-overview-cv-info/profile-overview-cv-info.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { finalize, map, Observable, of, switchMap, tap } from 'rxjs';
const DROPDOWN_FIELD: { value: keyof ICandidateCV, label: string }[] = [
  { value: 'certificates', label: 'Chứng chỉ' },
  { value: 'educations', label: 'Học vấn' },
  { value: 'languages', label: 'Ngôn ngữ' },
  { value: 'majorLevels', label: 'Kỹ năng chuyên môn' },
  { value: 'otherSkills', label: 'Kỹ năng khác' },
  { value: 'workExperiences', label: 'Kinh nghiệm làm việc' },
  { value: 'summary', label: 'Tóm tắt' }
]
@Component({
  selector: 'hc-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild(ProfileCvEducationComponent) educationRef!: ProfileCvEducationComponent;
  @ViewChild(ProfileCvOtherSkillComponent) otherSkillRef!: ProfileCvOtherSkillComponent;
  @ViewChild(ProfileCvWorkExperienceComponent) workExpRef!: ProfileCvWorkExperienceComponent;
  @ViewChild(ProfileCvSkillComponent) majorRef!: ProfileCvSkillComponent;
  @ViewChild(ProfileCvDescriptionComponent) desRef!: ProfileCvDescriptionComponent;
  @ViewChild(ProfileCvCertificateComponent) certificateRef!: ProfileCvCertificateComponent;
  @ViewChild(ProfileCvLanguageComponent) languageRef!: ProfileCvLanguageComponent;
  @ViewChild(ProfileCvDescriptionComponent) summaryRef!: ProfileCvDescriptionComponent;

  candidateProfile$ = new Observable<ICandidateProfile>();
  cv: ICandidateCV = {} as ICandidateCV;
  listDropdown: { value: keyof ICandidateCV, label: string }[] = DROPDOWN_FIELD.slice();
  loading = false;
  cvViewer: any[] = [];

  constructor(
    private userService: UserService,
    private helperService: HelperService,
    private storeService: StoreService,
    private apiService: ApiService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.candidateProfile$ = this.storeService.observerCandidateProfile();
    this.getCandidateProfile();
  }

  getCandidateProfile() {
    this.loading = true;
    this.userService.fetchCandidateProfile()
      .pipe(
        switchMap(({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.storeService.candidateProfile.next(data);
            return this.userService.fetchCandidateCV(data.id)
              .pipe(
                map(response => ({ ...response, profile: data }))
              );
          }
          return of({ status, data: null, profile: data });
        }),
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, profile }) => {
          if (status === ResponseStatus.success) {
            this.cv = data || {} as ICandidateCV;
            this.storeService.cv.next(this.cv);
            if (data && data.id) {
              this.filterListDropdown();
            }
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }



  filterListDropdown() {
    const { id, candidateId, isDeleted, createdAt, updatedAt, ...field } = this.cv;
    const keys = new Set<keyof ICandidateCV>();
    (Object.entries(field)).forEach(([key, value]) => {
      if (value?.length) {
        keys.add(key as keyof typeof field);
      }
    });
    this.listDropdown = DROPDOWN_FIELD.filter(option => !keys.has(option.value));
  }

  updateDataFiled(key: keyof ICandidateCV, data: any) {
    (this.cv[key] as unknown) = data;
    this.filterListDropdown();
  }

  createCV(id: number) {
    this.getOverviewInfo()?.subscribe(data => {
      if (data) {
        this.loading = true;
        this.apiService.createCV({ ...data, candidateId: id })
          .pipe(
            finalize(() => this.loading = false)
          )
          .subscribe({
            next: response => {
              if (response.status === ResponseStatus.success) {
                this.cv = {
                  ...DEFAULT_CANDIDATE_CV,
                  ...response.data
                };
                this.storeService.cv.next(this.cv);
              }
            },
            error: error => this.helperService.callApiFailedHandler(error)
          })
      }
    })
  }

  getOverviewInfo() {
    const modal = this.modalService.show(ProfileOverviewCvInfoComponent, {
      ignoreBackdropClick: true
    });
    return modal.content?.onClose.pipe(
      tap(data => modal.hide())
    )
  }


  addNewField($event: string) {
    switch ($event as keyof ICandidateCV) {
      case 'certificates': {
        this.certificateRef.addField();
        break;
      }
      case 'educations': {
        this.educationRef.addField();
        break;
      }
      case 'languages': {
        this.languageRef.addField();
        break;
      }
      case 'majorLevels': {
        this.majorRef.addField();
        break;
      }
      case 'otherSkills': {
        this.otherSkillRef.addField();
        break;
      }
      case 'workExperiences': {
        this.workExpRef.addField();
        break;
      }
      case 'summary': {
        this.summaryRef.addField();
      }
    }
  }

  updateNeedJob(id: number) {
    this.userService.updateIsNeededJob(id);
  }
}

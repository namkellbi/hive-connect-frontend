import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { ComponentsModule } from '@components/components.module';
import { ProfileCvCertificateComponent } from '@pages/user-profile/components/profile-cv-certificate/profile-cv-certificate.component';
import { ProfileCvDescriptionComponent } from '@pages/user-profile/components/profile-cv-description/profile-cv-description.component';
import { ProfileCvEducationComponent } from '@pages/user-profile/components/profile-cv-education/profile-cv-education.component';
import { ProfileCvLanguageComponent } from '@pages/user-profile/components/profile-cv-language/profile-cv-language.component';
import { ProfileCvOtherSkillComponent } from '@pages/user-profile/components/profile-cv-other-skill/profile-cv-other-skill.component';
import { ProfileCvSkillComponent } from '@pages/user-profile/components/profile-cv-skill/profile-cv-skill.component';
import { ProfileCvWorkExperienceComponent } from '@pages/user-profile/components/profile-cv-work-experience/profile-cv-work-experience.component';
import { ProfileHeaderComponent } from '@pages/user-profile/components/profile-header/profile-header.component';
import { ProfileIntroComponent } from '@pages/user-profile/components/profile-intro/profile-intro.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormModule } from '@components/form/form.module';
import { RouterModule } from '@angular/router';
import { ProfileCvViewerComponent } from '@pages/user-profile/components/profile-cv-viewer/profile-cv-viewer.component';
import { ProfileViewerOfCvComponent } from '../profile-viewer-of-cv/profile-viewer-of-cv.component';



@NgModule({
  declarations: [
    ProfileComponent,
    ProfileHeaderComponent,
    ProfileIntroComponent,
    ProfileCvEducationComponent,
    ProfileCvWorkExperienceComponent,
    ProfileCvSkillComponent,
    ProfileCvCertificateComponent,
    ProfileCvOtherSkillComponent,
    ProfileCvDescriptionComponent,
    ProfileCvLanguageComponent,
    ProfileCvViewerComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    SharedModule,
    FormModule,
    RouterModule.forChild([
      { path: '', component: ProfileComponent },
      { path: 'viewer', component: ProfileViewerOfCvComponent }
    ]),
  ]
})
export class ProfileModule { }

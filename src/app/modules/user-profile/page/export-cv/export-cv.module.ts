import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnTemplatePrimaryComponent } from '@pages/user-profile/components/cv-template/cn-template-primary/cn-template-primary.component';
import { CvTemplateSecondaryComponent } from '@pages/user-profile/components/cv-template/cv-template-secondary/cv-template-secondary.component';
import { ExportCvComponent } from './export-cv.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';



@NgModule({
  declarations: [
    ExportCvComponent,
    CnTemplatePrimaryComponent,
    CvTemplateSecondaryComponent,
  ],
  imports: [
    CommonModule,
    DirectivesModule,
    PipesModule,
    RouterModule.forChild([{ path: '', component: ExportCvComponent }]),
    SharedModule
  ],
  exports: [
    CnTemplatePrimaryComponent,
    CvTemplateSecondaryComponent,
  ]
})
export class ExportCvModule { }

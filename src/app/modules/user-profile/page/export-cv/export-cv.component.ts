import { Component, OnInit } from '@angular/core';
import { IMAGE_TYPE, PDF_IMAGE_WIDTH, PDF_PARAMETERS, PDF_POSITION } from '@constant/settings';
import { IResponse, ResponseStatus } from '@interfaces/response';
import { ICandidateCV, ICandidateProfile, IFieldMajor, IMajor, IUser } from '@interfaces/user';
import { HelperService } from '@services/helper.service';
import { SearchService } from '@services/search.service';
import { IUserObserver, StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { switchMap, of, filter, map, Subject, takeUntil, Observable, from, combineLatest, finalize, tap, mergeMap, lastValueFrom } from 'rxjs';

@Component({
  selector: 'hc-export-cv',
  templateUrl: './export-cv.component.html',
  styleUrls: ['./export-cv.component.scss']
})
export class ExportCvComponent implements OnInit {

  cv$ = new Observable<ICandidateCV>();
  user$ = new Observable<IUserObserver>();
  candidateProfile$ = new Observable<ICandidateProfile>();
  info$ = new Observable<{
    cv: ICandidateCV,
    user: IUser,
    profile: ICandidateProfile
  }>()
  templateChoose: 1 | 2 = 1;

  majorField: IFieldMajor[] = [];
  loading = false;

  mapper = {
    1: 'template-1',
    2: 'template-2'
  }

  private destroy$ = new Subject();

  constructor(
    private userService: UserService,
    private helperService: HelperService,
    private storeService: StoreService,
    private searchService: SearchService
  ) { }

  ngOnInit(): void {
    this.cv$ = this.getCV();
    this.user$ = this.storeService.observerUser();
    this.candidateProfile$ = this.storeService.observerCandidateProfile();
    this.searchService.observerMajorField()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe({
        next: data => this.majorField = data
      })
    this.info$ = combineLatest([
      this.cv$,
      this.user$,
      this.candidateProfile$
    ])
      .pipe(
        takeUntil(this.destroy$),
        filter(([cv, user, profile]) => !!cv && user != null && !!profile),
        map(([cv, user, profile]) => ({ cv, user: user || {} as IUser, profile }))
      );
    document.body.classList.add('bg-grey');
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    document.body.classList.remove('bg-grey');
    this.destroy$.complete();
  }

  getCV() {
    return this.userService.fetchCandidateProfile()
      .pipe(
        takeUntil(this.destroy$),
        switchMap(({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.storeService.candidateProfile.next(data);
            return this.userService.fetchCandidateCV(data.id);
          }
          return of({ status: ResponseStatus.error, data: {} as ICandidateCV });
        }),
        filter(response => response.status === ResponseStatus.success),
        mergeMap(async ({ data }) => {
          const fieldId = new Set(data.majorLevels.map(item => item.fieldId));
          const majorField: Promise<IResponse<IFieldMajor[]>>[] = [];
          fieldId.forEach(id => majorField.push(lastValueFrom(this.searchService.fetchMajorField(id))));
          const { majorLevels } = data;
          const mapper = majorLevels.reduce((acc, cur) => ({ ...acc, [cur.majorId]: cur }), {} as { [key: number]: IMajor });
          (await Promise.all(majorField))
            .reduce((acc, cur) => acc.concat(cur.data), [] as IFieldMajor[])
            .forEach(item => {
              const major = mapper[item.id];
              if (major) {
                major.majorName = item.majorName;
              }
            });
          console.log(data);
          return data;
        })
      )
  }

  getPdf(targetPdfElement: HTMLElement): Observable<jsPDF> {
    return from(html2canvas(targetPdfElement, { useCORS: true })).pipe(
      takeUntil(this.destroy$),
      map(canvas => {
        const imgHeight = (canvas.height * PDF_IMAGE_WIDTH) / canvas.width;
        const contentDataURL = canvas.toDataURL(IMAGE_TYPE, 1.0);
        const pdf = new jsPDF(PDF_PARAMETERS);

        pdf.addImage(contentDataURL, IMAGE_TYPE, PDF_POSITION.x, PDF_POSITION.y, PDF_IMAGE_WIDTH, imgHeight);

        return pdf;
      })
    );
  }

  exportPDF(templateRef: HTMLElement) {
    const el = document.getElementById(this.mapper[this.templateChoose]);
    this.loading = true;
    this.getPdf(el || templateRef).subscribe(pdf => {
      pdf.save(`file-${this.getDateStamp()}.pdf`);
      this.loading = false;
    });
  }

  getDateStamp(): string {
    return new Date().toLocaleDateString();
  }
}

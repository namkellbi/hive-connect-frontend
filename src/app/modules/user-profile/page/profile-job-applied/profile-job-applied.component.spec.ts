import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileJobAppliedComponent } from './profile-job-applied.component';

describe('ProfileJobAppliedComponent', () => {
  let component: ProfileJobAppliedComponent;
  let fixture: ComponentFixture<ProfileJobAppliedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileJobAppliedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileJobAppliedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { DROPDOWN_JOB_STATE } from '@constant/form';
import { JobApplied, JobApplyState } from '@interfaces/job';
import { IRequestPagination } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BehaviorSubject, combineLatest, filter, finalize, map, Observable, startWith, Subject, switchMap, takeUntil } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-profile-job-applied',
  templateUrl: './profile-job-applied.component.html',
  styleUrls: ['./profile-job-applied.component.scss']
})
export class ProfileJobAppliedComponent implements OnInit {
  jobApplied$ = new Observable<JobApplied[]>();
  total = 0;
  pagination$ = new BehaviorSubject<IRequestPagination>({ ...DEFAULT_PAGINATION });
  readonly DEFAULT_PAGINATION = DEFAULT_PAGINATION;
  loading = false;
  approvalStatus = new Subject<JobApplyState>();
  destroy$ = new Subject();
  readonly DROPDOWN_JOB_STATE = DROPDOWN_JOB_STATE;
  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.jobApplied$ = combineLatest([
      this.storeService.observerCandidateProfile(),
      this.pagination$.asObservable(),
      this.approvalStatus.asObservable().pipe(startWith('' as JobApplyState))
    ]).pipe(
      takeUntil(this.destroy$),
      filter(([profile]) => !!profile.id),
      switchMap(([{ id }, pagination, approvalStatus]) => {
        this.loading = true;
        return this.apiService.getJobApplied({ candidateId: id, ...pagination, approvalStatus })
          .pipe(
            finalize(() => this.loading = false)
          );
      }),
      map(({ status, data }) => {
        if (status === ResponseStatus.success) {
          const { data: applies, pagination } = data;
          this.total = pagination.totalRecords;
          return applies.map(item => ({
            ...item,
            job: {
              ...item.job,
              companyAvatar: item.companyAvatar,
              companyName: item.companyName
            }
          }))
        }
        return [];
      })
    )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  get statusApply() {
    return {
      [JobApplyState.approve]: 'Đơn của bạn đã được chấp thuận',
      [JobApplyState.reject]: 'Đơn của bạn đã bị từ chối',
      [JobApplyState.pending]: 'Đơn của bạn đang được xử lý'
    }
  }

  pageChanged($event: PageChangedEvent) {
    this.pagination$.next({
      ...DEFAULT_PAGINATION,
      pageNo: $event.page
    })
  }

  filterStatus($event: Event) {
    this.approvalStatus.next(($event.target as HTMLSelectElement).value as JobApplyState);
  }

  cancelApplyJob(item: JobApplied) {
    const request = {
      jobId: item.appliedJobId,
      candidateId: this.storeService.getCandidateProfile().id
    }
    this.loading = true;
    this.apiService.applyJob(request)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Hủy thành công');
            this.approvalStatus.next('' as JobApplyState);
          } else {
            this.helperService.showError('', response.message || 'Hủy thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
}

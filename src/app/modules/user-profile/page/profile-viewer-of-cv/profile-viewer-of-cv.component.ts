import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ICvViewerRequest, IRequestPagination } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { IProfileView } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { catchError, combineLatest, filter, finalize, map, Observable, of, startWith, Subject, switchMap } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-profile-viewer-of-cv',
  templateUrl: './profile-viewer-of-cv.component.html',
  styleUrls: ['./profile-viewer-of-cv.component.scss']
})
export class ProfileViewerOfCvComponent implements OnInit {
  pagination$ = new Subject<IRequestPagination>();
  loading = false;
  totals = 0;
  viewer$ = new Observable<IProfileView[]>();
  DEFAULT_PAGINATION = DEFAULT_PAGINATION;
  constructor(
    private helperService: HelperService,
    private apiService: ApiService,
    private storeService: StoreService,
    private userService: UserService,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this.viewer$ = combineLatest([
      this.storeService.observerCandidateCV(this.userService),
      this.pagination$.pipe(startWith(DEFAULT_PAGINATION))
    ])
      .pipe(
        filter(([cv]) => !!cv.id && !!cv.candidateId),
        switchMap(([cv, page]) => {
          this.loading = true;
          return this.getListCvViewer(cv.id, cv.candidateId, page)
            .pipe(
              finalize(() => this.loading = false),
              map(response => {
                if (response.status === ResponseStatus.success) {
                  this.totals = response.data.pagination.totalPage;
                  return response.data.data;
                } else {
                  throw new Error(response.message);
                }
              }),
              catchError(error => {
                this.helperService.callApiFailedHandler(error)
                return of([] as IProfileView[]);
              })
            );
        })
      )
  }

  getListCvViewer(cvId: number, candidateId: number, request: IRequestPagination) {
    const params: ICvViewerRequest = {
      ...request,
      cvId: cvId,
      candidateId: candidateId
    }
    return this.apiService.getCvViewer(params)
  }

  pageChanged($event: PageChangedEvent) {
    this.pagination$.next({
      pageNo: $event.page,
      pageSize: $event.itemsPerPage
    })
  }
  back() {
    this._location.back();
  }
}

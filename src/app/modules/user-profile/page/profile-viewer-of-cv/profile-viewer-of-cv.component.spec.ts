import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileViewerOfCvComponent } from './profile-viewer-of-cv.component';

describe('ProfileViewerOfCvComponent', () => {
  let component: ProfileViewerOfCvComponent;
  let fixture: ComponentFixture<ProfileViewerOfCvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileViewerOfCvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileViewerOfCvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { getRegexPassword } from '@constant/form';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';
import { checkPassword } from 'src/app/core/utils/form';

@Component({
  selector: 'hc-user-profile-change-password',
  templateUrl: './user-profile-change-password.component.html',
  styleUrls: ['./user-profile-change-password.component.scss']
})
export class UserProfileChangePasswordComponent implements OnInit {

  form!: FormGroup;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      oldPassword: ['', [Validators.required, Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(getRegexPassword())]],
      matchPassword: ['', [Validators.required, Validators.maxLength(50)]]
    }, { validators: checkPassword });
  }

  submit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    const { oldPassword, password, matchPassword } = this.form.value;
    const { username } = this.storeService.getUserInfo();
    this.loading = true;
    this.apiService.changePasswordRequest(username, { oldPassword, confirmPassword: matchPassword, newPassword: password })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.form.reset();
            this.helperService.showSuccess('', 'Đổi mật khẩu thành công');
          } else {
            this.helperService.showError('', message || 'Đổi mật khẩu thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

}

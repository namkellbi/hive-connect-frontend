import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { BaseResponse, IResponse, ResponseStatus } from "@interfaces/response";
import { HelperService } from "@services/helper.service";
import { Observable, of, finalize, tap, Subject } from "rxjs";
import { updateArray } from "src/app/core/utils/resourceHandle";

@Component({
  template: ''
})
export class BaseFormComponent<T> implements OnInit {
  @Input() data: T[] = [];
  @Input() cvId = 0;
  @Output() onDataChange = new EventEmitter<T[]>();
  loading = false;
  isAdd = false;
  formArray!: FormArray;
  form!: FormGroup;
  index: keyof T = 'id' as keyof T;
  destroy$ = new Subject();
  constructor(
    protected helperService: HelperService
  ) { }
  ngOnInit(): void {
    this.buildFormArray();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  buildForm() {
    // const keysOfProps = keys
  }

  addField() {
    if (this.isAdd) return;
    this.buildForm();
    this.isAdd = true;
  }

  buildFormArray() {
    this.formArray = new FormArray(this.data.map(item => new FormControl(item)));
  }

  add(callback?: (response: IResponse<T>) => void, onSuccess?: (result: T[]) => void) {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    this.loading = true;
    const params = this.form.value;
    this.addApi(params)
      .pipe(
        tap(response => callback && callback(response)),
        finalize(() => {
          this.loading = false;
          this.isAdd = false;
          this.form.reset();
        })
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.data = updateArray(data, this.data, this.index);
            onSuccess && onSuccess(this.data);
            this.onDataChange.emit(this.data);
            this.helperService.showSuccess('', 'Thêm thành công');
          } else {
            this.helperService.showError('', message || 'Something when wrong')
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
  }

  update(data: T, callback?: (response: IResponse<T>) => void, onSuccess?: (result: T[]) => void) {
    this.loading = true;
    this.updateApi(data).pipe(
      tap(response => callback && callback(response)),
      finalize(() => {
        this.loading = false;
      })
    )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            this.data = updateArray(data, this.data, this.index);
            this.onDataChange.emit(this.data);
            onSuccess && onSuccess(this.data);
            this.helperService.showSuccess('', 'Sửa thành công');
          } else {
            this.helperService.showError('', message || 'Something when wrong');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  delete(data: T, callback?: ((response: IResponse<T>) => void) | undefined, onSuccess?: (result: T[]) => void): void {
    this.loading = true;
    this.deleteApi(data)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            const index = this.data.findIndex(item => item[this.index] === data[this.index]);
            index > - 1 && (this.data.splice(index, 1));
            onSuccess && onSuccess(this.data);
            this.onDataChange.emit(this.data);
            this.helperService.showSuccess('', 'Xóa thành công');
          } else {
            this.helperService.showError('', 'Xóa thất bại')
          }
          callback && callback(response);
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  deleteApi(data: T): Observable<IResponse<T>> {
    return of({ status: ResponseStatus.error, data: {} as T, message: '' });
  }

  addApi(data: T): Observable<IResponse<T>> {
    return of({} as IResponse<T>);
  }

  updateApi(data: T): Observable<IResponse<T>> {
    return of({ status: ResponseStatus.error, data: {} as T, message: '' });
  }

  abort() {
    this.isAdd = false;
  }
}
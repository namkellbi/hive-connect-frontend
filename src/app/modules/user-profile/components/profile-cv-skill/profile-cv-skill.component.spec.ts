import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvSkillComponent } from './profile-cv-skill.component';

describe('ProfileCvSkillComponent', () => {
  let component: ProfileCvSkillComponent;
  let fixture: ComponentFixture<ProfileCvSkillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvSkillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DROPDOWN_LEVEL } from '@constant/form';
import { IResponse, ResponseStatus } from '@interfaces/response';
import { IFieldJob, IFieldMajor, IMajor } from '@interfaces/user';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize, merge, Observable, of, Subject, takeUntil } from 'rxjs'

@Component({
  selector: 'hc-profile-cv-skill',
  templateUrl: './profile-cv-skill.component.html',
  styleUrls: ['./profile-cv-skill.component.scss']
})
export class ProfileCvSkillComponent extends BaseFormComponent<IMajor> implements OnInit, OnDestroy {
  readonly DROPDOWN_LEVEL = DROPDOWN_LEVEL;
  listField: (IFieldJob & { isLoad: boolean })[] = [];
  listLoad: IFieldMajor[] = [];
  ddField: { value: number, label: string }[] = [];
  ddMajor: { value: number, label: string }[] = [];
  isEdit = false;
  loadingMajor = false;
  majorIds = new Set<number>();
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    helperService: HelperService
  ) {
    super(helperService);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    setTimeout(() => {
      this.getFields();
    }, 100);
  }

  override buildForm() {
    this.destroy$.next(null);
    this.form = this.fb.group({
      id: '',
      fieldId: ['', Validators.required],
      cvId: this.cvId,
      level: ['', [Validators.required, Validators.maxLength(50)]],
      majorId: ['', Validators.required],
      status: 0
    });
    this.form.get('fieldId')?.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value) {
          this.ddMajor = this.listLoad.filter(item => item.fieldId === +value && (this.isEdit || !this.majorIds.has(item.id)))
            .map(item => ({ value: item.id, label: item.majorName }));
          const field = this.listField.find(item => item.id === +value);
          if (this.ddMajor.length === 0 && field && !field.isLoad) {
            this.getMajorOfField(+value);
            field.isLoad = true;
          }
        }
      })
  }

  override addApi(data: IMajor) {
    return this.apiService.insertCVMajor(data);
  }


  edit(data: IMajor) {
    this.addField();
    this.isEdit = true;
    this.form.patchValue({
      ...data,
    });
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;

      super.update(
        params,
        ({ data }) => {
          this.isEdit = false;
          this.isAdd = false;
          data.fieldName = this.listField.find(item => item.id === data.fieldId)?.fieldName || ''
          data.majorName = this.listLoad.find(item => item.id === data.majorId)?.majorName || '';
        },
        (data) => {
          this.setMajorIds(data);
        });
    } else {
      super.add(
        ({ data }) => {
          data.fieldName = this.listField.find(item => item.id === data.fieldId)?.fieldName || '';
          data.majorName = this.listLoad.find(item => item.id === data.majorId)?.majorName || '';
        },
        (data) => this.setMajorIds(data)
      );
    }
  }

  override delete(data: IMajor, callback?: ((response: IResponse<IMajor>) => void) | undefined, onSuccess?: ((result: IMajor[]) => void) | undefined): void {
    super.delete(data, () => { }, this.setMajorIds.bind(this));
  }

  setMajorIds(data: IMajor[]) {
    this.majorIds = new Set<number>(this.data.map(item => item.majorId));
  }

  getFields() {
    // this.loading = true;
    this.form?.disable();
    this.apiService.getAllFieldJob()
      .pipe(finalize(() => this.form?.enable()))
      .subscribe({
        next: ({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.listField = data.map(item => ({ ...item, isLoad: false }));
            this.ddField = data.map(item => ({ value: item.id, label: item.fieldName }));
            const fieldIds = new Map<number, number>();
            this.data.forEach((major, index) => {
              major.fieldName = data.find(item => item.id === major.fieldId)?.fieldName || '';
              if (!fieldIds.has(major.majorId)) {
                fieldIds.set(major.majorId, major.fieldId);
              }
            });
            this.fetchBatchMajor(fieldIds);
            this.setMajorIds(this.data);
          }
        }
      });
  }

  fetchBatchMajor(fields: Map<number, number>) {
    const fieldIds = new Set<number>();
    fields.forEach(value => fieldIds.add(value))
    merge(...Array.from(fieldIds).map(id => this.apiService.getMajorOfField(id)))
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.listLoad.push(...data);
            this.data.forEach(major => {
              major.majorName = data.find(item => item.id === major.majorId)?.majorName || '';
            });
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })

  }


  getMajorOfField(filedId: number) {
    // this.loading = true;
    if (this.loadingMajor) return;
    this.loadingMajor = true;
    this.form?.disable();
    this.apiService.getMajorOfField(filedId)
      .pipe(
        finalize(() => {
          this.loadingMajor = false;
          this.form?.enable();
        })
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success && data) {
            this.listLoad.push(...data);
            this.ddMajor = data.map(item => ({ value: item.id, label: item.majorName }));
          }
        }
      });
  }

  override updateApi(data: IMajor): Observable<IResponse<IMajor>> {
    return this.apiService.updateMajor(data);
  }

  override deleteApi(data: IMajor): Observable<IResponse<IMajor>> {
    return this.apiService.deleteMajor(data.id);
  }
}

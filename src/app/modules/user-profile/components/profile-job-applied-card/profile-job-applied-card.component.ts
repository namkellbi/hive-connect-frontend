import { Component, Input, OnInit } from '@angular/core';
import { Job } from '@interfaces/job';
import moment from 'moment';

@Component({
  selector: 'hc-profile-job-applied-card',
  templateUrl: './profile-job-applied-card.component.html',
  styleUrls: ['./profile-job-applied-card.component.scss']
})
export class ProfileJobAppliedCardComponent implements OnInit {
  @Input() job = {} as Job;
  constructor() { }

  ngOnInit(): void {
  }
  get remainingTime() {
    return moment().diff(moment(this.job.endDate), 'days');
  }
}

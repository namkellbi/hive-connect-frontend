import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileJobAppliedCardComponent } from './profile-job-applied-card.component';

describe('ProfileJobAppliedCardComponent', () => {
  let component: ProfileJobAppliedCardComponent;
  let fixture: ComponentFixture<ProfileJobAppliedCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileJobAppliedCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileJobAppliedCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

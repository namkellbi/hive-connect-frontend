import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IResponse } from '@interfaces/response';
import { IEducation } from '@interfaces/user';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { Observable, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-profile-cv-education',
  templateUrl: './profile-cv-education.component.html',
  styleUrls: ['./profile-cv-education.component.scss']
})
export class ProfileCvEducationComponent extends BaseFormComponent<IEducation> implements OnInit {
  isEdit = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    helperService: HelperService
  ) {
    super(helperService);
  }

  override ngOnInit(): void {

  }
  override buildForm() {
    this.form = this.fb.group({
      id: '',
      school: ['', [Validators.required, Validators.maxLength(255)]],
      cvId: this.cvId,
      major: ['', [Validators.required, Validators.maxLength(255)]],
      startDate: ['', Validators.required],
      endDate: ['',],
      studying: true,
      description: ['', [Validators.maxLength(500)]]
    });
    this.form.get('studying')?.valueChanges
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (data) {
          this.form.get('endDate')?.removeValidators(Validators.required);
        } else {
          this.form.get('endDate')?.addValidators(Validators.required);
        }
      })
  }

  override addApi(data: IEducation) {
    return this.apiService.insertCVEducation(data);
  }

  edit(data: IEducation) {
    this.addField();
    this.form.patchValue({
      ...data,
      startDate: new Date(data.startDate),
      endDate: new Date(data.endDate),
    });
    this.isEdit = true;
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;

      super.update(params, () => {
        this.isEdit = false;
        this.isAdd = false;
      });
    } else {
      super.add();
    }
  }

  override updateApi(data: IEducation) {
    return this.apiService.updateCVEducation(data);
  }
  override deleteApi(data: IEducation): Observable<IResponse<IEducation>> {
    return this.apiService.deleteEducation(data.id);
  }
}

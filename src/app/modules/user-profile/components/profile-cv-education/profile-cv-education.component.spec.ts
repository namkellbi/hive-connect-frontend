import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvEducationComponent } from './profile-cv-education.component';

describe('ProfileCvEducationComponent', () => {
  let component: ProfileCvEducationComponent;
  let fixture: ComponentFixture<ProfileCvEducationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvEducationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

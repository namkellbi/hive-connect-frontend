import { Component, Input, OnInit } from '@angular/core';
import { ICvViewerRequest } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-profile-cv-viewer',
  templateUrl: './profile-cv-viewer.component.html',
  styleUrls: ['./profile-cv-viewer.component.scss']
})
export class ProfileCvViewerComponent implements OnInit {
  @Input() cvId = 0;
  @Input() candidateId = 0;
  cvViewer: any[] = [];
  totals = 0;
  constructor(
    private helperService: HelperService,
    private apiService: ApiService,
  ) { }

  ngOnInit(): void {
    this.getListCvViewer();
  }

  getListCvViewer() {
    const params: ICvViewerRequest = {
      ...DEFAULT_PAGINATION,
      cvId: this.cvId,
      candidateId: this.candidateId
    }
    this.apiService.getCvViewer(params).subscribe({
      next: response => {
        if (response.status === ResponseStatus.success) {
          this.cvViewer = response.data.data;
          this.totals = response.data.pagination.totalRecords;
        }
      },
      error: error => this.helperService.callApiFailedHandler(error)
    })
  }
}

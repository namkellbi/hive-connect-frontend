import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvViewerComponent } from './profile-cv-viewer.component';

describe('ProfileCvViewerComponent', () => {
  let component: ProfileCvViewerComponent;
  let fixture: ComponentFixture<ProfileCvViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ResponseStatus } from '@interfaces/response';
import { ICandidateCV, ICandidateProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { IUserObserver, StoreService } from '@services/store.service';
import { Observable, Subject, takeUntil } from 'rxjs';
import { FileFinishUploadType } from 'src/app/shared/directives/file-handle.directive';

@Component({
  selector: 'hc-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss']
})
export class ProfileHeaderComponent implements OnInit, OnDestroy {
  @Input() listDropdown: { value: keyof ICandidateCV, label: string }[] = [];
  @Output() addNewField = new EventEmitter<string>();
  newField = '';
  user$ = new Observable<IUserObserver>();
  profile = {} as ICandidateProfile;
  destroy$ = new Subject();
  constructor(
    private storeService: StoreService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.user$ = this.storeService.observerUser();
    this.storeService.candidateProfile
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => this.profile = data);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  newFieldChange($event: string) {
    if ($event) {
      this.addNewField.emit($event);
    }
  }

  uploadFinish($event: FileFinishUploadType[]) {
    if ($event && $event.length) {
      this.storeService.saveAvatar($event[0].url);

      this.apiService.updateProfile({
        id: this.storeService.getUserInfo().id,
        avatar: $event[0].url
      }).subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.storeService.setUserInfo(response.data);
          }
        }
      })
    }
  }
}

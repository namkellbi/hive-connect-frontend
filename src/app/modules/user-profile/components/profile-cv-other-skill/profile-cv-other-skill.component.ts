import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DROPDOWN_LEVEL } from '@constant/form';
import { IResponse } from '@interfaces/response';
import { IOtherSkill } from '@interfaces/user';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'hc-profile-cv-other-skill',
  templateUrl: './profile-cv-other-skill.component.html',
  styleUrls: ['./profile-cv-other-skill.component.scss']
})
export class ProfileCvOtherSkillComponent extends BaseFormComponent<IOtherSkill> implements OnInit {
  readonly DROPDOWN_LEVEL = DROPDOWN_LEVEL;
  isEdit = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    helperService: HelperService
  ) {
    super(helperService);
  }
  edit(data: IOtherSkill) {
    this.addField();
    this.form.patchValue({
      ...data,
    });
    this.isEdit = true;
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;

      super.update(params, () => {
        this.isEdit = false;
        this.isAdd = false;
      });
    } else {
      super.add();
    }
  }
  override ngOnInit(): void {
    super.ngOnInit();
  }

  override buildForm() {
    this.form = this.fb.group({
      id: '',
      skillName: ['', [Validators.required, Validators.maxLength(255)]],
      cvId: this.cvId,
      level: ['', [Validators.required, Validators.maxLength(50)]]
    });
  }

  override addApi(data: IOtherSkill) {
    return this.apiService.insertCVSkill(data);
  }

  override updateApi(data: IOtherSkill): Observable<IResponse<IOtherSkill>> {
    return this.apiService.updateOtherSkill(data);
  }

  override deleteApi(data: IOtherSkill): Observable<IResponse<IOtherSkill>> {
    return this.apiService.deleteOtherSkill(data.id);
  }
}

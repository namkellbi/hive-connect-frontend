import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvOtherSkillComponent } from './profile-cv-other-skill.component';

describe('ProfileCvOtherSkillComponent', () => {
  let component: ProfileCvOtherSkillComponent;
  let fixture: ComponentFixture<ProfileCvOtherSkillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvOtherSkillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvOtherSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

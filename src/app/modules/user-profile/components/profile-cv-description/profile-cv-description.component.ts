import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IResponse } from '@interfaces/response';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'hc-profile-cv-description',
  templateUrl: './profile-cv-description.component.html',
  styleUrls: ['./profile-cv-description.component.scss']
})
export class ProfileCvDescriptionComponent extends BaseFormComponent<string> implements OnInit {
  isEdit = false;
  constructor(
    private fb: FormBuilder,
    helperService: HelperService,
    private apiService: ApiService
  ) {
    super(helperService);
  }

  override buildForm() {
    this.form = this.fb.group({
      newSummary: ['', [Validators.required, Validators.maxLength(1000)]],
      cvId: this.cvId
    });
  }

  override addApi(data: any) {
    return this.apiService.updateCvSummary(data);
  }

  override updateApi(data: any): Observable<IResponse<string>> {
    return this.apiService.updateCvSummary(data);
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;
      super.update(params, () => {
        this.isEdit = false;
        this.isAdd = false;
      });
    } else {
      super.add();
    }
  }

  edit(data: any) {
    this.addField();
    this.form.patchValue({
      newSummary: data,
    });
    this.isEdit = true;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvDescriptionComponent } from './profile-cv-description.component';

describe('ProfileCvDescriptionComponent', () => {
  let component: ProfileCvDescriptionComponent;
  let fixture: ComponentFixture<ProfileCvDescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvDescriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

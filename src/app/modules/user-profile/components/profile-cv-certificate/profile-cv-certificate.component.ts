import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DROPDOWN_LEVEL } from '@constant/form';
import { IResponse } from '@interfaces/response';
import { ICertificate } from '@interfaces/user';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'hc-profile-cv-certificate',
  templateUrl: './profile-cv-certificate.component.html',
  styleUrls: ['./profile-cv-certificate.component.scss']
})
export class ProfileCvCertificateComponent extends BaseFormComponent<ICertificate> implements OnInit {
  readonly DROPDOWN_LEVEL = DROPDOWN_LEVEL;
  isEdit = false;
  constructor(
    private fb: FormBuilder,
    helperService: HelperService,
    private apiService: ApiService
  ) {
    super(helperService);
  }
  override buildForm() {
    this.form = this.fb.group({
      id: '',
      certificateName: ['', [Validators.required, Validators.maxLength(255)]],
      cvId: this.cvId,
      certificateUrl: ['', [Validators.required, Validators.maxLength(255)]],
      status: 1,
    });
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;
      super.update(params, () => {
        this.isEdit = false;
        this.isAdd = false;
      });
    } else {
      super.add();
    }
  }

  edit(data: ICertificate) {
    this.addField();
    this.form.patchValue({
      ...data,
    });
    this.isEdit = true;
  }

  override addApi(data: ICertificate) {
    return this.apiService.insertCVCertificate(data);
  }

  override updateApi(data: ICertificate) {
    return this.apiService.updateCertificate(data);
  }

  override deleteApi(data: ICertificate): Observable<IResponse<ICertificate>> {
    return this.apiService.deleteCertificate(data.id);
  }
}

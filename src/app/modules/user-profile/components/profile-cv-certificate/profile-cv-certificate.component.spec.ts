import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvCertificateComponent } from './profile-cv-certificate.component';

describe('ProfileCvCertificateComponent', () => {
  let component: ProfileCvCertificateComponent;
  let fixture: ComponentFixture<ProfileCvCertificateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvCertificateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

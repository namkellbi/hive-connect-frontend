import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DROPDOWN_LEVEL, getFullNameRegex, getRegexPhone } from '@constant/form';
import { ICandidateProfile, IUser } from '@interfaces/user';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-profile-intro',
  templateUrl: './profile-intro.component.html',
  styleUrls: ['./profile-intro.component.scss']
})
export class ProfileIntroComponent implements OnInit {
  @Input() profile = {} as ICandidateProfile;
  form!: FormGroup;
  isEdit = false;
  listGender = [
    { value: true, label: 'Nam' },
    { value: false, label: 'Nữ' }
  ];
  loading = false;
  userInfo = {} as IUser;
  destroy$ = new Subject();
  readonly DROPDOWN_LEVEL = DROPDOWN_LEVEL;
  constructor(
    private fb: FormBuilder,
    private helperService: HelperService,
    private userService: UserService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.userInfo = this.storeService.getUserInfo();
    this.storeService.observerUser()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (data) {
          this.userInfo = data;
        }
      })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  initForm() {
    this.form = this.fb.group({
      id: this.profile.id,
      userId: this.profile.userId,
      gender: this.profile.gender,
      birthDate: [new Date(this.profile.birthDate) || '', Validators.required],
      country: [this.profile.country, [Validators.required, Validators.maxLength(255)]],
      name: [this.profile.fullName || '', [Validators.required, Validators.pattern(getFullNameRegex()), Validators.maxLength(255)]],
      address: [this.profile.address || '', [Validators.required, Validators.maxLength(255)]],
      socialLink: [this.profile.socialLink || '', [Validators.maxLength(255)]],
      avatarUrl: [this.profile.avatarUrl || '', [Validators.maxLength(255)]],
      introduction: [this.profile.introduction || '', [Validators.maxLength(500)]],
      phoneNumber: [this.userInfo.phone, [Validators.pattern(getRegexPhone()), Validators.required, Validators.maxLength(20)]]
    });
  }

  get getGender() {
    return this.profile.gender ? 'Nam' : 'Nữ';
  }

  get getVerifyPhoneLabel() {
    return this.userInfo.phone ? this.userInfo.verifiedPhone ? 'Đã xác thực' : 'Chưa xác thực' : ''
  }

  get getVerifyPhoneClass() {
    return this.userInfo.verifiedPhone ? 'text-success' : 'text-danger';
  }

  async submit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    const params = { ...this.form.value };
    params.birthDate.setMinutes(params.birthDate.getMinutes() - params.birthDate.getTimezoneOffset());
    params.birthDate = (params.birthDate as Date).toISOString();
    this.loading = true;
    const response = await this.userService.updateBaseProfile(params);
    this.userService.reloadInfo();
    this.loading = false;
    if (!response) return;
    this.isEdit = false;

  }

  edit() {
    this.initForm();
    this.isEdit = true;
  }

  verifyPhoneNumber(phone: string) {
    this.userService.verifyPhone(phone);
  }
}

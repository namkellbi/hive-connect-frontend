import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IResponse } from '@interfaces/response';
import { IWorkExperience } from '@interfaces/user';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { Observable, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-profile-cv-work-experience',
  templateUrl: './profile-cv-work-experience.component.html',
  styleUrls: ['./profile-cv-work-experience.component.scss']
})
export class ProfileCvWorkExperienceComponent extends BaseFormComponent<IWorkExperience> implements OnInit {
  @Input() totalExp = '';
  isEdit = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    helperService: HelperService
  ) {
    super(helperService);
  }

  override ngOnInit(): void {

  }
  override buildForm() {
    this.form = this.fb.group({
      id: '',
      companyName: ['', [Validators.required, Validators.maxLength(100)]],
      cvId: this.cvId,
      position: ['', [Validators.required, Validators.maxLength(100)]],
      startDate: ['', Validators.required],
      endDate: ['',],
      description: ['', Validators.maxLength(500)],
      working: true
    });
    this.form.get('working')?.valueChanges
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (data) {
          this.form.get('endDate')?.removeValidators(Validators.required);
        } else {
          this.form.get('endDate')?.addValidators(Validators.required);
        }
      })
  }

  override addApi(data: IWorkExperience) {
    return this.apiService.insertCVWorkExp(data);
  }

  edit(data: IWorkExperience) {
    this.addField();
    this.form.patchValue({
      ...data,
      startDate: new Date(data.startDate),
      endDate: new Date(data.endDate)
    });
    this.isEdit = true;
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;

      super.update(params, () => {
        this.isEdit = false;
        this.isAdd = false;
      });
    } else {
      super.add();
    }
  }

  override updateApi(data: IWorkExperience): Observable<IResponse<IWorkExperience>> {
    return this.apiService.updateDateWorkExp(data);
  }

  override deleteApi(data: IWorkExperience): Observable<IResponse<IWorkExperience>> {
    return this.apiService.deleteWorkExp(data.id);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvWorkExperienceComponent } from './profile-cv-work-experience.component';

describe('ProfileCvWorkExperienceComponent', () => {
  let component: ProfileCvWorkExperienceComponent;
  let fixture: ComponentFixture<ProfileCvWorkExperienceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvWorkExperienceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvWorkExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

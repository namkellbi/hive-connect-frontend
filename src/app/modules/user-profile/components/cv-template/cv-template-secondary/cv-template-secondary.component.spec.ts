import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CvTemplateSecondaryComponent } from './cv-template-secondary.component';

describe('CvTemplateSecondaryComponent', () => {
  let component: CvTemplateSecondaryComponent;
  let fixture: ComponentFixture<CvTemplateSecondaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CvTemplateSecondaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CvTemplateSecondaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

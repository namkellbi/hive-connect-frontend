import { Component, Input, OnInit } from '@angular/core';
import { JobLevel } from '@interfaces/job';
import { ICandidateCV, ICandidateProfile, IUser } from '@interfaces/user';

@Component({
  selector: 'hc-cv-template-secondary',
  templateUrl: './cv-template-secondary.component.html',
  styleUrls: ['./cv-template-secondary.component.scss']
})
export class CvTemplateSecondaryComponent implements OnInit {
  @Input() cv = {} as ICandidateCV;
  @Input() user = {} as IUser;
  @Input() profile = {} as ICandidateProfile;
  @Input() isView = false;
  constructor() { }

  ngOnInit(): void {
  }

  get levelPercent() {
    return {
      [JobLevel.one]: 1,
      [JobLevel.three]: 2,
      [JobLevel.two]: 4,
      [JobLevel.four]: 5
    }
  }
}

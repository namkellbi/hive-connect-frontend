import { Component, Input, OnInit } from '@angular/core';
import { MAP_JOB_LEVEL_VALUE } from '@constant/form';
import { ICandidateCV, ICandidateProfile, IFieldMajor, IMajor, IUser } from '@interfaces/user';
import { SearchService } from '@services/search.service';
import { combineLatest, map, merge, of, Subject, switchMap } from 'rxjs';

@Component({
  selector: 'hc-cn-template-primary',
  templateUrl: './cn-template-primary.component.html',
  styleUrls: ['./cn-template-primary.component.scss']
})
export class CnTemplatePrimaryComponent implements OnInit {
  @Input() cv = {} as ICandidateCV;
  @Input() user = {} as IUser;
  @Input() profile = {} as ICandidateProfile;
  @Input() majorField: IFieldMajor[] = [];
  @Input() isView = false;
  destroy$ = new Subject();
  constructor(
    private searchService: SearchService,
  ) { }

  ngOnInit(): void {
    const { majorLevels } = this.cv;
    const fieldIds = new Set<number>(majorLevels.map(item => item.fieldId));
    const api = of([] as IFieldMajor[]);
    const listMajor: IFieldMajor[] = [];
    fieldIds.forEach(id => {
      api.pipe(
        switchMap(data => {
          listMajor.push(...data);
          return this.searchService.fetchMajorField(id).pipe(map(response => response.data));
        })
      )
    });
    api.subscribe(data => {
      listMajor.push(...data);
      majorLevels.forEach(item => {
        const majorField = listMajor.find(el => +el.id === +item.majorId);
        if (majorField) {
          item.majorName = majorField.majorName;
        }
      })
    })
  }

  get mapLevel() {
    return MAP_JOB_LEVEL_VALUE;
  }

}

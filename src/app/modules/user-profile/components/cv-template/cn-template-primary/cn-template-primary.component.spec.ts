import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CnTemplatePrimaryComponent } from './cn-template-primary.component';

describe('CnTemplatePrimaryComponent', () => {
  let component: CnTemplatePrimaryComponent;
  let fixture: ComponentFixture<CnTemplatePrimaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CnTemplatePrimaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CnTemplatePrimaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

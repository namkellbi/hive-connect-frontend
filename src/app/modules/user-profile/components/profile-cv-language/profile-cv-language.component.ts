import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DROPDOWN_LEVEL } from '@constant/form';
import { IResponse } from '@interfaces/response';
import { ILanguage } from '@interfaces/user';
import { BaseFormComponent } from '@pages/user-profile/base/base.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'hc-profile-cv-language',
  templateUrl: './profile-cv-language.component.html',
  styleUrls: ['./profile-cv-language.component.scss']
})
export class ProfileCvLanguageComponent extends BaseFormComponent<ILanguage> implements OnInit {
  readonly DROPDOWN_LEVEL = DROPDOWN_LEVEL;
  isEdit = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    helperService: HelperService
  ) {
    super(helperService);
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override buildForm() {
    this.form = this.fb.group({
      id: '',
      language: ['', [Validators.required, Validators.maxLength(50)]],
      cvId: this.cvId,
      level: ['', [Validators.required, Validators.maxLength(50)]]
    });
  }
  edit(data: ILanguage) {
    this.addField();
    this.form.patchValue({
      ...data,
    });
    this.isEdit = true;
  }

  addOrEdit() {
    if (this.isEdit) {
      this.helperService.validateFormField(this.form);
      if (this.form.invalid) return;
      const params = this.form.value;

      super.update(params, () => {
        this.isEdit = false;
        this.isAdd = false;
      });
    } else {
      super.add();
    }
  }

  override addApi(data: ILanguage) {
    return this.apiService.insertCvLanguage(data);
  }

  override updateApi(data: ILanguage): Observable<IResponse<ILanguage>> {
    return this.apiService.updateLanguage(data);
  }

  override deleteApi(data: ILanguage): Observable<IResponse<ILanguage>> {
    return this.apiService.deleteLanguage(data.id);
  }
}

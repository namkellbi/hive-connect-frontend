import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCvLanguageComponent } from './profile-cv-language.component';

describe('ProfileCvLanguageComponent', () => {
  let component: ProfileCvLanguageComponent;
  let fixture: ComponentFixture<ProfileCvLanguageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCvLanguageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCvLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileOverviewCvInfoComponent } from './profile-overview-cv-info.component';

describe('ProfileOverviewCvInfoComponent', () => {
  let component: ProfileOverviewCvInfoComponent;
  let fixture: ComponentFixture<ProfileOverviewCvInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileOverviewCvInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileOverviewCvInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

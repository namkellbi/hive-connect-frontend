import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IDropDownItem } from '@interfaces/form';
import { HelperService } from '@services/helper.service';

@Component({
  selector: 'hc-profile-overview-cv-info',
  templateUrl: './profile-overview-cv-info.component.html',
  styleUrls: ['./profile-overview-cv-info.component.scss']
})
export class ProfileOverviewCvInfoComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  form!: FormGroup;
  listExpYear: IDropDownItem<number>[] = Array.from({ length: 20 })
    .map((_, index) => index + 1)
    .map(value => ({ value, label: `${value} năm` }));
  constructor(
    private fb: FormBuilder,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      summary: ['', [Validators.maxLength(1000)]],
      totalExperienceYear: ['', [Validators.required, Validators.maxLength(20)]]
    })
  }

  submit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    const params = this.form.value;
    params.totalExperienceYear = `${params.totalExperienceYear} năm`;
    this.close(params);
  }

  close(data?: any) {
    this.onClose.emit(data);
  }
}

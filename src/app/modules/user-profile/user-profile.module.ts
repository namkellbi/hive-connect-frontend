import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '@components/components.module';
import { FormModule } from '@components/form/form.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProfileJobAppliedCardComponent } from './components/profile-job-applied-card/profile-job-applied-card.component';
import { ProfileOverviewCvInfoComponent } from './components/profile-overview-cv-info/profile-overview-cv-info.component';
import { ProfileJobAppliedComponent } from './page/profile-job-applied/profile-job-applied.component';
import { ProfileJobFollowComponent } from './page/profile-job-follow/profile-job-follow.component';
import { ProfileComponent } from './page/profile/profile.component';
import { UserProfileChangePasswordComponent } from './page/user-profile-change-password/user-profile-change-password.component';
import { UserProfileLayoutComponent } from './user-profile-layout.component';
import { ProfileViewerOfCvComponent } from './page/profile-viewer-of-cv/profile-viewer-of-cv.component';



@NgModule({
  declarations: [
    UserProfileLayoutComponent,

    UserProfileChangePasswordComponent,
    ProfileJobFollowComponent,
    ProfileJobAppliedComponent,
    ProfileJobAppliedCardComponent,
    ProfileOverviewCvInfoComponent,
    ProfileViewerOfCvComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComponentsModule,
    FormModule,
    RouterModule.forChild([
      {
        path: '', component: UserProfileLayoutComponent, children: [
          { path: '', component: ProfileComponent, redirectTo: 'cv', pathMatch: 'full' },
          // { path: ':username/:id', component: ProfileComponent },
          { path: 'password', component: UserProfileChangePasswordComponent },
          { path: 'follow', component: ProfileJobFollowComponent },
          { path: 'applied', component: ProfileJobAppliedComponent },
          { path: 'cv', loadChildren: () => import('./page/profile/profile.module').then(m => m.ProfileModule) }
        ]
      },
      { path: 'export-cv', loadChildren: () => import('./page/export-cv/export-cv.module').then(m => m.ExportCvModule) },
    ])
  ]
})
export class UserProfileModule { }

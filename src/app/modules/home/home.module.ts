import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '@components/components.module';
import { ChatModule } from '@pages/chat/chat.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { CandidateGuard } from 'src/app/core/guards/candidate.guard';
import { HomepageGuard } from 'src/app/core/guards/homepage.guard';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompanyCardComponent } from './components/company-card/company-card.component';
import { HomepageCadidateSectionThirdComponent } from './components/homepage-cadidate-section-third/homepage-cadidate-section-third.component';
import { HomepageCandidataCompanyComponent } from './components/homepage-candidata-company/homepage-candidata-company.component';
import { HomepageCandidateBannerComponent } from './components/homepage-candidate-banner/homepage-candidate-banner.component';
import { HomepageCandidateJobSectionFirstComponent } from './components/homepage-candidate-job-section-first/homepage-candidate-job-section-first.component';
import { HomepageCandidateJobSectionSecondsComponent } from './components/homepage-candidate-job-section-seconds/homepage-candidate-job-section-seconds.component';
import { HomepageCandidateOtherJobComponent } from './components/homepage-candidate-other-job/homepage-candidate-other-job.component';
import { HomepageCandidateSearchComponent } from './components/homepage-candidate-search/homepage-candidate-search.component';
import { HomepageCandidateSpotlightComponent } from './components/homepage-candidate-spotlight/homepage-candidate-spotlight.component';
import { HomeLayoutComponent } from './home-layout.component';
import { CompanyDetailComponent } from './pages/company-detail/company-detail.component';
import { CompanyListComponent } from './pages/company-list/company-list.component';
import { HomeComponent } from './pages/home/home.component';



@NgModule({
  declarations: [
    HomeLayoutComponent,
    HomeComponent,
    HomepageCandidateBannerComponent,
    HomepageCandidateJobSectionFirstComponent,
    HomepageCandidateJobSectionSecondsComponent,
    HomepageCadidateSectionThirdComponent,
    HomepageCandidataCompanyComponent,
    HomepageCandidateOtherJobComponent,
    CompanyDetailComponent,
    CompanyListComponent,
    CompanyCardComponent,
    HomepageCandidateSpotlightComponent,
    HomepageCandidateSearchComponent,
  ],
  exports: [
    HomepageCandidateBannerComponent,
    HomepageCandidateSpotlightComponent,
    HomepageCandidateJobSectionSecondsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '', component: HomeLayoutComponent, children: [
          { path: '', redirectTo: 'home', pathMatch: 'full' },
          { path: 'home', component: HomeComponent, canActivate: [HomepageGuard] },
          { path: 'profile', loadChildren: () => import('@pages/user-profile/user-profile.module').then(m => m.UserProfileModule), canActivate: [AuthGuard, CandidateGuard, HomepageGuard] },
          { path: 'recruiter', loadChildren: () => import('@pages/recruiter/recruiter.module').then(m => m.RecruiterModule) },
          { path: 'search', loadChildren: () => import('@pages/search/search.module').then(m => m.SearchModule), canActivate: [HomepageGuard] },
          { path: 'job', loadChildren: () => import('@pages/job/job.module').then(m => m.JobModule) },
          { path: 'company/:id', component: CompanyDetailComponent },
          { path: 'company', component: CompanyListComponent }
        ]
      },
    ]),
    InfiniteScrollModule,
    ChatModule
  ]
})
export class HomeModule { }

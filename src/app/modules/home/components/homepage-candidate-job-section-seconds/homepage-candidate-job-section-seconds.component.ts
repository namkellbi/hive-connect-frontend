import { Component, OnInit } from '@angular/core';
import { Job } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { filter, switchMap, tap, map, Observable, of } from 'rxjs';
import { defaultRequest, defaultResponseObj } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-homepage-candidate-job-section-seconds',
  templateUrl: './homepage-candidate-job-section-seconds.component.html',
  styleUrls: ['./homepage-candidate-job-section-seconds.component.scss']
})
export class HomepageCandidateJobSectionSecondsComponent implements OnInit {
  listJob$ = new Observable<Job[]>();
  isLogin = false;
  constructor(
    private userService: UserService,
    private apiService: ApiService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.listJob$ = this.storeService.observerUser()
      .pipe(
        tap(data => this.isLogin = data !== null && !!data.id),
        switchMap(user => this.userService.fetchCandidateProfile()),
        map(({ data, status }) => status === ResponseStatus.success && data?.id != null ? data : null),
        switchMap(profile => profile?.id ? this.apiService.getSuggestJob(profile.id)
          .pipe(
            map(
              response => response.status === ResponseStatus.success
                ? response
                : defaultResponseObj<Job[]>()
            ))
          : defaultRequest<Job[]>()
        ),
        map(response => response.data),
        tap(data => console.log(data))
      )
  }

}

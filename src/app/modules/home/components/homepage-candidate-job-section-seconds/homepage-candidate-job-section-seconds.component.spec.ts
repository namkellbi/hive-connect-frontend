import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidateJobSectionSecondsComponent } from './homepage-candidate-job-section-seconds.component';

describe('HomepageCandidateJobSectionSecondsComponent', () => {
  let component: HomepageCandidateJobSectionSecondsComponent;
  let fixture: ComponentFixture<HomepageCandidateJobSectionSecondsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidateJobSectionSecondsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidateJobSectionSecondsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BannerPosition, IBannerRequest } from '@interfaces/payment';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'hc-homepage-candidate-banner',
  templateUrl: './homepage-candidate-banner.component.html',
  styleUrls: ['./homepage-candidate-banner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomepageCandidateBannerComponent implements OnInit {
  @Input() displayPosition = BannerPosition.homepageBannerA;
  banner: IBannerRequest[] = [];
  constructor(
    private apiService: ApiService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.apiService.getBannerShow(this.displayPosition)
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.banner = data;
            this.cdRef.detectChanges();
          }
        }
      })
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidateBannerComponent } from './homepage-candidate-banner.component';

describe('HomepageCandidateBannerComponent', () => {
  let component: HomepageCandidateBannerComponent;
  let fixture: ComponentFixture<HomepageCandidateBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidateBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidateBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

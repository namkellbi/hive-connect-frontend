import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidateJobSectionFirstComponent } from './homepage-candidate-job-section-first.component';

describe('HomepageCandidateJobSectionFirstComponent', () => {
  let component: HomepageCandidateJobSectionFirstComponent;
  let fixture: ComponentFixture<HomepageCandidateJobSectionFirstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidateJobSectionFirstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidateJobSectionFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

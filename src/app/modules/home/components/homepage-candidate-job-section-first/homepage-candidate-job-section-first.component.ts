import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Job } from '@interfaces/job';
import { IResponsePagination, ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize, Observable } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-homepage-candidate-job-section-first',
  templateUrl: './homepage-candidate-job-section-first.component.html',
  styleUrls: ['./homepage-candidate-job-section-first.component.scss'],
})
export class HomepageCandidateJobSectionFirstComponent implements OnInit {
  @Input() listJob = [] as Job[];
  loading = false;
  currentShow: 'popular' | 'urgent' | 'news' = 'popular';
  pagination = { ...DEFAULT_PAGINATION, pageSize: 12 };
  isMore = true
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.loadingJob();
  }

  loadPopularJob() {
    return this.apiService.getPopularJob(this.pagination);
  }

  loadUrgentJob() {
    return this.apiService.getUrgentJob(this.pagination);

  }

  loadNewJob() {
    return this.apiService.getNewJob(this.pagination);
  }
  changeTab(tab: 'popular' | 'urgent' | 'news') {
    this.currentShow = tab;
    this.reload();
  }

  reload() {
    this.listJob = [];
    this.pagination = { ...DEFAULT_PAGINATION, pageSize: 12 };
    this.isMore = true;
    this.loadingJob();
  }

  loadingJob() {
    let api = new Observable<IResponsePagination<Job>>()
    switch (this.currentShow) {
      case 'news':
        api = this.loadNewJob();
        break;

      case 'popular':
        api = this.loadPopularJob();
        break;
      case 'urgent':
        api = this.loadUrgentJob();
        break;
    }
    this.loading = true;
    this.listJob = [];
    api
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            const { data: job, pagination } = data;
            this.listJob = job;
            this.isMore = this.pagination.pageNo < pagination.totalPage;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  prevPage() {
    this.pagination.pageNo -= 1;
    this.loadingJob();
  }
  nextPage() {
    this.pagination.pageNo += 1;
    this.loadingJob();
  }
}

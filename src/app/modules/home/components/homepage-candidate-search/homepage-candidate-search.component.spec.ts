import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidateSearchComponent } from './homepage-candidate-search.component';

describe('HomepageCandidateSearchComponent', () => {
  let component: HomepageCandidateSearchComponent;
  let fixture: ComponentFixture<HomepageCandidateSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidateSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidateSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

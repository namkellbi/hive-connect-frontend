import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDropDownItem } from '@interfaces/form';
import { IFieldJob } from '@interfaces/user';
import { SearchService } from '@services/search.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'hc-homepage-candidate-search',
  templateUrl: './homepage-candidate-search.component.html',
  styleUrls: ['./homepage-candidate-search.component.scss']
})
export class HomepageCandidateSearchComponent implements OnInit {
  destroy$ = new Subject();
  fieldJob: IDropDownItem<any>[] = [];
  country: IDropDownItem<any>[] = [];
  randomJob: IFieldJob[] = [];
  selectedField = { value: '', label: 'Tất cả ngành nghề' };
  selectedCountry = { value: '', label: 'Tất cả tỉnh thành' };
  constructor(
    private searchService: SearchService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.searchService.observerJobField()
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(data => {
        this.fieldJob = [
          { value: '', label: 'Tất cả ngành nghề' },
          ...data.map(item => ({ value: item.id, label: item.fieldName }))
        ];
        this.getRandomField([...data]);
      });
    this.searchService.observerCountry()
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(data => {
        this.country = [
          { value: '', label: 'Tất cả tỉnh thành' },
          ...data.map(item => ({ value: item.id, label: item.countryName }))
        ]
      });
  }

  getRandomField(field: IFieldJob[]) {
    this.randomJob = field.sort(() => 0.5 - Math.random()).slice(0, 6);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  search(searchText: string) {
    this.router.navigate(['/search'], {
      queryParams: {
        searchText,
        career: this.selectedField.value,
        location: this.selectedCountry.value
      }
    })
  }
}

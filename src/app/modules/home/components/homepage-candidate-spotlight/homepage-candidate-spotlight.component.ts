import { Component, Input, OnInit } from '@angular/core';
import { IBannerRequest } from '@interfaces/payment';

@Component({
  selector: 'hc-homepage-candidate-spotlight',
  templateUrl: './homepage-candidate-spotlight.component.html',
  styleUrls: ['./homepage-candidate-spotlight.component.scss']
})
export class HomepageCandidateSpotlightComponent implements OnInit {
  @Input() banner = {} as IBannerRequest;
  constructor() { }

  ngOnInit(): void {
  }

}

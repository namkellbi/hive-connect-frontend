import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidateSpotlightComponent } from './homepage-candidate-spotlight.component';

describe('HomepageCandidateSpotlightComponent', () => {
  let component: HomepageCandidateSpotlightComponent;
  let fixture: ComponentFixture<HomepageCandidateSpotlightComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidateSpotlightComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidateSpotlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

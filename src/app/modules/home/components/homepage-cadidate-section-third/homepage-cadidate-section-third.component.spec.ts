import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCadidateSectionThirdComponent } from './homepage-cadidate-section-third.component';

describe('HomepageCadidateSectionThirdComponent', () => {
  let component: HomepageCadidateSectionThirdComponent;
  let fixture: ComponentFixture<HomepageCadidateSectionThirdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCadidateSectionThirdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCadidateSectionThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

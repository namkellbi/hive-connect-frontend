import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Job, WorkStyle } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-homepage-cadidate-section-third',
  templateUrl: './homepage-cadidate-section-third.component.html',
  styleUrls: ['./homepage-cadidate-section-third.component.scss'],
})
export class HomepageCadidateSectionThirdComponent implements OnInit {
  loadingRemote = false;
  loadingFulltime = false;
  loadingPartTime = false;
  @Input() remote = [] as Job[];
  @Input() fulltime = [] as Job[];
  @Input() partTime = [] as Job[];
  constructor(
    private apiService: ApiService,
  ) { }

  ngOnInit(): void {
    this.loadFulltime();
    this.loadPartTime();
    this.loadRemote();
  }

  loadRemote() {
    this.loadingRemote = true;
    this.apiService.getRemoteJob()
      .pipe(
        finalize(() => this.loadingRemote = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.remote = data.data;
          }
        }
      })
  }
  loadFulltime() {
    this.loadingFulltime = true;
    this.apiService.getFullTimeJob()
      .pipe(
        finalize(() => this.loadingFulltime = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.fulltime = data.data;
          }
        }
      })
  }
  loadPartTime() {
    this.loadingPartTime = true;
    this.apiService.getParTimeJob()
      .pipe(
        finalize(() => this.loadingPartTime = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.partTime = data.data;
          }
        }
      })
  }

  get workStyle() {
    return WorkStyle
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { ICompany } from '@interfaces/job';

@Component({
  selector: 'hc-company-card',
  templateUrl: './company-card.component.html',
  styleUrls: ['./company-card.component.scss']
})
export class CompanyCardComponent implements OnInit {
  @Input() company = {} as ICompany;
  @Input() loading = false;
  constructor() { }

  ngOnInit(): void {
  }

}

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Job } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { IFieldJob } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { SearchService } from '@services/search.service';
import { finalize, Observable, tap } from 'rxjs';

@Component({
  selector: 'hc-homepage-candidate-other-job',
  templateUrl: './homepage-candidate-other-job.component.html',
  styleUrls: ['./homepage-candidate-other-job.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomepageCandidateOtherJobComponent implements OnInit {
  field$ = new Observable<IFieldJob[]>();
  selectedField = {} as IFieldJob;
  isFirstTime = true;
  loading = false;
  @Input() jobs = [] as Job[];
  constructor(
    private searchService: SearchService,
    private apiService: ApiService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.field$ = this.searchService.observerJobField()
      .pipe(
        tap(data => {
          if (data.length && this.isFirstTime) {
            this.loadJob(data[0]);
          }
        })
      );
  }

  loadJob(field: IFieldJob) {
    if (this.loading) return;
    this.selectedField = field;
    this.jobs = [];
    this.loading = true;
    this.isFirstTime = false;
    this.apiService.getJobByField(field.id)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.jobs = data.data;
            this.loading = false
            this.cdRef.detectChanges();
          }
        }
      })
  }

}

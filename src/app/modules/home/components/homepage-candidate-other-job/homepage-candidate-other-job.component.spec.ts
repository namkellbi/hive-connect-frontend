import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidateOtherJobComponent } from './homepage-candidate-other-job.component';

describe('HomepageCandidateOtherJobComponent', () => {
  let component: HomepageCandidateOtherJobComponent;
  let fixture: ComponentFixture<HomepageCandidateOtherJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidateOtherJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidateOtherJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ICompanyShort } from '@interfaces/job';
import { ApiService } from '@services/api.service';
import { map, Observable } from 'rxjs';

@Component({
  selector: 'hc-homepage-candidata-company',
  templateUrl: './homepage-candidata-company.component.html',
  styleUrls: ['./homepage-candidata-company.component.scss']
})
export class HomepageCandidataCompanyComponent implements OnInit {
  company$ = new Observable<ICompanyShort[]>();
  skeleton = Array.from({ length: 6 });
  constructor(
    private apiService: ApiService,
  ) { }

  ngOnInit(): void {
    this.company$ = this.apiService.getTopRecruitmentCompany()
      .pipe(
        map(({ data }) => data)
      );
  }

}

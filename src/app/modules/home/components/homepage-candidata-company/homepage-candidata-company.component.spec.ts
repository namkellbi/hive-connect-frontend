import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageCandidataCompanyComponent } from './homepage-candidata-company.component';

describe('HomepageCandidataCompanyComponent', () => {
  let component: HomepageCandidataCompanyComponent;
  let fixture: ComponentFixture<HomepageCandidataCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageCandidataCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageCandidataCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

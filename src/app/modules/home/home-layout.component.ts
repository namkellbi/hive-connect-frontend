import { Component, OnInit } from '@angular/core';
import { ROLE } from '@interfaces/user';
import { SearchService } from '@services/search.service';
import { StoreService } from '@services/store.service';
import { map, Observable } from 'rxjs';

@Component({
  selector: 'hc-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {
  showSearchBar$ = new Observable<boolean>();
  constructor(
    private searchService: SearchService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.showSearchBar$ = this.searchService.observeStateSearchBar()
      .pipe(
        map(data => this.storeService.getUserInfo()?.roleId === ROLE.recruiter ? false : data)
      );
  }

}

import { Component, OnInit } from '@angular/core';
import { ICompany } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {
  company: ICompany[] = [];
  loading = false;
  companyName = '';
  request = { ...DEFAULT_PAGINATION };
  totals = 0;
  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.getListCompany();
  }

  getListCompany() {
    this.loading = true;
    this.apiService.getCompany({
      ...this.request,
      companyName: this.companyName
    })
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.company.push(...data.data);
            this.totals = data.pagination.totalRecords;
          }
        }
      })
  }
  search() {
    this.request.pageNo = 1;
    this.company = [];
    this.getListCompany();
  }
  loadMore() {
    this.request.pageNo += 1;
    this.getListCompany();
  }
}

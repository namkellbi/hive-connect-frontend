import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '@components/base-component/base.component';
import { ICompany, Job } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ICompanyPersonal, IRecruiterProfile } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { filter, finalize, map, Observable, of, switchMap, takeUntil } from 'rxjs';
import { defaultRequest, DEFAULT_PAGINATION } from 'src/app/core/utils/defaultObject';

@Component({
  selector: 'hc-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  company$ = new Observable<ICompany>();
  loading = false;
  jobs: Job[] = [];
  personalFilter = {
    ...DEFAULT_PAGINATION,
    hasMore: true,
    companyId: 0
  }
  jobFilter = {
    ...DEFAULT_PAGINATION
  }
  loadingPersonal = false;
  personal: ICompanyPersonal[] = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private helperService: HelperService
  ) {
    super();
  }

  ngOnInit(): void {
    this.company$ = this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$),

        switchMap(params => {
          if (!params['id']) {
            return of({} as ICompany);
          }
          this.loading = true;
          this.getJobs(+params['id']);
          this.resetFilter(+params['id']);
          return this.apiService.getCompanyDetail(+params['id'])
            .pipe(
              finalize(() => this.loading = false),
              map((response) => this.helperService.callApiResponseHandler(response)),
            )
        })
      )
    document.body.classList.add('bg-grey');
  }

  override ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    super.ngOnDestroy();
    document.body.classList.remove('bg-grey');
  }

  getJobs(companyId: number) {
    this.apiService.getJobByCompany(companyId, this.jobFilter).subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.jobs = data.data;
        }
      }
    })
  }
  resetFilter(companyId: number) {
    this.personalFilter = {
      ...this.personalFilter,
      pageNo: 1,
      companyId,
      hasMore: true
    };
    this.getPersonal();
  }
  getPersonal() {
    this.loadingPersonal = true;
    this.apiService.getCompanyPersonal(this.personalFilter)
      .pipe(
        finalize(() => this.loadingPersonal = false)
      )
      .subscribe({
        next: ({ status, data, message }) => {
          if (status === ResponseStatus.success) {
            const { data: personal, message, pagination: { totalPage } } = data;
            this.personal = personal;
            this.personalFilter.hasMore = totalPage > this.personalFilter.pageNo;
          } else {
            this.helperService.showError('', message || 'Lấy thông tin nhân viên thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      });
  }
  prevPersonal() {
    this.personalFilter.pageNo -= 1;
    this.getPersonal();
  }

  nextPersonal() {
    this.personalFilter.pageNo += 1;
    this.getPersonal();
  }
}

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BannerPosition } from '@interfaces/payment';
import { IHomepageResponse, ResponseStatus } from '@interfaces/response';
import { HomepageCandidateSpotlightComponent } from '@pages/home/components/homepage-candidate-spotlight/homepage-candidate-spotlight.component';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { NavigationService } from '@services/navigation.service';
import { SearchService } from '@services/search.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { map, Observable, Subject } from 'rxjs';

@Component({
  selector: 'hc-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPushs
})
export class HomeComponent implements OnInit {
  loadingPost = false;
  homepage$ = new Observable<IHomepageResponse>();
  destroy$ = new Subject();
  constructor(
    private searchService: SearchService,
    private modalService: BsModalService,
    private apiService: ApiService,
    private helperService: HelperService,
    private cdRef: ChangeDetectorRef,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.navigationService.check$.next(null);
    this.searchService.setStateSearchBar(false);
    this.apiService.getBannerShow(BannerPosition.spotlight)
      .subscribe(({ status, data }) => {
        if (status === ResponseStatus.success && data.length) {
          this.modalService.show(HomepageCandidateSpotlightComponent, {
            class: 'modal-dialog-centered',
            initialState: {
              banner: data[Math.floor(Math.random() * (data.length - 1))]
            }
          });
          this.cdRef.detectChanges();
        }
      });
    // this.homepage$ = this.apiService.getHomepage()
    //   .pipe(
    //     map(({ status, data }) => data)
    //   )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  get position() {
    return BannerPosition;
  }
}

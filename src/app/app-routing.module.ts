import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from '@components/not-found-page/not-found-page.component';
import { AuthGuard } from './core/guards/auth.guard';
import { CollaboratorGuard } from './core/guards/collaborator.guard';

const routes: Routes = [
  { path: '', loadChildren: () => import('@pages/home/home.module').then(m => m.HomeModule), canActivate: [CollaboratorGuard] },
  { path: 'collaborator', loadChildren: () => import('./modules/collaborator/collaborator.module').then(m => m.CollaboratorModule), canActivate: [AuthGuard, CollaboratorGuard] },
  { path: 'auth', loadChildren: () => import('@pages/auth/auth.module').then(m => m.AuthModule) },
  { path: 'payment', loadChildren: () => import('@pages/payment/payment.module').then(m => m.PaymentModule), canActivate: [CollaboratorGuard] },
  { path: '404', component: NotFoundPageComponent },
  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

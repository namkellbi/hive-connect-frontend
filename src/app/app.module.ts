import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localeVN from '@angular/common/locales/vi';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticateInterceptor } from './core/interceptor/authenticate.interceptor';
import { SharedModule } from './shared/shared.module';

const config: SocketIoConfig = {
  url: environment.SOCKET_URL, options: {
    path: environment.SOCKET_ROOT
  }
};


registerLocaleData(localeVN);
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    // SocketIoModule.forRoot(config)
  ],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticateInterceptor,
      multi: true
    },
    { provide: LOCALE_ID, useValue: 'vi-VN' },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'đ' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { IFindJobRequest, IRequestPagination } from "@interfaces/request";
import { IResponse, ResponseStatus } from "@interfaces/response";
import { Observable, of } from "rxjs";

export const DEFAULT_PAGINATION: IRequestPagination = {
  pageNo: 1,
  pageSize: 20
}

export const DEFAULT_SEARCH: IFindJobRequest = {
  countryId: 0,
  fieldId: 0,
  fromSalary: 0,
  toSalary: 0,
  jobName: '',
  pageNo: 1,
  pageSize: 10,
  rank: '',
  workForm: '',
  workPlace: '',
  sortBy: ''
}
export const defaultResponseObj = <T>(): IResponse<T> => ({ status: ResponseStatus.error, data: {} as T, message: '' });
export const defaultRequest = <T>(): Observable<IResponse<T>> => of(defaultResponseObj<T>());
export const defaultRequestSuccess = <T>(data = {} as T): Observable<IResponse<T>> => of({ status: ResponseStatus.success, data, message: '' })
import { AbstractControl, ValidationErrors } from "@angular/forms";
import { FileUpload } from "@interfaces/form";
import { FileUploadType } from "@interfaces/request";

export const checkPassword = (group: AbstractControl): ValidationErrors | null => {
  const pass = group.get('password')?.value;
  const confirmPass = group.get('matchPassword')?.value;
  if (!confirmPass) {
    return { required: true };
  }
  if (pass !== confirmPass) {
    // group.get('password')?.setErrors({ 'notMatch': true });
    group.get('matchPassword')?.setErrors({ 'notMatch': true });
    return { notSame: true };
  }
  if (pass.length > 50) {
    group.get('password')?.setErrors({ maxlength: true })
    return { maxlength: true }
  }
  if (confirmPass.length > 50) {
    group.get('matchPassword')?.setErrors({ maxlength: true })
    return { maxlength: true }
  }
  group.get('matchPassword')?.setErrors(null);
  return null
}

export const checkRangeSalary = (group: AbstractControl): ValidationErrors | null => {
  const from = +group.get('fromSalary')?.value;
  const to = +group.get('toSalary')?.value;
  if (!to || !from) {
    return { required: true }
  }
  if (to < from) {
    group.get('fromSalary')?.setErrors({ 'largerThanRange': true });
    group.get('toSalary')?.setErrors({ 'smallerThanRange': true });
    return { outOfRange: true };
  }
  group.get('fromSalary')?.setErrors(null);
  group.get('toSalary')?.setErrors(null);
  return null;
}

export const getImageUpload = (event: Event): FileUpload | null => {
  const ele = event.target as HTMLInputElement;
  if (ele.files) {
    const result: FileUpload = { url: '', file: ele.files[0] };
    const reader = new FileReader();
    reader.readAsDataURL(ele.files[0]);
    reader.onload = (_event) => {
      result.url = reader.result;
    }
    return result;
  }
  return null
}

export class FileUploadValidate {

  static fileType: { [key in FileUploadType]: string[] } = {
    [FileUploadType.cv]: [
      "application/pdf",
    ],
    [FileUploadType.image]: ["image/jpg", "image/jpeg", "image/png", "image/bmp"],
  }
  static validate(validateType: FileUploadType, fileType: string) {
    return this.fileType[validateType].includes(fileType);
  }
}
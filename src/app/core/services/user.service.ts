import { Injectable } from '@angular/core';
import { OtpModalComponent } from '@components/modal/otp-modal/otp-modal.component';
import { ResponseStatus } from '@interfaces/response';
import { ICandidateProfile, IRecruiterAccountInfo, RecruiterInfoCheck, ROLE } from '@interfaces/user';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BehaviorSubject, finalize, map, of, switchMap, throwError } from 'rxjs';
import { defaultRequest, defaultRequestSuccess } from '../utils/defaultObject';
import { ApiService } from './api.service';
import { HelperService } from './helper.service';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  loadingProfile = false;
  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private helperService: HelperService,
    private modalService: BsModalService
  ) { }

  verifyPhone(phone: string) {
    const modal = this.modalService.show(OtpModalComponent, {
      initialState: {
        phoneNumber: phone
      },
      ignoreBackdropClick: true
    });
    modal.content?.onClose.subscribe(data => {
      modal.hide();
      if (data) {
        this.storeService.updateVerifyPhone();
      }
    })
  }
  reloadRecruiterAccountVerify() {
    this.fetchRecruiterAccountVerify().subscribe(data => this.storeService.setRecruiterVerifyInfo(data));
  }
  fetchRecruiterAccountVerify() {
    const { id } = this.storeService.getRecruiterProfile();
    if (!id) return of({ uncheck: RecruiterInfoCheck.email } as IRecruiterAccountInfo);
    return this.apiService.getRecruiterAccountInfo(id).pipe(
      map(({ status, data }) => status === ResponseStatus.success ? data : { uncheck: RecruiterInfoCheck.email } as IRecruiterAccountInfo)
    )
  }
  fetchUserInfo(id: number) {
    return this.apiService.getUserInfo(id);
  }
  fetchCandidateProfile(reload = false) {
    const { roleId, id } = this.storeService.getUserInfo() || {};
    const profile = this.storeService.getCandidateProfile();
    if (reload) {
      return this.apiService.getCandidateProfile(id);
    }
    return id ? (profile?.id ? defaultRequestSuccess(profile) : this.apiService.getCandidateProfile(id)) : defaultRequest<ICandidateProfile>();
  }

  // getCandidateProfile() {
  //   return this.fetchCandidateProfile().pipe(
  //     switchMap()
  //   )
  // }

  getRecruiterProfile() {
    return this.fetchRecruiterProfile()
      .pipe(
        switchMap((response) => {
          if (response.status === ResponseStatus.success) {
            this.storeService.recruiterProfile.next(response.data);
          }
          return this.storeService.observerRecruiterProfile();
        })
      )
  }

  fetchCandidateCV(candidateId: number) {
    return this.apiService.getCandidateCV(candidateId);
  }

  fetchRecruiterProfile(userId?: number, reload = false) {
    const { id } = this.storeService.getUserInfo();
    const profile = this.storeService.getRecruiterProfile();
    if (reload) {
      return this.apiService.getRecruiterProfile(userId || id);
    }
    return profile?.id ? defaultRequestSuccess(profile) : this.apiService.getRecruiterProfile(userId || id);
  }

  reloadRecruiterProfile() {
    const { id } = this.storeService.getUserInfo();
    this.apiService.getRecruiterProfile(id).subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.storeService.recruiterProfile.next(data);
        }
      }
    })
  }

  updateIsNeededJob(id: number) {
    this.apiService.updateIsNeededJob(id).subscribe({
      next: response => {
        if (response.status === ResponseStatus.success) {
          // this.storeService.updateNeedJobState();
          this.helperService.showSuccess('', 'Cập nhật trạng thái tìm việc thành công');
        } else {
          this.helperService.showError('', 'Cập nhật trạng thái tìm việc thất bại')
        }
      },
      error: error => this.helperService.callApiFailedHandler(error)
    });
  }

  async updateBaseProfile(params: any) {
    return new Promise((resolve, reject) => {
      this.apiService.updateBaseProfile(params).subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.storeService.updateBaseProfile(response.data);
            this.storeService.updatePhone(params.phoneNumber);
            this.helperService.showSuccess('', 'Cập nhật thông tin thành công');
            resolve(params);
          } else {
            this.helperService.showError('', response.message || 'Cập nhật thông tin thất bại');
            resolve(false)
          }
        },
        error: error => {
          this.helperService.callApiFailedHandler(error);
          resolve(false);
        }
      })
    })
  }

  reloadInfo() {
    const userInfo = this.storeService.getUserInfo();
    const { roleId } = userInfo;
    this.fetchUserInfo(userInfo.id).subscribe({
      next: ({ status, data }) => {
        if (status === ResponseStatus.success) {
          this.storeService.updateUserInfo(data);
        }
      }
    })
    if (roleId === ROLE.candidate) {
      this.fetchCandidateProfile(true)
        .pipe(
          switchMap(({ status, data, message }) => {
            if (status === ResponseStatus.success) {
              this.storeService.candidateProfile.next(data);
              this.storeService.updateVerifyPhone(data.verifiedPhone);
              return this.fetchCandidateCV(data.id)
                .pipe(
                  map(response => ({ ...response, profile: data })),
                );
            }
            return throwError(() => new Error(message))
          }),
        )
        .subscribe({
          next: ({ status, data, }) => {
            if (status === ResponseStatus.success) {
              this.storeService.cv.next(data);
            }
          },
          error: error => this.helperService.callApiFailedHandler(error)
        })
    }
    if (roleId === ROLE.recruiter) {
      this.fetchRecruiterProfile(userInfo.id, true)
        .subscribe({
          next: ({ status, data }) => {
            if (status === ResponseStatus.success) {
              this.storeService.recruiterProfile.next(data);
              this.storeService.updateVerifyPhone(data.verifiedPhone);
            }
          }
        })
    }
  }
}

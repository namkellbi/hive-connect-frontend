import { Injectable } from '@angular/core';
import { ICompany, ICountry } from '@interfaces/job';
import { IFindJobRequest } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { IFieldJob, IFieldMajor } from '@interfaces/user';
import { BehaviorSubject, Subject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private loading = new Subject<boolean>();
  private isShowSearchBar = new BehaviorSubject<boolean>(true);
  private fieldJob = new BehaviorSubject<IFieldJob[]>([]);
  private country = new BehaviorSubject<ICountry[]>([]);
  private majorOfField = new BehaviorSubject<IFieldMajor[]>([]);
  private companies = new BehaviorSubject<ICompany[]>([]);
  private loaded = new Set<number>();
  constructor(
    private apiService: ApiService
  ) { }

  fetchCompany() {
    if (this.companies.getValue().length) return;
    this.apiService.getListCompany().subscribe({
      next: response => {
        if (response.status === ResponseStatus.success) {
          this.companies.next(response.data);
        }
      },
      error: error => console.log(error)
    })
  }

  observerCompany() {
    return this.companies.asObservable();
  }

  getSearchLoading() {
    return this.loading.asObservable();
  }

  searchJob(request: IFindJobRequest) {
    return this.apiService.searchJob(request);
  }

  toggleSearchBar() {
    const state = this.isShowSearchBar.getValue();
    this.isShowSearchBar.next(!state);
  }

  setStateSearchBar(state: boolean) {
    this.isShowSearchBar.next(state);
  }

  observeStateSearchBar() {
    return this.isShowSearchBar.asObservable();
  }

  getAllField() {
    if (this.fieldJob.getValue().length !== 0) return;
    this.apiService.getAllFieldJob()
      .subscribe({
        next: ({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.fieldJob.next(data);
          }
        }
      });
  }

  getAllCountry() {
    if (this.country.getValue().length !== 0) return;
    this.apiService.getAllCountry()
      .subscribe({
        next: ({ data, status }) => {
          if (status === ResponseStatus.success) {
            this.country.next(data);
          }
        }
      })
  }

  getMajorOfField(filedId: number) {
    if (this.loaded.has(filedId)) {
      return;
    }
    this.loaded.add(filedId);
    this.fetchMajorField(filedId)
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success && data) {
            this.updateMajorOfField(data);
          }
        }
      });
  }

  fetchMajorField(filedId: number) {
    return this.apiService.getMajorOfField(filedId);
  }

  updateMajorOfField(data: IFieldMajor[]) {
    const list = this.majorOfField.getValue();
    this.majorOfField.next(list.concat(data));
  }

  observerMajorField() {
    return this.majorOfField.asObservable();
  }

  observerJobField() {
    return this.fieldJob.asObservable();
  }

  observerCountry() {
    return this.country.asObservable();
  }
}

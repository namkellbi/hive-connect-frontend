import { Injectable } from '@angular/core';
import { STORAGE_KEY } from '@constant/settings';
import { BehaviorSubject, combineLatest, filter, map, of, switchMap } from 'rxjs';
import { ICandidateCV, ICandidateProfile, IRecruiterAccountInfo, IRecruiterProfile, IUser, RecruiterInfoCheck, ROLE } from '../interfaces/user';
import { parseJSON } from '../utils/resourceHandle';
import { UserService } from './user.service';

export type IUserObserver = IUser | null;
export interface InfoRecruiter {
  info: IUserObserver;
  profile: IRecruiterProfile;
}
export interface InfoCandidate {
  info: IUserObserver;
  profile: ICandidateProfile;
}
@Injectable({
  providedIn: 'root'
})
export class StoreService {

  userInfo = new BehaviorSubject<IUserObserver>({} as IUser);
  candidateProfile = new BehaviorSubject<ICandidateProfile>({} as ICandidateProfile);
  recruiterProfile = new BehaviorSubject<IRecruiterProfile>({} as IRecruiterProfile);
  cv = new BehaviorSubject<ICandidateCV>({} as ICandidateCV);
  verifyInfo = new BehaviorSubject<IRecruiterAccountInfo>({ uncheck: RecruiterInfoCheck.email } as IRecruiterAccountInfo);
  constructor(
  ) {
  }

  setRecruiterVerifyInfo(data: IRecruiterAccountInfo) {
    this.verifyInfo.next(data)
  }

  observerRecruiterVerifyInfo() {
    return this.verifyInfo.asObservable();
  }

  observerUser() {
    return this.userInfo.asObservable().pipe(map(data => data ? data : {} as IUser));
  }

  observerCandidateProfile() {
    return this.candidateProfile.asObservable();
  }

  observerRecruiterProfile() {
    return this.recruiterProfile.asObservable();
  }

  observerCV() {
    return this.cv.asObservable();
  }

  observerUserInfoRecruiter() {
    return combineLatest([
      this.observerUser(),
      this.observerRecruiterProfile()
    ])
      .pipe(
        map(([info, profile]) => ({ info, profile }))
      )
  }
  observerUserInfoCandidate() {
    return combineLatest([
      this.observerUser(),
      this.observerCandidateProfile()
    ])
      .pipe(
        map(([info, profile]) => ({ info, profile }))
      )
  }

  observerCandidateCV(userService: UserService) {
    return this.candidateProfile.pipe(
      filter(profile => !!profile.id),
      switchMap(profile => userService.fetchCandidateCV(profile.id)),
      map(response => response.data)
    )
  }

  updateUserInfo(user: IUser) {
    this.setUserInfo(user);
    this.userInfo.next(user);
  }

  getCV() {
    return this.cv.getValue();
  }

  getCandidateProfile() {
    return this.candidateProfile.getValue();
  }

  getRecruiterProfile() {
    return this.recruiterProfile.getValue();
  }

  getAccessToken() {
    return this.getKey(STORAGE_KEY.ACCESS_TOKEN);
  }

  getRefreshToken() {
    return this.getKey(STORAGE_KEY.REFRESH_TOKEN);
  }

  setAccessToken(token: string) {
    this.setKey(STORAGE_KEY.ACCESS_TOKEN, token);
  }

  setRefreshToken(token: string) {
    this.setKey(STORAGE_KEY.REFRESH_TOKEN, token);
  }

  setUserInfo(info: IUser) {
    this.setKey(STORAGE_KEY.USER_INFO, JSON.stringify(info));
  }

  getUserInfo() {
    return parseJSON(this.getKey(STORAGE_KEY.USER_INFO), {} as IUser);
  }

  clearStore() {
    localStorage.clear();
    this.userInfo.next({} as IUser);
  }

  saveAvatar(avatar: string) {
    const userInfo = this.userInfo.getValue();
    if (userInfo) {
      userInfo.avatar = avatar;
      this.userInfo.next(userInfo);
      this.setUserInfo(userInfo);
    }
  }

  savePhone(phone: string) {
    const userInfo = this.userInfo.getValue();
    if (userInfo) {
      userInfo.phone = phone;
      this.userInfo.next(userInfo);
      this.setUserInfo(userInfo);
    }
  }

  setLatestUser(username: string) {
    localStorage.setItem(STORAGE_KEY.LATEST_USER, username);
  }

  getLatestUser() {
    return localStorage.getItem(STORAGE_KEY.LATEST_USER) || '';
  }

  getKey(key: string) {
    return localStorage.getItem(key) || '';
  }

  setKey(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  updateNeedJobState() {
    const profile = this.candidateProfile.getValue();
    profile.needJob = !profile.needJob;
    this.candidateProfile.next(profile);
  }

  updateBaseProfile(data: ICandidateProfile) {
    const profile = this.candidateProfile.getValue();
    this.candidateProfile.next({
      ...profile,
      ...data,
    })
  }

  updatePhone(phone: string) {
    const user = this.userInfo.getValue();
    if (user) {
      user.phone = phone;
      this.userInfo.next(user);
      this.setUserInfo(user)
    }
  }

  updateVerifyPhone(state = true) {
    const info = this.userInfo.getValue();
    if (info) {
      this.userInfo.next({
        ...info,
        verifiedPhone: state
      });
      this.setUserInfo({
        ...info,
        verifiedPhone: state
      })
    }
  }

  updateRecruiterBasicInfo(params: any) {
    const user = this.userInfo.getValue();
    const profile = this.recruiterProfile.getValue();
    this.userInfo.next({
      ...user,
      ...params,
      name: params.fullName,

    });
    this.setUserInfo({
      ...user,
      ...params
    })
    this.recruiterProfile.next({
      ...profile,
      ...params,
    });
  }
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { STORAGE_KEY } from '@constant/settings';
import { IRegisterRequest } from '@interfaces/request';
import { ICandidateProfile, IRecruiterProfile, ROLE } from '@interfaces/user';
import { ApiService } from './api.service';
import { HelperService } from './helper.service';
import { StoreService } from './store.service';
import { UserService } from './user.service'
import { LoginResponse, ResponseStatus } from '@interfaces/response';
declare var google: any;
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
    private router: Router,
    private helperService: HelperService,
    private userService: UserService
  ) { }

  async loginSuccess(data: LoginResponse) {
    const { data: { candidate, recruiter, user }, token } = data;
    this.storeService.setAccessToken(token);
    this.storeService.setRefreshToken(token);
    this.storeService.setLatestUser(user.email);
    this.storeService.setUserInfo(user);
    this.storeService.userInfo.next(user);
    this.storeService.candidateProfile.next(candidate || {} as ICandidateProfile);
    this.storeService.recruiterProfile.next(recruiter || {} as IRecruiterProfile);
    if (candidate?.id) {
      this.userService.fetchCandidateCV(candidate.id)
        .subscribe({
          next: ({ status, data, }) => {
            if (status === ResponseStatus.success) {
              this.storeService.cv.next(data);
            }
          },
        })
    }
    if (recruiter?.id) {
      this.userService.fetchRecruiterProfile(user.id, true).subscribe({
        next: response => {
          if (response.status === ResponseStatus.success) {
            this.storeService.recruiterProfile.next(response.data);
          }
        }
      });
    }
  }

  validateLoginAccount({ data: { user, approvedBusinessLicense, createdOrRequestedJoinCompany, verifiedEmail, joinedCompany }, token }: LoginResponse) {
    if (user.roleId === ROLE.admin) {
      this.helperService.showError('', 'Tài khoản của bạn không có quyền đăng nhập');
      return false;
    }
    if (user.roleId === ROLE.collaborator) {
      return true;
    }
    if (!verifiedEmail) {
      this.storeService.setKey(STORAGE_KEY.USERNAME, user.username);
      this.storeService.setKey(STORAGE_KEY.EMAIL_TEMP, user.email);
      this.router.navigate(['/auth', 'register'], {
        queryParams: {
          step: 3,
          resend: 'email'
        }
      })
      return false;
    }
    if (!createdOrRequestedJoinCompany && user.roleId === ROLE.recruiter) {
      this.storeService.setKey(STORAGE_KEY.TOKEN_TEMP, token);
      this.router.navigate(['/auth', 'register'], {
        queryParams: {
          step: 4,
          id: user.id
        }
      });
      return false;
    }
    return true;
  }

  // getUserInfo(username: string) {
  //   return lastValueFrom(this.apiService.getBasicProfileUser(username));
  // }

  logout() {
    this.storeService.clearStore();
    this.signOut();
    google.accounts.id.disableAutoSelect();
    this.router.navigate(['/auth/login']).then(() => window.location.reload());
  }

  async signInWithGoogle() {
    // const response = await this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
    // return response
  }

  async signInWithFB() {
    // return this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    // this.ssocialAuthService.signOut();
  }

  register(request: IRegisterRequest) {
    return this.apiService.register(request);
  }
}

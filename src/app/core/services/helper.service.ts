import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { DEFAULT_IMAGE_UPLOAD } from '@constant/form';
import { FileUploadType, IRequestUploadImage } from '@interfaces/request';
import { IResponse, ResponseStatus } from '@interfaces/response';
import { ROLE } from '@interfaces/user';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { lastValueFrom, map, of, switchMap, throwError } from 'rxjs';
import { ApiService } from './api.service';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
    private toastr: ToastrService,
    private storeService: StoreService,
    private apiService: ApiService,
    private router: Router
  ) { }

  showSuccess(title: string, content: string, option?: IndividualConfig) {
    return this.toastr.success(content, title, option);
  }

  showError(title: string, content: string, option?: IndividualConfig) {
    return this.toastr.error(content, title, option);
  }

  showWarning(title: string, content: string, option?: IndividualConfig) {
    return this.toastr.warning(content, title, option);
  }

  validateFormField(formGroup: FormGroup): void {
    formGroup.markAllAsTouched();
  }

  uploadImage(file: File, uploadType: FileUploadType = FileUploadType.image) {
    return lastValueFrom(
      this.apiService.uploadImage(
        this.getFormSubmitFile(file, uploadType)
      )
    )
      .then(result => {
        if (result.status === ResponseStatus.error) {
          throw new Error(result.message);
        }
        return { url: result.data, type: file.type.split('/')[0] };
      });
  }

  uploadImageObservable(file: File, uploadType: FileUploadType = FileUploadType.image) {
    return this.apiService.uploadImage(
      this.getFormSubmitFile(file, uploadType)
    ).pipe(
      switchMap(response => {
        this.callApiResponseHandler(response);
        return response.status === ResponseStatus.success
          ? of({ url: response.data, type: file.type.split('/')[0] })
          : throwError(() => response.message)
      }
      )
    );
  }

  uploadBatchImage(files: File[]) {
    return Promise.all(files.map(file => this.uploadImage(file)));
  }

  getFormSubmitFile(file: File, typeUpload: FileUploadType) {
    const request = {
      uploadFileName: file.name,
      type: file.type.split('/')[1],
      typeUpload,
      file
    }
    const form = new FormData();
    Object.entries(request).forEach(([key, value]) => form.append(key, value));
    return form;
  }

  handleImageInfo(file: File) {
    const request = {
      uploadFileName: file.name,
      type: file.type.split('/')[1]
    }
    return new Blob([JSON.stringify(request)], { type: 'application/json' });
  }

  uploadFile(file: File) {
    const fileName = new Date().getTime().toString() + file.name;
    return new Promise((resolve, reject) => {
      resolve({ url: '', type: file.type.split('/')[0] });
    })
  }

  notSupported() {
    this.showWarning('', 'Tính năng đang được phát triển');
  }

  makeId(length: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }

  callApiFailedHandler(error: any, message?: string) {
    console.error('call failed', error);
    this.showError('', error.message || message || 'Xảy ra lỗi');
  }

  callApiResponseHandler<T>(response: IResponse<T>) {
    if (response.status === ResponseStatus.success) {
      return response.data;
    }
    if (response.status === ResponseStatus.notFound) {
      this.router.navigate(['/404']);
      return {} as T;
    }
    // this.callApiFailedHandler(new Error(response.message));
    throw new Error(response.message);
    // return {} as T;
  }
}

import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ROLE } from '@interfaces/user';
import { combineLatest, filter, of, startWith, Subject, switchMap } from 'rxjs';
import { StoreService } from './store.service';

export interface INavigationItem {
  label: string;
  path: string[];
  fragment?: string;
}

const guestNavigator: INavigationItem[] = [
  { label: 'Việc làm', path: ['/home'] },
  { label: 'Công ty', path: ['company'] },
  // { label: 'Dành cho nhà tuyển dụng', path: ['/recruiter', 'introduction'] }
];
const candidateNavigator: INavigationItem[] = [
  { label: 'Việc làm', path: ['/home'] },
  { label: 'Hồ sơ', path: ['/profile'] },
  { label: 'Công ty', path: ['company'] },
];

const recruiterNavigator: INavigationItem[] = [
  { label: 'Đăng tin', path: ['/recruiter', 'job', 'create'] },
  { label: 'Tìm CV', path: ['/recruiter', 'find-cv'] },
  { label: 'Báo giá dịch vụ', path: ['/recruiter', 'payment', 'package'] }
]
const guestRecruiterNavigator: INavigationItem[] = [
  { label: 'Giới thiệu', path: ['/recruiter', 'introduction'] },
  { label: 'Tính năng', path: ['/recruiter', 'introduction'], fragment: 'feature' },
  { label: 'Báo giá', path: ['/recruiter', 'package'] }
]

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  private preNavigator: 'default' | 'recruiter' = 'default';
  check$ = new Subject();
  constructor(
    private router: Router,
    private storeService: StoreService
  ) {
  }

  getNavigatorItem() {
    return combineLatest([
      this.storeService.observerUser(),
      this.check$.asObservable().pipe(startWith({}))
    ]).pipe(
      switchMap(([user,]) => {
        if (user && user.roleId === ROLE.recruiter) {
          return of(recruiterNavigator);
        }
        if (user && user.roleId === ROLE.candidate) {
          return of(candidateNavigator);
        }
        return location.pathname.includes('recruiter') ? of(guestRecruiterNavigator) : of(guestNavigator)
      })
    )
  }

  setPrevNavigator(url: string) {
    let prevRoute: 'default' | 'recruiter' = 'default';
    if (url.includes('recruiter')) {
      prevRoute = 'recruiter';
    }
    const isDiff = this.preNavigator !== prevRoute;
    this.preNavigator = prevRoute;
    return isDiff;
  }
}

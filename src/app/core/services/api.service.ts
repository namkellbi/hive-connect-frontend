import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBase } from '@interfaces/base';
import { IRoom } from '@interfaces/chat';
import { ICompany, ICompanyShort, ICountry, Job, JobApplied, JobApplyState, JobDetail, JobFollowType, JobRecruiterView } from '@interfaces/job';
import { BannerPosition, IBannerRequest, IPackage, IRentalPackage, ITransaction, PaymentPackageDetail, PaymentRental, PurchasedPackage } from '@interfaces/payment';
import { IPost, IRecruiterFindCv, IUserPost } from '@interfaces/post';
import { IChangeApplyRequestState, ICvViewerRequest, IFindJobRequest, ILoginRequest, IPaymentRequest, IRegisterRequest, IRequestPagination } from '@interfaces/request';
import { IHomepageResponse, IPackageDetailResponse, IResponse, IResponsePagination, LoginResponse, ResponseStatus } from '@interfaces/response';
import { ICandidateCV, ICandidateProfile, ICertificate, ICompanyPersonal, ICVApply, IEducation, IFieldJob, IFieldMajor, ILanguage, IMajor, IOtherSkill, IProfileView, IRecruiterAccountInfo, IRecruiterProfile, IRecruiterProfileExtend, IRequestJoinCompany, IUser, IWorkExperience } from '@interfaces/user';
import { delay, map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { post } from '../mock/post';
import { recommend } from '../mock/recommend';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getApiV1(url: string) {
    return `${environment.API_URL}${environment.API_V1}${url}`;
  }

  getSocketApi(url: string) {
    return `${environment.SOCKET_URL}/${url}`;
  }

  indexPagination<T extends IBase>(response: IResponsePagination<T>) {
    if (response.data && response.data.data && response.data.data.length) {
      const { page, size } = response.data.pagination;
      response.data.data = response.data.data.map((item, index) => ({
        ...item,
        stt: ((+page - 1) * +size) + index + 1
      }));
    }
    return response;
  }

  login(params: ILoginRequest) {
    const url = this.getApiV1('login');
    return this.httpClient.post<LoginResponse>(url, params);
  }

  register(request: IRegisterRequest) {
    const url = this.getApiV1('register');
    return this.httpClient.post<IResponse<IUser> & { token: string }>(url, request);
  }

  getPost(): Observable<IPost[]> {
    return of(post as IPost[]).pipe(delay(500));
  }

  getRecommendUser() {
    return of<IUserPost[]>(recommend).pipe(delay(500));
  }

  getCandidateProfile(userId: number) {
    const url = this.getApiV1('candidate/find-by-userid');
    return this.httpClient.get<IResponse<ICandidateProfile>>(url, { params: { userId } });
  }
  getCandidateCV(candidateId: number) {
    const url = this.getApiV1('CV/get-cv');
    return this.httpClient.get<IResponse<ICandidateCV>>(url, { params: { candidateId } });
  }

  insertCvLanguage(params: ILanguage) {
    const url = this.getApiV1('CV/insert-language');
    return this.httpClient.post<IResponse<ILanguage>>(url, params);
  }

  insertCVWorkExp(params: IWorkExperience) {
    const url = this.getApiV1('CV/insert-work-exp');
    return this.httpClient.post<IResponse<IWorkExperience>>(url, params);
  }

  insertCVSkill(params: IOtherSkill) {
    const url = this.getApiV1('CV/insert-other-skill');
    return this.httpClient.post<IResponse<IOtherSkill>>(url, params);
  }

  insertCVEducation(params: IEducation) {
    const url = this.getApiV1('CV/insert-education');
    return this.httpClient.post<IResponse<IEducation>>(url, params);
  }

  insertCVCertificate(params: ICertificate) {
    return this.httpClient.post<IResponse<ICertificate>>(this.getApiV1('CV/insert-certificate'), params);
  }

  insertCVMajor(params: IMajor) {
    return this.httpClient.post<IResponse<IMajor>>(this.getApiV1('CV/insert-major-level'), params);
  }

  updateCVEducation(params: IEducation) {
    const url = this.getApiV1('CV/update-education');
    return this.httpClient.put<IResponse<IEducation>>(url, params);
  }

  updateCvSummary(params: { cvId: number; newSummary: string }) {
    return this.httpClient.post<IResponse<string>>(this.getApiV1('CV/update-cv-summary'), params);
  }

  updateOtherSkill(params: IOtherSkill) {
    return this.httpClient.put<IResponse<IOtherSkill>>(this.getApiV1('CV/update-other-skill'), params);
  }

  updateMajor(params: IMajor) {
    return this.httpClient.put<IResponse<IMajor>>(this.getApiV1('CV/update-major-level'), params);
  }

  updateLanguage(params: ILanguage) {
    return this.httpClient.put<IResponse<ILanguage>>(this.getApiV1('CV/update-language'), params);
  }

  updateDateWorkExp(params: IWorkExperience) {
    return this.httpClient.put<IResponse<IWorkExperience>>(this.getApiV1('CV/update-work-experience'), params);
  }

  updateCertificate(params: ICertificate) {
    return this.httpClient.put<IResponse<ICertificate>>(this.getApiV1('CV/update-certificate'), params);
  }

  deleteEducation(id: number) {
    return this.httpClient.delete<IResponse<IEducation>>(this.getApiV1('CV/delete-education'), { params: { id } });
  }

  getAllFieldJob() {
    return this.httpClient.get<IResponse<IFieldJob[]>>(this.getApiV1('CV/get-all-field'));
  }

  getMajorOfField(fieldId: number) {
    return this.httpClient.get<IResponse<IFieldMajor[]>>(this.getApiV1('CV/get-major-by-field'), { params: { fieldId } });
  }

  getUrgentJob(params: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/urgent-job'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  getRemoteJob() {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/remote-job')).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getPopularJob(params: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/popular-job'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getParTimeJob() {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/parttime-job')).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getNewJob(params: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/new-job'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getJobDetail(id: number, candidateId?: number) {
    return this.httpClient.get<IResponse<JobDetail>>(this.getApiV1(`job/job-detail/${id}`), { params: { candidateId: candidateId || -1 } });
  }
  getJobByField(id: number) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1(`job/job-by-field`), { params: { id, pageSize: 12 } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getFullTimeJob() {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1(`job/fulltime-job`)).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  searchJob(request: IFindJobRequest) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1(`job/find-job`), { params: { ...request } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getSuggestJob(id: number) {
    return this.httpClient.get<IResponse<Job[]>>(this.getApiV1(`job/suggest-job/${id}`));
  }
  getAllCountry() {
    return this.httpClient.get<IResponse<ICountry[]>>(this.getApiV1('common/get-vietnam-country'));
  }

  getRecruiterJobCreate(request: IRequestPagination & { recruiterId: number, jobName?: string }) {
    return this.httpClient.get<IResponsePagination<JobRecruiterView>>(this.getApiV1('job/get-jobs-of-recruiter'), { params: { ...request } }).pipe(
      map(response => this.indexPagination(response))
    );
  }

  getRecruiterProfile(userId: number) {
    return this.httpClient.get<IResponse<IRecruiterProfile>>(this.getApiV1(`recruiter/recruiter-profile/${userId}`))
      .pipe(
        map(response => {
          if (response.status === ResponseStatus.success) {
            const { data } = response;
            return {
              ...response,
              data: {
                ...data,
                id: data.id || data.recruiterId
              }
            }
          }
          return response;
        })
      );
  }

  getCVAppliedJob(jobId: number) {
    return this.httpClient.get<IResponse<any>>(this.getApiV1('job/list-cv-applied-a-job'), { params: { jobId } });
  }

  getCompany(params: any) {
    return this.httpClient.get<IResponsePagination<ICompany>>(this.getApiV1('company/search-company'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  createJob(request: any) {
    return this.httpClient.post<IResponse<Job>>(this.getApiV1('job/create-job'), request);
  }

  updateJob(request: any) {
    return this.httpClient.put<IResponse<Job>>(this.getApiV1('job/update-job'), request);
  }

  applyJob(request: any) {
    return this.httpClient.post<IResponse>(this.getApiV1('job/apply-job'), request);
  }

  deleteWorkExp(id: number) {
    return this.httpClient.delete<IResponse<IWorkExperience>>(this.getApiV1('CV/delete-work-experience'), { params: { id } });
  }
  deleteOtherSkill(id: number) {
    return this.httpClient.delete<IResponse<IOtherSkill>>(this.getApiV1('CV/delete-other-skill'), { params: { id } });
  }
  deleteMajor(id: number) {
    return this.httpClient.delete<IResponse<IMajor>>(this.getApiV1('CV/delete-major-level'), { params: { id } });
  }
  deleteLanguage(id: number) {
    return this.httpClient.delete<IResponse<ILanguage>>(this.getApiV1('CV/delete-language'), { params: { id } });
  }
  deleteCertificate(id: number) {
    return this.httpClient.delete<IResponse<ICertificate>>(this.getApiV1('CV/delete-certificate'), { params: { id } });
  }

  getCvAppliedJob(jobId: number, request: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<ICVApply>>(this.getApiV1(`job/list-cv-applied-a-job/${jobId}`), { params: { ...request } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  approveJob(params: IChangeApplyRequestState) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1('job/approve-job'), params);
  }

  createCompany(params: any) {
    return this.httpClient.post<IResponse<ICompany>>(this.getApiV1('company/create-company'), params);
  }

  updateCompanyInfo(recruiterId: number, params: any) {
    return this.httpClient.put<IResponse<ICompany>>(this.getApiV1(`company/update-company-information/${recruiterId}`), params, { params: { recruiterId } });
  }

  resendEmailVerify(username: string) {
    return this.httpClient.post<IResponse<unknown>>(this.getApiV1(`resend-email/${username}`), {});
  }

  confirmEmailToken(token: string) {
    return this.httpClient.post<IResponse<IUser> & { token: string }>(this.getApiV1('confirm-account'), {}, {
      params: { token }
    })
  }

  uploadImage(params: FormData) {
    return this.httpClient.post<IResponse<string>>(this.getApiV1('files/upload-file'), params);
  }

  createCV(params: any) {
    return this.httpClient.post<IResponse<ICandidateCV>>(this.getApiV1('CV/create-cv'), params);
  }

  updateIsNeededJob(candidateId: number) {
    return this.httpClient.put<IResponse<unknown>>(this.getApiV1(`candidate/update-is-need-job`), {}, { params: { candidateId } });
  }

  updateBaseProfile(params: any) {
    return this.httpClient.put<IResponse<ICandidateProfile>>(this.getApiV1('candidate/update-cv-base-information'), params);
  }

  getTopRecruitmentCompany() {
    return this.httpClient.get<IResponse<ICompanyShort[]>>(this.getApiV1('company/get-top-12-recruitment-companies'));
  }

  getCvViewer(params: ICvViewerRequest) {
    return this.httpClient.get<IResponsePagination<IProfileView>>(this.getApiV1('candidate/get-cv-viewer'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  getListCompany() {
    return this.httpClient.get<IResponse<ICompany[]>>(this.getApiV1('company/get-list-company'));
  }

  updateRecruiterProfile(params: any) {
    return this.httpClient.put<IResponse<{
      approvedBusinessLicense: boolean,
      avatarName: string,
      avatarUrl: string,
      companyAddress: string,
      companyId: number
      companyName: string,
      email: string,
      fullName: string,
      gender: boolean,
      joinedCompany: boolean
      linkedinAccount: string,
      phone: string,
      position: string,
      recruiterId: number,
      userName: string
    }>>(this.getApiV1(`recruiter/update-recruiter-profile/${params.id}`), params);
  }

  getCompanyDetail(companyId: number, recruiterId: number = 0) {
    return this.httpClient.get<IResponse<ICompany>>(this.getApiV1('company/company-information'), { params: { companyId, recruiterId } });
  }

  getJobByCompany(companyId: number, pagination: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<Job>>(this.getApiV1('job/get-jobs-by-company'), { params: { companyId, ...pagination } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  sendRequestJoinCompany(params: any) {
    return this.httpClient.post<IResponse<IRequestJoinCompany>>(this.getApiV1('recruiter/send-request-join-company'), params);
  }

  changePasswordRequest(username: string, params: { oldPassword: string, newPassword: string, confirmPassword: string }) {
    return this.httpClient.post<IResponse<unknown>>(this.getApiV1(`password/${username}`), params);
  }

  listRentalPackage() {
    return this.httpClient.get<IResponse<IRentalPackage[]>>(this.getApiV1(`package/list-rental-package`));
  }

  listPackage(params: { name: string; rentalPackageId: PaymentRental }) {
    return this.httpClient.get<IResponsePagination<IPackage>>(this.getApiV1('package/list-package'), { params }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  getDetailPackage(packageId: number, groupPackageId: number) {
    return this.httpClient.get<IResponse<IPackageDetailResponse>>(this.getApiV1(`package/package`), { params: { packageId, groupPackageId } });
  }

  payment(params: IPaymentRequest) {
    return this.httpClient.post<IResponse<{ paymentUrl: string }>>(this.getApiV1(`payment/create-url-payment`), params);
  }

  uploadBusinessLicense(params: FormData) {
    return this.httpClient.put<IResponse<IRecruiterProfile>>(this.getApiV1(''), params);
  }

  getCompanyPersonal(params: (IRequestPagination & { companyId: number })) {
    return this.httpClient.get<IResponsePagination<ICompanyPersonal>>(this.getApiV1('recruiter/get-recruiter-by-company'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  savePaymentResult(params: any) {
    return this.httpClient.post<IResponse<unknown>>(this.getApiV1('payment/save-payment-success'), params, { params });
  }

  getTransaction(params: any) {
    return this.httpClient.get<IResponsePagination<ITransaction>>(this.getApiV1('payment/payment-information'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );
  }

  updateProfile(params: any) {
    return this.httpClient.post<IResponse<IUser>>(this.getApiV1(`user/update/${params.id}`), params);
  }

  getJobApplied(params: { candidateId: number, approvalStatus?: JobApplyState } & IRequestPagination) {
    return this.httpClient.get<IResponsePagination<JobApplied>>(this.getApiV1('candidate/get-applied-jobs-of-candidate'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );
  }

  getFollowJob(candidateId: number, pagination: IRequestPagination) {
    return this.httpClient.get<IResponsePagination<JobDetail>>(this.getApiV1(`candidate/followed-job`), { params: { candidateId, ...pagination } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }

  follow(params: { followerId: number; followedId: number; type: JobFollowType }) {
    return this.httpClient.post<IResponse<unknown>>(this.getApiV1('candidate/follow-something'), params);
  }
  unFollow(params: { followerId: number; followedId: number; type: JobFollowType }) {
    return this.httpClient.delete<IResponse<unknown>>(this.getApiV1('candidate/unfollow'), { params });
  }
  sendOTP(phone: string) {
    return this.httpClient.post<IResponse<string>>(this.getApiV1(`otp/send-otp`), {}, { params: { phone } });
  }
  verifyOTP(phone: string, code: string) {
    return this.httpClient.post<IResponse<string>>(this.getApiV1('otp/verify-otp'), {}, { params: { phone, code } });
  }
  getRecruiterAccountInfo(id: number) {
    return this.httpClient.get<IResponse<IRecruiterAccountInfo>>(this.getApiV1(`recruiter/common-infor/${id}`));
  }
  updateViewerCV(params: any) {
    return this.httpClient.post<IResponse<unknown>>(this.getApiV1('candidate/insert-who-view-cv'), params);
  }
  getSameJobs(detailJobId: number) {
    return this.httpClient.get<IResponse<JobDetail[]>>(this.getApiV1('job/same-jobs-of-other-companies'), { params: { detailJobId } });
  }
  checkSendRequestJoinCompany(senderId: number) {
    return this.httpClient.get<IResponse<IRequestJoinCompany>>(this.getApiV1('recruiter/get-sent-request'), { params: { senderId } });
  }
  reportJob(params: any, id: number) {
    return this.httpClient.post<IResponse<any>>(this.getApiV1(`job/report-job/${id}`), params);
  }
  getPurchasedPackage(recruiterId: number) {
    return this.httpClient.get<IResponse<PurchasedPackage[]>>(this.getApiV1('payment/purchased-package'), { params: { recruiterId } });
  }
  getDetailPurchasePackage(paymentId: number, recruiterId: number) {
    return this.httpClient.get<IResponse<PaymentPackageDetail>>(this.getApiV1(`banner/get-detail-purchased-package/${recruiterId}`), { params: { paymentId } });
  }

  getBusinessStatus(recruiterId: number) {
    return this.httpClient.get<IResponse<IRecruiterProfileExtend>>(this.getApiV1(`recruiter/get-business-license/${recruiterId}`));
  }

  uploadRequest(params: any) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1('recruiter/upload-business-license'), params);
  }

  applyPackageToJob(params: any) {
    return this.httpClient.put<IResponse<unknown>>(this.getApiV1('payment/change-payment-active'), params);
  }

  requestUploadBanner(recruiterId: number, params: any) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1(`banner/upload-banner/${recruiterId}`), params);
  }

  checkExistEmail(email: string) {
    return this.httpClient.get<IResponse<boolean>>(this.getApiV1('check-email-registered'), { params: { email } });
  }

  loginGoogle(param: any) {
    return this.httpClient.post<LoginResponse>(this.getApiV1('login-google'), param);
  }

  sendEmailReset(email: string) {
    return this.httpClient.post<IResponse>(this.getApiV1('forgot-password'), {}, { params: { email } });
  }

  resetPassword(params: any) {
    return this.httpClient.post<IResponse>(this.getApiV1('reset-password'), params);
  }

  getBannerShow(displayPosition: BannerPosition) {
    return this.httpClient.get<IResponse<IBannerRequest[]>>(this.getApiV1('banner/get-banner-for-candidate'), { params: { displayPosition } });
  }
  getHomepage() {
    return this.httpClient.get<IResponse<IHomepageResponse>>(this.getApiV1('job/all-job-data-homepage'));
  }
  getAllNotification(params: any) {
    return this.httpClient.get<IResponse<any> & {
      pagination: {
        page: number;
        size: number;
        totalPage: number;
        totalRecords: number;
      }
    }>(this.getApiV1('user/get-all-notification'), { params: { ...params } });
  }
  findCV(params: any) {
    return this.httpClient.get<IResponsePagination<IRecruiterFindCv>>(this.getApiV1('recruiter/find-cv'), { params: { ...params } }).pipe(
      map(response => this.indexPagination(response))
    );;
  }
  getUserInfo(id: number) {
    return this.httpClient.get<IResponse<IUser>>(this.getApiV1(`user/user-infor/${id}`));
  }
  getCvForRecruiter(recruiterId: number, cvId: number) {
    return this.httpClient.get<IResponse<ICandidateCV>>(this.getApiV1('CV/get-cv-with-pay'), { params: { recruiterId, cvId } });
  }
  updateNotification(notificationId: number) {
    return this.httpClient.put<IResponse>(this.getApiV1('user/update-notification-seen'), {}, { params: { notificationId } });
  }
  getRequestJoinCompany(params: any) {
    return this.httpClient.get<IResponsePagination<IRequestJoinCompany>>(this.getApiV1(`recruiter/get-receive-request`), { params }).pipe(
      map(response => this.indexPagination(response))
    );
  }
  updateRequestJoinCompany(params: any) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1(`recruiter/approve-join-company-request`), params);
  }
  getTotalCvView(recruiterId: number) {
    return this.httpClient.get<IResponse<number>>(this.getApiV1(`recruiter/total-view-cv/${recruiterId}`));
  }

  deleteCompanyPersonal(recruiterId: number) {
    return this.httpClient.put<IResponse<any>>(this.getApiV1(`recruiter/remove-recruiter-from-company/${recruiterId}`), {});
  }

  saveDraftJob(jobId: number) {
    return this.httpClient.put<IResponse>(this.getApiV1(`job/draft-job/${jobId}`), {});
  }

  submitFormQuestion(params: IRoom) {
    return this.httpClient.post<IResponse<IRoom>>(this.getSocketApi('room'), params);
  }

  getRoomActive() {
    return this.httpClient.get<IResponse<IRoom[]>>(this.getSocketApi('room/active'));
  }

  checkSeenCv(params: { candidateId: number, jobId: number }) {
    return this.httpClient.get<IResponse<boolean>>(this.getApiV1('recruiter/is-seen-uploaded-cv'), { params });
  }

  seeUploadCvDetail(params: { candidateId: number, jobId: number, recruiterId: number }) {
    return this.httpClient.get<IResponse<string>>(this.getApiV1('recruiter/see-upload-cv-detail'), { params });
  }

  draftJob(jobId: number) {
    return this.httpClient.put<IResponse>(this.getApiV1(`job/draft-job/${jobId}`), { jobId });
  }
}

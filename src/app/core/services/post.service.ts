import { Injectable } from '@angular/core';
import { IPost } from '@interfaces/post';
import { BehaviorSubject, finalize } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private posts = new BehaviorSubject<IPost[]>([]);
  private loading = new BehaviorSubject<boolean>(false);
  constructor(
    private apiService: ApiService
  ) { }

  private loadPost() {
    this.loading.next(true)
    this.apiService.getPost().pipe(finalize(() => this.loading.next(false))).subscribe({
      next: response => this.posts.next(response)
    });
  }

  getPost() {
    if (!this.posts.getValue().length) {
      this.loadPost();
    }
    return this.posts.asObservable();
  }

  loadingState() {
    return this.loading.asObservable();
  }
}

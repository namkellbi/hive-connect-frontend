import { Injectable } from '@angular/core';
import { SOCKET } from '@constant/settings';
import { IMessage, IMessageSend, IRoom } from '@interfaces/chat';
import { ResponseStatus } from '@interfaces/response';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject, filter, map, Subject, takeUntil } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  rooms = new BehaviorSubject<IRoom[]>([]);
  messages = new BehaviorSubject<IMessage[]>([]);
  messageDestroy$ = new Subject();
  constructor(
    // private socket: Socket,
    private apiService: ApiService
  ) {
  }

  clearMessage() {
    this.messages.next([]);
    this.messageDestroy$.next(null);
  }

  getSnapshotRoom() {
    return this.rooms.getValue();
  }

  observableRooms() {
    return this.rooms.asObservable();
  }

  fetchCreatedRoom() {
    this.apiService.getRoomActive().subscribe({
      next: response => {
        if (response.status === ResponseStatus.success) {
          this.rooms.next(response.data)
        }
      },
      error: error => console.log(error)
    })
  }

  observableRoom(roomId: number) {
    return this.observableRooms()
      .pipe(
        map(rooms => rooms.find(room => room.id === roomId) || {} as IRoom)
      );
  }

  observableRoomMessage() {
    // this.socket.fromEvent<IMessage>(SOCKET.EVENT.receiveMessage)
    //   .pipe(
    //     takeUntil(this.messageDestroy$)
    //   )
    //   .subscribe(data => {
    //     this.addMessage(data);
    //   });
    // this.socket.fromEvent<IMessage>(SOCKET.EVENT.messageSended)
    //   .pipe(
    //     takeUntil(this.messageDestroy$)
    //   )
    //   .subscribe(data => {
    //     this.addMessage(data);
    //   })
  }

  addMessage(message: IMessage) {
    const snapshot = this.messages.getValue();
    snapshot.push(message);
    this.messages.next(snapshot);
  }

  sendMessage(params: IMessageSend) {
    console.log(params);
    // this.socket.emit(SOCKET.EVENT.sendMessage, params);
  }

  onInitSocket() {
    // this.socket.fromEvent<IRoom>(SOCKET.EVENT.roomChange)
    //   .subscribe(room => {
    //     this.updateRoom(room);
    //     console.log('room change', room);
    //   });

  }

  updateRoom(room: IRoom) {
    const snapshot = this.getSnapshotRoom();
    const idx = snapshot.findIndex(item => item.id === room.id);
    if (idx > -1) {
      snapshot.splice(idx, 1, room);
    } else {
      snapshot.push(room);
    }

    this.rooms.next(snapshot);
  }

  getSocketId() {
    return ''//this.socket.ioSocket.id;
  }
}

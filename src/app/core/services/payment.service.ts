import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Banner, IPackage } from '@interfaces/payment';
import { Payment } from '@interfaces/request';
import { ResponseStatus } from '@interfaces/response';
import { BsModalService } from 'ngx-bootstrap/modal';
import { tap } from 'rxjs';
import { ApiService } from './api.service';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
  ) { }

  buyPackage(packageInfo: IPackage & { jobId: number }) {
    const { id } = this.storeService.recruiterProfile.getValue();
    const request = new Payment(id, packageInfo.discount || packageInfo.price, packageInfo, null, packageInfo.jobId);
    return this.payment(request);
  }
  buyBannerPackage(banner: Banner) {
    const { id } = this.storeService.recruiterProfile.getValue();
    const request = new Payment(id, banner.discount || banner.price, null, banner);
    return this.payment(request);
  }
  payment(request: Payment) {
    return this.apiService.payment({ ...request }).pipe(
      tap(({ status, data }) => {
        if (status === ResponseStatus.success) {
          const url = new URL(data.paymentUrl);
          this.openModalPayment(url.toString());
        }
      })
    );
  }
  savePaymentResult(query: any) {
    return this.apiService.savePaymentResult(query);
  }
  openModalPayment(url: string) {
    window.location.href = url;
  }
  getTransaction(params: any) {
    const { id } = this.storeService.getRecruiterProfile();
    params.recruiterId = id;
    return this.apiService.getTransaction(params);
  }
}

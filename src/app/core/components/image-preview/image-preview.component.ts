import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'hc-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss']
})
export class ImagePreviewComponent implements OnInit {
  @Input() url: any = '';
  @Input() showDelete = true;
  @Input() extendClass = '';
  @Output() onRemove = new EventEmitter();

  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
  }

  fullScreen($event: Event, temp: any) {
    $event.stopPropagation();
    $event.preventDefault();
    this.modalService.show(temp, {
      class: 'modal-dialog-centered modal-dialog-scrollable modal-xl'
    });
  }
}

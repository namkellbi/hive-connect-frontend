import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDateCommonComponent } from './input-date-common.component';

describe('InputDateCommonComponent', () => {
  let component: InputDateCommonComponent;
  let fixture: ComponentFixture<InputDateCommonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputDateCommonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDateCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { HelperService } from '@services/helper.service';
import { enGbLocale, } from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { InputCommonComponent } from '../input-common/input-common.component';
import { defineLocale } from 'ngx-bootstrap/chronos';

@Component({
  selector: 'hc-input-date-common',
  templateUrl: './input-date-common.component.html',
  styleUrls: ['./input-date-common.component.scss']
})
export class InputDateCommonComponent extends InputCommonComponent implements OnInit {
  @Input() minDate!: Date;
  @Input() maxDate!: Date;
  constructor(
    helperService: HelperService,
    private localeService: BsLocaleService
  ) {
    super(helperService);
    enGbLocale.invalidDate = 'Ngày không hợp lệ';
    defineLocale('custom locale', enGbLocale);
    this.localeService.use('custom locale');
  }

}

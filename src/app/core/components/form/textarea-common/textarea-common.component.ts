import { Component, OnInit } from '@angular/core';
import { HelperService } from '@services/helper.service';
import { InputCommonComponent } from '../input-common/input-common.component';

@Component({
  selector: 'hc-textarea-common',
  templateUrl: './textarea-common.component.html',
  styleUrls: ['./textarea-common.component.scss']
})
export class TextareaCommonComponent extends InputCommonComponent implements OnInit {

  constructor(
    helperService: HelperService
  ) {
    super(helperService);
  }

}

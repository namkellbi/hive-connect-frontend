import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { HelperService } from '@services/helper.service';
import { Editor, Toolbar } from 'ngx-editor';

@Component({
  selector: 'hc-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss']
})
export class TextEditorComponent implements OnInit {
  @Input() formControlName = '';
  @Input() placeHolder = '';
  @Input() controls!: AbstractControl;
  @Input() isRequired = false;
  @Input() label = '';
  @Input() id = '';
  editor!: Editor;
  toolbar: Toolbar = [
    // default value
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ];
  constructor(
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.editor = new Editor();
    this.id = this.helperService.makeId(Math.floor(Math.random() * 10 + 1)) + this.label + this.placeHolder;
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.editor.destroy();
  }

  get formControl() {
    return this.controls as FormControl;
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FORM_ERRORS } from '@constant/form';

@Component({
  selector: 'hc-form-error[controls][type][errorList]',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss']
})
export class FormErrorComponent implements OnInit {
  @Input() controls!: AbstractControl;
  @Input() errorList: string[] = [];
  @Input() type = '';
  readonly FORM_ERRORS = FORM_ERRORS;
  constructor() { }

  ngOnInit(): void {
  }

}

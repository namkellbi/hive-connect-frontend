import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSelectCommonComponent } from './input-select-common.component';

describe('InputSelectCommonComponent', () => {
  let component: InputSelectCommonComponent;
  let fixture: ComponentFixture<InputSelectCommonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputSelectCommonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSelectCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { HelperService } from '@services/helper.service';
import { InputCommonComponent } from '../input-common/input-common.component';

@Component({
  selector: 'hc-input-select-common',
  templateUrl: './input-select-common.component.html',
  styleUrls: ['./input-select-common.component.scss']
})
export class InputSelectCommonComponent extends InputCommonComponent implements OnInit {
  @Input() listOption: { value: any; label: string }[] = [];
  constructor(
    helperService: HelperService
  ) {
    super(helperService);
  }

}

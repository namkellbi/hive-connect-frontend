import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputSelectCommonComponent } from './input-select-common/input-select-common.component';
import { TextareaCommonComponent } from './textarea-common/textarea-common.component';
import { InputCommonComponent } from './input-common/input-common.component';
import { InputDateCommonComponent } from './input-date-common/input-date-common.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BootstrapModule } from 'src/app/shared/bootstrap/bootstrap.module';
import { FormErrorComponent } from './form-error/form-error.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { NgxEditorModule } from 'ngx-editor';



@NgModule({
  declarations: [
    InputSelectCommonComponent,
    TextareaCommonComponent,
    InputCommonComponent,
    InputDateCommonComponent,
    FormErrorComponent,
    TextEditorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BootstrapModule,
    NgxEditorModule
  ],
  exports: [
    InputSelectCommonComponent,
    TextareaCommonComponent,
    InputCommonComponent,
    InputDateCommonComponent,
    FormErrorComponent,
    TextEditorComponent
  ]
})
export class FormModule { }

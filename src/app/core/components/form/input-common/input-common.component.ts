import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { HelperService } from '@services/helper.service';

@Component({
  selector: 'hc-input-common[controls]',
  templateUrl: './input-common.component.html',
  styleUrls: ['./input-common.component.scss']
})
export class InputCommonComponent implements OnInit {
  @Input() set controls(controls: AbstractControl) {
    this._control = controls as FormControl;
  };
  @Input() errorList: string[] = [];
  @Input() typeInput = 'text';
  @Input() placeHolder = '';
  @Input() label = '';
  @Input() errorRoot = 'base';
  @Input() classWrapper = 'mb-24';
  @Input() isRequired = false;
  @Input() name = '';
  _control!: FormControl;
  id = '';
  constructor(
    private helperService: HelperService
  ) {
  }

  ngOnInit(): void {
    this.id = this.helperService.makeId(Math.floor(Math.random() * 10 + 1)) + this.label + this.placeHolder;
  }

  onBlur($event: FocusEvent) {
    console.log('value')
    if (this.typeInput !== 'text') return;
    const el = ($event.target as HTMLInputElement);
    const prev = el.value;
    el.value = el.value.trim();
    if (el.value.length !== prev.length) {
      this._control.patchValue(el.value);
    }
  }
}

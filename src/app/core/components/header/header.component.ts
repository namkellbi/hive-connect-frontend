import { Component, OnInit } from '@angular/core';
import { ICandidateProfile, IRecruiterProfile, IUser, ROLE } from '@interfaces/user';
import { AuthService } from '@services/auth.service';
import { INavigationItem, NavigationService } from '@services/navigation.service';
import { SearchService } from '@services/search.service';
import { IUserObserver, StoreService } from '@services/store.service';
import { combineLatest, map, Observable, startWith, tap } from 'rxjs';

@Component({
  selector: 'hc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user$ = new Observable<IUserObserver>();
  navigationItem$ = new Observable<INavigationItem[]>();
  profile$ = new Observable<ICandidateProfile>();
  fullName = '';
  readonly ROLE = ROLE;
  constructor(
    private navigationService: NavigationService,
    private storeService: StoreService,
    private authService: AuthService,
    private searchService: SearchService,
  ) { }

  ngOnInit(): void {
    this.navigationItem$ = this.navigationService.getNavigatorItem();
    this.user$ = this.storeService.observerUser();
    combineLatest([
      this.storeService.observerCandidateProfile().pipe(startWith({} as ICandidateProfile)),
      this.storeService.observerRecruiterProfile().pipe(startWith({} as IRecruiterProfile))
    ]).subscribe(([candidate, recruiter]) => {
      if (candidate?.id) {
        this.fullName = candidate.fullName;
      }
      if (recruiter?.id) {
        this.fullName = recruiter.fullName;
      }
    })
    this.searchService.getAllField();
    this.searchService.getAllCountry();
    this.searchService.fetchCompany();
  }
  logout() {
    this.authService.logout();
  }

}

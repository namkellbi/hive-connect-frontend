import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { getRegexEmail, getRegexPhone } from '@constant/form';
import { JobDetail } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { StoreService } from '@services/store.service';
import { finalize } from 'rxjs';

@Component({
  selector: 'hc-report-job',
  templateUrl: './report-job.component.html',
  styleUrls: ['./report-job.component.scss']
})
export class ReportJobComponent implements OnInit {
  @Input() job = {} as JobDetail;

  @Output() onClose = new EventEmitter();
  form!: FormGroup;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private helperService: HelperService,
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const { id } = this.activatedRoute.snapshot.params;
    this.form = this.fb.group({
      reportReason: ['', [Validators.required, Validators.maxLength(255)]],
      fullName: ['', [Validators.required, Validators.maxLength(255)]],
      phone: ['', [Validators.pattern(getRegexPhone()), Validators.maxLength(20)]],
      userAddress: ['', Validators.required, Validators.maxLength(255)],
      userEmail: ['', [Validators.required, Validators.pattern(getRegexEmail()), Validators.maxLength(50)]],
      jobId: this.job.jobId || this.job.id || 0,
      relatedLink: [`/job/${id || this.job.jobId || this.job.id || 0}`, [Validators.maxLength(255)]]
    });
    // {
    //   "jobId": 0,
    //   "reportReason": "string",
    //   "relatedLink": "string",
    //   "fullName": "string",
    //   "phone": "string",
    //   "userAddress": "string",
    //   "userEmail": "string"
    // }
  }

  submit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) return;
    const { id } = this.storeService.getUserInfo();
    this.loading = true;
    const params = this.form.value;
    this.apiService.reportJob(params, id)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.helperService.showSuccess('', 'Báo cáo thành công');
            this.onClose.emit();
          } else {
            this.helperService.showError('', 'Báo cáo thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

}

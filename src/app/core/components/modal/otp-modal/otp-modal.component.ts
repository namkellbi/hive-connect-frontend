import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ResponseStatus } from '@interfaces/response';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize, lastValueFrom } from 'rxjs';


@Component({
  selector: 'app-otp-modal',
  templateUrl: './otp-modal.component.html',
  styleUrls: ['./otp-modal.component.scss']
})
export class OtpModalComponent implements OnInit, AfterViewInit {
  @ViewChildren('formRow') rows: any;
  @Input() phoneNumber = '846158720';
  @Output() getNewOTP = new EventEmitter();
  @Output() onClose = new EventEmitter();
  form!: FormGroup;
  formInput = ['input1', 'input2', 'input3', 'input4', 'input5', 'input6'];
  countDown = 120;
  errorMessage = '';
  prefix = '+84';
  sendingOTP = false;
  callbackVerifyOTP!: (otp: string) => Promise<any>;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) {
    this.form = this.toFormGroup(this.formInput);
  }

  ngOnInit(): void {
    this.sendOTP();
  }

  sendOTP() {
    this.sendingOTP = true;
    this.apiService.sendOTP(`${this.phoneNumber}`)
      .pipe(
        finalize(() => this.sendingOTP = false)
      )
      .subscribe({
        next: response => {
          this.setCountDown();
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      if (this.rows) {
        this.rows._results[0].nativeElement.focus();
      }
    }, 300);
  }

  toFormGroup(elements: any) {
    const group: any = {};

    elements.forEach((key: string) => {
      group[key] = new FormControl('', Validators.required);
    });
    return new FormGroup(group);
  }

  setCountDown() {
    this.countDown = 120;
    const interval = setInterval(() => {
      if (this.countDown < 1) {
        clearInterval(interval);
        return;
      }
      this.countDown -= 1;
    }, 1000);
  }

  closeModal(data?: any) {
    this.onClose.emit(data);
  }

  keyUpEvent(event: KeyboardEvent, index: any) {
    this.errorMessage = '';
    let pos = index;
    if (event.key === 'Backspace') {
      pos = index - 1;
    } else {
      pos = index + 1;
    }

    if (!(event.target as HTMLInputElement).value && event.key !== 'Backspace') {
      return;
    }

    if (pos > -1 && pos < this.formInput.length) {
      this.rows._results[pos].nativeElement.focus();
    }
    this.onSubmit();
  }

  async onSubmit() {
    this.helperService.validateFormField(this.form);
    if (this.form.invalid) {
      return;
    }

    const value = this.form.value;
    const otp = Object.keys(value).map(x => value[x]).reduce((pre, cur) => pre + cur, '');

    const response = await lastValueFrom(this.apiService.verifyOTP(this.phoneNumber, otp));

    if (response.status === ResponseStatus.success && response.data.toLowerCase() === 'approved') {
      this.closeModal(response.data);
      return;
    }
    this.errorMessage = response.message || 'Xác thực thất bại';
  }

  reSendOTP() {
    if (this.countDown > 1) {
      return;
    }
    this.getNewOTP.emit();
    this.reSendOTP();
  }

}

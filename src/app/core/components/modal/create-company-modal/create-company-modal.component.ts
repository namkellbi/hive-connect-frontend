import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RecruiterBusinessLicenseComponent } from '@components/modal/recruiter-business-license/recruiter-business-license.component';
import { getRegexEmail } from '@constant/form';
import { IDropDownItem } from '@interfaces/form';
import { ICompany, ImageObj } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterProfile, RequestJoinCompany } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { SearchService } from '@services/search.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { finalize, Observable, Subject, takeUntil, tap } from 'rxjs';
import { FileFinishPendingType } from 'src/app/shared/directives/file-handle.directive';

@Component({
  selector: 'hc-create-company-modal',
  templateUrl: './create-company-modal.component.html',
  styleUrls: ['./create-company-modal.component.scss']
})
export class CreateCompanyModalComponent implements OnInit, OnDestroy {
  @Input() profile: IRecruiterProfile = {} as IRecruiterProfile;
  @Input() company = {} as ICompany;
  @Input() isCreateCompany = false;
  @ViewChild(RecruiterBusinessLicenseComponent) license!: RecruiterBusinessLicenseComponent;
  @Output() joinCompany = new EventEmitter();
  company$ = new Observable<ICompany[]>();
  companyId = 0;
  form!: FormGroup;
  fieldJob: IDropDownItem[] = [];
  loading = false;
  avatar = {} as FileFinishPendingType;
  imageListUpload: FileFinishPendingType[] = [];
  coverImageUrl = {} as FileFinishPendingType;
  imageListOriginal: ImageObj[] = [];
  imageListRemove: ImageObj[] = [];
  destroy$ = new Subject();

  constructor(
    private searchService: SearchService,
    private fb: FormBuilder,
    private helperService: HelperService,
    private apiService: ApiService,
    private modalService: BsModalService

  ) { }

  ngOnInit(): void {
    this.company$ = this.searchService.observerCompany();
    this.searchService.observerJobField()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => this.fieldJob = data.map(item => ({ label: item.fieldName, value: item.id })));
    if (this.isCreateCompany) {
      this.initForm();
      // this.form.patchValue(this.company);
      this.imageListOriginal = this.company.companyImageUrlList || [];
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  initForm() {
    this.form = this.fb.group({
      fieldWork: ['', [Validators.required, Validators.maxLength(255)]],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      email: ['', [Validators.required, Validators.pattern(getRegexEmail()), Validators.maxLength(50)]],
      phoneNumber: ['', [Validators.required, Validators.pattern(/^(0[3|5|7|8|9])([0-9]{8})$/g), Validators.maxLength(20)]],
      description: ['', [Validators.maxLength(5000)]],
      website: ['', [Validators.maxLength(255)]],
      numberEmployees: ['', [Validators.required, Validators.min(1)]],
      address: ['', [Validators.required, Validators.maxLength(255)]],
      avatar: ['', [Validators.maxLength(255)]],
      taxCode: ['', [Validators.required, Validators.pattern(/^([0-9]{13}|[0-9]{10})$/gi), Validators.maxLength(20)]],
      mapUrl: ['', Validators.maxLength(1000)],
      creatorId: [this.profile.id, [Validators.maxLength(255)]],
      avatarName: ['', [Validators.maxLength(255)]],
      url: ['']
    });
    if (this.company?.id) {
      const numberEmployees = this.company.numberEmployees.match(/[0-9]/gi);
      this.form = this.fb.group({
        companyId: this.company.id,
        companyEmail: [this.company.email, [Validators.required, Validators.pattern(/^[a-zA-Z0-9.! #$%&'*+\=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/), Validators.maxLength(50)]],
        companyPhone: [this.company.phone, [Validators.required, Validators.pattern(/^(0[3|5|7|8|9])([0-9]{8})$/g), Validators.maxLength(20)]],
        companyDescription: [this.company.description, [Validators.maxLength(5000)]],
        companyWebsite: [this.company.website, [Validators.maxLength(255)]],
        numberEmployees: [numberEmployees?.join('') || '', [Validators.required, Validators.min(1)]],
        companyAddress: [this.company.address, [Validators.required, Validators.maxLength(255)]],
        taxCode: [this.company.taxCode, [Validators.required, Validators.pattern(/^([0-9]{13}|[0-9]{10})$/gi), Validators.maxLength(20)]],
        mapUrl: [this.company.mapUrl, [Validators.maxLength(1000)]],
        avatarUrl: [this.company.avatar, [Validators.maxLength(255)]],
        coverImageUrl: ['', [Validators.maxLength(255)]],
        uploadImageUrlList: [[]],
        deleteImageIdList: [[]],
        name: [this.company.name, [Validators.maxLength(255)]],
        fieldWork: [this.company.fieldWork, [Validators.maxLength(255)]]
      });
      this.form.get('name')?.disable();
      this.form.get('fieldWork')?.disable();
    }
  }

  handleFileFinish($event: FileFinishPendingType[]) {
    if ($event && $event[0]) {
      this.avatar = $event[0];
    }
  }

  handleImageFinish($event: FileFinishPendingType[]) {
    if ($event && $event.length) {
      this.imageListUpload.push(...$event);
    }
  }

  handleFileCoverFinish($event: FileFinishPendingType[]) {
    if ($event && $event[0]) {
      this.coverImageUrl = $event[0];
    }
  }

  switchMode(state: boolean) {
    if (state) {
      this.initForm();
    } else {
      this.joinCompany.emit();
    }
    this.isCreateCompany = state;
  }

  async submit() {
    // this.helperService.validateFormField(this.form);
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    const params = { ...this.form.value };
    this.loading = true;
    try {
      await this.license.submit();
      if (this.avatar && this.avatar.file) {
        const fileUploaded = await this.helperService.uploadImage(this.avatar.file);
        params.avatar = fileUploaded.url;
        params.avatarUrl = fileUploaded.url;
        params.url = fileUploaded.url;
      }
      if (this.coverImageUrl && this.coverImageUrl.file) {
        const fileUploaded = await this.helperService.uploadImage(this.coverImageUrl.file);
        params.coverImageUrl = fileUploaded.url;
      }
      if (this.imageListUpload.length) {
        const files = this.imageListUpload.reduce((acc, cur) => cur.file ? acc.concat([cur.file]) : acc, [] as File[]);
        const filesUpload = await this.helperService.uploadBatchImage(files);
        // this.imageListOriginal.push(...filesUpload.map(file => ({ url: file.url, id: Math.random() })));
        params.uploadImageUrlList = filesUpload.map(file => file.url);
      }
      params.deleteImageIdList = this.imageListRemove.map(item => item.id);
      params.numberEmployees = `${params.numberEmployees} người`;
      if (!this.company.id) {
        params.fieldWork = this.fieldJob.find(item => +item.value === +params.fieldWork)?.label;
      }
      const api = this.company?.id
        ? this.apiService.updateCompanyInfo(this.profile.id, params)
        : this.apiService.createCompany(params);
      api
        .pipe(
          finalize(() => this.loading = false)
        )
        .subscribe({
          next: response => {
            this.helperService.callApiResponseHandler(response);
            if (response.status === ResponseStatus.success) {
              this.joinCompany.emit(response.data || {});
            }
          },
          error: error => this.helperService.callApiFailedHandler(error)
        })
    } catch (error: any) {
      this.loading = false;
      this.helperService.showError('', error?.message || 'Cập nhật thông tin thất bại')
    }
  }

  sendRequestJoin(id: number) {
    const params = {
      id: id,
      createAt: new Date().toISOString(),
      status: RequestJoinCompany.pending,
      senderId: this.profile.id,
      companyId: id,
      approverId: 0
    }
    this.loading = true;
    this.apiService.sendRequestJoinCompany(params)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.joinCompany.emit(data);
            this.helperService.showSuccess('', 'Gửi yêu cầu thành công, Bạn hãy đợi người quản lý của công ty duyệt nhé')
          } else {
            this.helperService.showError('', 'Gửi yêu cầu thất bại ')
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  removeOrigin(index: number) {
    const removed = this.imageListOriginal.splice(index, 1);
    this.imageListRemove = this.imageListRemove.concat(removed);
  }

  removeUploadImage(index: number) {
    this.imageListUpload.splice(index, 1);
  }

  removeAvatar() {
    this.avatar = {} as FileFinishPendingType;
  }

  removeCoverImage() {
    this.coverImageUrl = {} as FileFinishPendingType;
  }
  previewUrl(urlTemp: any) {
    this.modalService.show(urlTemp);
  }
}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SafeResourceUrl } from '@angular/platform-browser';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';

@Component({
  selector: 'hc-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: ['./payment-modal.component.scss']
})
export class PaymentModalComponent implements OnInit {
  url: SafeResourceUrl = '';
  constructor(
  ) { }

  ngOnInit(): void {
  }

  payment() {

  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentModalComponent } from './payment-modal/payment-modal.component';
import { CreateCompanyModalComponent } from './create-company-modal/create-company-modal.component';
import { OtpModalComponent } from './otp-modal/otp-modal.component';
import { FormModule } from '@components/form/form.module';
import { ReportJobComponent } from './report-job/report-job.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ComponentsModule } from '@components/components.module';
import { RecruiterBusinessLicenseComponent } from './recruiter-business-license/recruiter-business-license.component';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';


@NgModule({
  declarations: [
    CreateCompanyModalComponent,
    PaymentModalComponent,
    OtpModalComponent,
    ReportJobComponent,
    RecruiterBusinessLicenseComponent
  ],
  imports: [
    CommonModule,
    FormModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    AngularSvgIconModule,
    ComponentsModule,
    PipesModule
  ],
  exports: [
    CreateCompanyModalComponent,
    PaymentModalComponent,
    OtpModalComponent,
    ReportJobComponent,
    RecruiterBusinessLicenseComponent
  ]
})
export class ModalModule { }

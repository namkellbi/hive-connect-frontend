import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterBusinessLicenseComponent } from './recruiter-business-license.component';

describe('RecruiterBusinessLicenseComponent', () => {
  let component: RecruiterBusinessLicenseComponent;
  let fixture: ComponentFixture<RecruiterBusinessLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterBusinessLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterBusinessLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

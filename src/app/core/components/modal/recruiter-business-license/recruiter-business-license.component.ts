import { Component, Input, OnInit } from '@angular/core';
import { JobApplyState } from '@interfaces/job';
import { ResponseStatus } from '@interfaces/response';
import { IRecruiterProfileExtend } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize, lastValueFrom } from 'rxjs';
import { FileFinishPendingType } from 'src/app/shared/directives/file-handle.directive';

@Component({
  selector: 'hc-recruiter-business-license',
  templateUrl: './recruiter-business-license.component.html',
  styleUrls: ['./recruiter-business-license.component.scss']
})
export class RecruiterBusinessLicenseComponent implements OnInit {
  @Input() recruiterId = 0;
  @Input() isView = false;
  loadingStatus = false;
  profile = {} as IRecruiterProfileExtend;
  mainLicense = {} as FileFinishPendingType;
  otherLicense = {} as FileFinishPendingType;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.getStatus();
  }

  getStatus() {
    this.apiService.getBusinessStatus(this.recruiterId)
      .pipe(
        finalize(() => this.loadingStatus = false)
      )
      .subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.profile = data;
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }

  handleMainLicense($event: FileFinishPendingType[]) {
    if ($event && $event[0]) {
      this.mainLicense = $event[0];
    }
  }
  handleOtherLicense($event: FileFinishPendingType[]) {
    if ($event && $event[0]) {
      this.otherLicense = $event[0];
    }
  }
  get statusBusiness() {
    if (!this.profile.businessLicenseApprovalStatus) return '';
    return {
      [JobApplyState.approve]: 'Đã xác thực',
      [JobApplyState.pending]: 'Đang chờ duyệt',
      [JobApplyState.reject]: 'Bị từ chối'
    }[this.profile.businessLicenseApprovalStatus];
  }

  get statusOtherBusiness() {
    if (!this.profile.additionalLicenseApprovalStatus) return '';
    return {
      [JobApplyState.approve]: 'Đã xác thực',
      [JobApplyState.pending]: 'Đang chờ duyệt',
      [JobApplyState.reject]: 'Bị từ chối'
    }[this.profile.additionalLicenseApprovalStatus];
  }

  get status() {
    return JobApplyState;
  }

  async submit() {
    const request = this.handleBeforeSubmit();
    if (!request.get('additionalMultipartFile') && !request.get('businessMultipartFile')) return;
    try {
      const response = await lastValueFrom(this.apiService.uploadRequest(request));
      if (response.status === ResponseStatus.error) {
        throw new Error(response.message);
      }
    } catch (error: any) {
      this.helperService.showError('', error.message || 'Tải chứng nhận thất bại');
    }

  }

  handleBeforeSubmit() {
    const request = new FormData();
    request.append('recruiterId', this.recruiterId + '');
    const otherLicense = this.otherLicense.file;
    if ((!this.profile.additionalLicenseApprovalStatus || this.profile.additionalLicenseApprovalStatus !== JobApplyState.approve) && otherLicense) {
      request.append('additionalMultipartFile', otherLicense);
    }
    const license = this.mainLicense.file;
    if ((!this.profile.businessLicenseApprovalStatus || this.profile.businessLicenseApprovalStatus !== JobApplyState.approve) && license) {
      request.append('businessMultipartFile', license);
    }
    return request;
  }
  onRemove(target: FileFinishPendingType) {
    target = {} as FileFinishPendingType;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertismentJobCampainComponent } from './advertisment-job-campain.component';

describe('AdvertismentJobCampainComponent', () => {
  let component: AdvertismentJobCampainComponent;
  let fixture: ComponentFixture<AdvertismentJobCampainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertismentJobCampainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertismentJobCampainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

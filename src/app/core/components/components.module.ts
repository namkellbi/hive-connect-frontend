import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdvertisementJobCampaignComponent } from './advertisment-job-campain/advertisment-job-campain.component';
import { FooterComponent } from './footer/footer.component';
import { FormModule } from './form/form.module';
import { HeaderComponent } from './header/header.component';
import { ImagePreviewComponent } from './image-preview/image-preview.component';
import { JobItemCardSecondaryComponent } from './job-item-card-secondary/job-item-card-secondary.component';
import { JobItemCardComponent } from './job-item-card/job-item-card.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { PostSkeletonComponent } from './post-skeleton/post-skeleton.component';
import { ReportJobComponent } from './modal/report-job/report-job.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { NotificationComponent } from './notification/notification.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';


@NgModule({
  declarations: [
    PostSkeletonComponent,
    HeaderComponent,
    FooterComponent,
    SearchBarComponent,
    JobItemCardComponent,
    JobItemCardSecondaryComponent,
    AdvertisementJobCampaignComponent,
    SkeletonComponent,
    ImagePreviewComponent,
    NotFoundPageComponent,
    NotificationComponent,
    ConfirmModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    InfiniteScrollModule
  ],
  exports: [
    PostSkeletonComponent,
    HeaderComponent,
    FooterComponent,
    SearchBarComponent,
    JobItemCardComponent,
    JobItemCardSecondaryComponent,
    AdvertisementJobCampaignComponent,
    SkeletonComponent,
    ImagePreviewComponent,
    ConfirmModalComponent
  ]
})
export class ComponentsModule { }

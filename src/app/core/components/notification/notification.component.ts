import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseStatus } from '@interfaces/response';
import { INotification, NotificationType } from '@interfaces/user';
import { ApiService } from '@services/api.service';
import { HelperService } from '@services/helper.service';
import { finalize } from 'rxjs';
import { DEFAULT_PAGINATION } from '../../utils/defaultObject';

@Component({
  selector: 'hc-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  @Input() userId = 0;
  loadingNoti = false;
  pagination = { ...DEFAULT_PAGINATION };
  listNoti: INotification[] = [];
  totalsNoti = 0;
  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }
  onShown() {
    this.listNoti = [];
    this.pagination.pageNo = 1;
    this.getNotification();
  }
  getNotification() {
    this.loadingNoti = true;
    this.apiService.getAllNotification({
      ...this.pagination,
      userId: this.userId
    })
      .pipe(
        finalize(() => this.loadingNoti = false)
      )
      .subscribe({
        next: ({ status, data, message, pagination }) => {
          if (status === ResponseStatus.success) {
            this.listNoti.push(...data);
            this.totalsNoti = pagination.totalRecords;
          } else {
            this.helperService.showError('', message || 'Lấy thông báo thất bại');
          }
        },
        error: error => this.helperService.callApiFailedHandler(error)
      })
  }
  loadMore() {
    this.pagination.pageNo += 1;
    this.getNotification();
  }
  resolveUrl(item: INotification) {
    return {
      [NotificationType.apply]: `/recruiter/jobs/${item.targetId}/cv`,
      [NotificationType.confirmCompany]: `/recruiter/account/company`,
      [NotificationType.confirmCv]: `/job/${item.targetId}`,
      [NotificationType.jobChange]: `/job/${item.targetId}`,
      [NotificationType.newJobCompany]: `/company/${item.targetId}`,
      [NotificationType.packageExpired]: `/recruiter/payment/package`,
      [NotificationType.packageBuy]: `/recruiter/payment/package`,
    }[item.type]
  }
  navigate(item: INotification) {
    this.apiService.updateNotification(item.id).subscribe({
      next: response => {
        item.seen = true;
      }
    })
    this.router.navigateByUrl(this.resolveUrl(item));
  }

}

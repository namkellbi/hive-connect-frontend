import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'hc-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  @Output() onClose = new EventEmitter();
  message = '';
  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { MAP_JOB_STYLE_NAME } from '@constant/form';
import { Job } from '@interfaces/job';

@Component({
  selector: 'hc-job-item-card-secondary',
  templateUrl: './job-item-card-secondary.component.html',
  styleUrls: ['./job-item-card-secondary.component.scss']
})
export class JobItemCardSecondaryComponent implements OnInit {
  @Input() job = {} as Job;
  constructor() { }

  ngOnInit(): void {
  }
  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobItemCardSecondaryComponent } from './job-item-card-secondary.component';

describe('JobItemCardSecondaryComponent', () => {
  let component: JobItemCardSecondaryComponent;
  let fixture: ComponentFixture<JobItemCardSecondaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobItemCardSecondaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobItemCardSecondaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

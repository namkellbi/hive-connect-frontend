import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DROPDOWN_JOB_STYLE } from '@constant/form';
import { IDropDownItem } from '@interfaces/form';
import { IFieldJob } from '@interfaces/user';
import { SearchService } from '@services/search.service';
import { combineLatest, debounceTime, filter, map, merge, Observable, Subject, takeUntil, tap } from 'rxjs';
interface SearchForm {
  searchText: string;
  career: string;
  workingForm: string;
  location: string;
}

type KeySearchForm = keyof SearchForm;
@Component({
  selector: 'hc-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  fieldJob$ = new Observable<IDropDownItem<number>[]>();
  country$ = new Observable<IDropDownItem<number>[]>();
  readonly DROPDOWN_JOB_STYLE = DROPDOWN_JOB_STYLE;
  menuPosition = 45;
  sticky = false;
  destroy$ = new Subject();
  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private searchService: SearchService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.searchService.getSearchLoading()
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value) {
          this.form.enable();
        } else {
          this.form.disable();
        }
      });
    this.fieldJob$ = this.searchService.observerJobField().pipe(
      takeUntil(this.destroy$),
      map(data => data.map(item => ({ value: item.id, label: item.fieldName })))
    );
    this.country$ = this.searchService.observerCountry()
      .pipe(
        takeUntil(this.destroy$),
        map(data => data.map(item => ({ value: item.id, label: item.countryName })))
      )
    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(params => {
        this.queryParamToForm(params);
      });

    this.observeForm();

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  initForm() {
    this.form = this.fb.group({
      searchText: [''],
      career: [''],
      workingForm: [''],
      location: ['']
    });
  }

  queryParamToForm(snapshot: Params) {
    const defaultForm: SearchForm = {
      searchText: '',
      career: '',
      workingForm: '',
      location: ''
    }

    Object.keys(snapshot).forEach(key => {
      if (snapshot[key] && defaultForm[key as KeySearchForm] != null) {
        defaultForm[key as KeySearchForm] = snapshot[key];
      }
    });
    this.form.patchValue(defaultForm, { emitEvent: false });
  }

  observeForm() {
    merge(
      this.getControlObservable('career'),
      this.getControlObservable('workingForm'),
      this.getControlObservable('location'),
      this.getControlObservable('searchText')
    ).pipe(
      takeUntil(this.destroy$),
      // filter(data => data !== ''),
      debounceTime(100),
    ).subscribe(() => this.toSearchPage());
  }

  getControlObservable(name: string): Observable<string> {
    return this.form.controls[name].valueChanges;
  }

  toSearchPage() {
    const value = this.form.value;
    this.router.navigate(['/search'], { queryParams: value });
  }

  @HostListener('window:scroll', ['$event'])
  handleScroll() {
    const windowScroll = window.pageYOffset;
    if (windowScroll >= this.menuPosition) {
      this.sticky = true;
    } else {
      this.sticky = false;
    }
  }
}

import { Component, OnDestroy } from "@angular/core";
import { DROPDOWN_GENDER, MAP_GENDER_VALUE, MAP_JOB_LEVEL_VALUE, MAP_JOB_STYLE_NAME } from "@constant/form";
import { Subject } from "rxjs";

@Component({
  selector: 'hc-base',
  template: ``
})
export class BaseComponent implements OnDestroy {

  destroy$ = new Subject();

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  get genderMapper() {
    return MAP_GENDER_VALUE;
  }

  get jobStyleMapper() {
    return MAP_JOB_STYLE_NAME;
  }

  get jobLevelMapper() {
    return MAP_JOB_LEVEL_VALUE;
  }

  get dropdownGender() {
    return DROPDOWN_GENDER;
  }
}
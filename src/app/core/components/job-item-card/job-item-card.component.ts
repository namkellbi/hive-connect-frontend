import { Component, Input, OnInit } from '@angular/core';
import { MAP_JOB_STYLE_NAME } from '@constant/form';
import { Job } from '@interfaces/job';

@Component({
  selector: 'hc-job-item-card',
  templateUrl: './job-item-card.component.html',
  styleUrls: ['./job-item-card.component.scss']
})
export class JobItemCardComponent implements OnInit {
  @Input() job = {} as Job;
  @Input() isLoading = false;
  constructor() { }

  ngOnInit(): void {
  }

  get mapWorkingForm() {
    return MAP_JOB_STYLE_NAME;
  }
}

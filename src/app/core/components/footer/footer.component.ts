import { Component, OnInit } from '@angular/core';
import { IFieldJob } from '@interfaces/user';
import { INavigationItem, NavigationService } from '@services/navigation.service';
import { SearchService } from '@services/search.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'hc-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  navigationItem$ = new Observable<INavigationItem[]>();
  fieldJob: IFieldJob[] = [];
  constructor(
    private navigationService: NavigationService,
    private searchService: SearchService
  ) { }

  ngOnInit(): void {
    this.navigationItem$ = this.navigationService.getNavigatorItem();
    this.searchService.observerJobField()
      .subscribe(data => this.fieldJob = data);
  }

}

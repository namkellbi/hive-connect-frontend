import { IBase } from "./base";
import { Gender, ICandidateCV } from "./user";

export enum WorkFrom {
  remote = 'REMOTE',
  fulltime = 'FULLTIME',
  parttime = 'PARTTIME'
}

export enum JobLevel {
  one = "Fundamental Awareness",
  two = "Limited Experience",
  three = "Intermediate",
  four = "Professional"
}

export enum WorkStyle {
  remote = "REMOTE",
  fulltime = "FULLTIME",
  partTime = 'PARTTIME'
}

export enum JobApplyState {
  approve = 'Approved',
  reject = 'Reject',
  pending = 'Pending'
}

export enum JobFollowType {
  job = 1, company, recruiter
}

export enum JobState {
  draft = 'Draft', posted = 'Posted'
}
export interface Job extends IBase {
  jobId: number;
  id: number;
  companyId: number;
  recruiterId: number;
  listHashtag: string[];
  companyName: string;
  jobName: string;
  jobDescription: string;
  jobRequirement: string;
  benefit: string;
  fromSalary: number;
  toSalary: number;
  numberRecruits: number;
  rank: string;
  workForm: WorkStyle;
  gender: Gender;
  startDate: string;
  endDate: string;
  workPlace: string;
  createdAt: string;
  updatedAt: string;
  isPopularJob: boolean;
  isNewJob: boolean;
  isUrgentJob: boolean;
  isDeleted: number;
  weekday: string;
  companyAvatar: string;
  countryId: number;
  fieldId: number;
  payment: boolean;
}

export interface ImageObj { id: number, url: string }
export interface ICompany extends IBase {
  address: string;
  avatar: string;
  description: string;
  email: string;
  fieldWork: string;
  id: number;
  isDeleted: number;
  mapUrl: string;
  name: string;
  numberEmployees: string;
  phone: string;
  taxCode: string;
  website: string;
  creator: boolean;
  companyImageUrlList: ImageObj[];
  coverImageUrl: string;
}
export interface ICompanyShort {
  companyId: number;
  applyCvNumber: number;
  companyAvatar: string;
  companyName: string;
}
export interface JobDetail extends Job {
  experience: string;
  applied: boolean;
  approvalStatus: JobApplyState;
  company: ICompany;
  following: boolean;
  fieldWork: string;
  academicLevel: string;
}

export interface ICountry {
  id: number;
  countryName: string;
}
export interface JobRecruiterView extends IBase {
  id: number;
  jobId: number;
  jobName: string;
  companyName: string;
  workPlace: string;
  fromSalary: number;
  toSalary: number;
  viewCount: number;
  applyCount: number;
  endDate: string;
  flag: JobState;
  canEdit?: boolean;
}

export interface JobApplied extends IBase {
  approvalStatus: JobApplyState;
  appliedJobDate: string;
  appliedJobId: number;
  approvalDate: string;
  companyName: string;
  companyAvatar: string;
  job: Job;
}

import { IBase } from "./base";

export interface IUserPost {
  id: number;
  username: string;
  displayName: string;
  avatar: string;
}

export interface ICommentPost {
  content: string;
  react: number;
  createdAt: string;
  user: IUserPost;
}

export interface IAttachmentPost {
  type: AttachmentPost,
  url: string;
}

export enum AttachmentPost {
  image = "image",
  video = "video"
}

export interface IPost {
  id: number;
  user: IUserPost,
  content: string,
  attachment: IAttachmentPost[],
  createdAt: string;
  react: number;
  comments: ICommentPost[]
}

export interface IRecruiterFindCv extends IBase {
  id: number;
  candidateId: number;
  isDeleted: number;
  summary: string;
  totalExperienceYear: string;
  createdAt: string;
  updatedAt: string;
  techStack: string;
  address: string;
  fullName: string;
  avatarUrl: string;
}

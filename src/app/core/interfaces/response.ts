import { Job } from "./job";
import { Banner, IPackage } from "./payment";
import { ICandidateProfile, IRecruiterProfile, IUser } from "./user";

export enum ResponseStatus {
  success = 'Success',
  error = 'Error',
  notFound = 'NotFound'
}

export enum ResponseMessage {
  paymentSuccess = 'PAYMENT_SUCCESS'
}

export interface IResponse<T = unknown> {
  data: T;
  status: ResponseStatus;
  message: string;
}

export interface LoginUserInfoResponse {
  user: IUser;
  candidate?: ICandidateProfile;
  recruiter?: IRecruiterProfile;
  joinedCompany: boolean;
  approvedBusinessLicense: boolean;
  verifiedEmail: boolean;
  createdOrRequestedJoinCompany: boolean;
}

export interface LoginResponse extends IResponse<LoginUserInfoResponse> {
  token: string;
}

export interface BaseResponse<key = 'id'> {
  key: number;
}

export type IResponsePagination<T> = IResponse<
  IResponse<T[]> & {
    pagination: {
      page: number;
      size: number;
      totalPage: number;
      totalRecords: number;
    }
  }>

export interface IDecodeGoogleCredential {
  aud: string;
  azp: string;
  email: string;
  email_verified: true
  exp: number;
  family_name: string;
  given_name: string;
  hd: string;
  iat: number;
  iss: string;
  jti: string;
  name: string;
  nbf: number;
  picture: string;
  sub: string;

}

export interface IHomepageResponse {
  remoteJob: Job[];
  parttimeJob: Job[];
  fulltimeJob: Job[];
  popularJob: Job[];
  urgentJob: Job[];
  newJob: Job[];
  jobByFields: Job[];
  suggestJobs: Job[];
}

export interface IPackageDetailResponse {
  bannerPackage: Banner;
  normalPackage: IPackage;
}
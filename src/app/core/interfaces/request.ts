import { JobApplyState } from "./job";
import { Banner, IPackage, PaymentRental } from "./payment";
import { ROLE } from "./user";

export enum FileUploadType {
  image = 'image',
  cv = 'cv'
}

export interface ILoginRequest {
  username: string;
  password: string;
}

export interface IRegisterRequest {
  username: string;
  password: string;
  confirmPassword: string;
  email: string;
  roleId: ROLE;
}

export interface IRequestPagination {
  pageNo: number;
  pageSize: number;
}

export interface IFindJobRequest extends IRequestPagination {
  fieldId: number;
  countryId: number;
  jobName: string;
  fromSalary: number;
  toSalary: number;
  rank: string;
  workForm: string;
  workPlace: string;
  sortBy: string;
}

export interface ICvViewerRequest extends IRequestPagination {
  cvId: number;
  candidateId: number;
}

export interface IChangeApplyRequestState {
  jobId: number;
  candidateId: number;
  approvalStatus: JobApplyState;
  createdAt: string;
  updatedAt: string;
  message: string;
}

export interface IRequestUploadImage {
  userId: number;
  companyId: number;
  eventId: number;
  candidatePostId: number;
  recruiterPostId: number;
  isAvatar: boolean;
  isBanner: boolean;
  type: 'Avatar' | 'CompanyImage'; //type Avatar || CompanyImage
}


export interface IPaymentRequest {
  recruiterId: number;
  detailPackageId: number;
  bannerId: number;
  amount: number;
  orderType: string;
  description: string;
  bankCode: string;
}

export class Payment implements IPaymentRequest {
  recruiterId: number;
  detailPackageId: number;
  bannerId: number;
  amount: number;
  orderType = "Mua";
  description = "Mua";
  bankCode: string = 'NCB';
  jobId = 0;
  constructor(recruiterId: number, amount: number, packageDetail: IPackage | null = null, banner: Banner | null = null, jobId = 0) {
    this.recruiterId = recruiterId;
    this.amount = amount;
    this.detailPackageId = packageDetail ? packageDetail.id : 0;
    this.bannerId = banner ? banner.id : 0;
    this.jobId = jobId;
  }
}
import { IBase } from "./base";
import { JobApplyState, JobRecruiterView } from "./job";

export interface IRentalPackage {
  id: number;
  packageGroup: string;
  description: string;
}

export enum PaymentRental {
  candidate = 1,
  advertisement = 2,
  banner = 3
}

export enum PackageType {
  basic = 'basic', classic = 'classic', gold = 'gold'
}

export enum BannerPosition {
  spotlight = 'spotlight',
  homepageBannerA = 'homepageBannerA',
  homepageBannerB = 'homepageBannerB',
  homepageBannerC = 'homepageBannerC',
  jobBannerA = 'jobBannerA',
  jobBannerB = 'jobBannerB',
  jobBannerC = 'jobBannerC'
}

export enum BannerRequestPosition {
  spotlight = 'spotlight',
  homepageBannerA = 'homepageBannerA',
  homepageBannerB = 'homepageBannerB',
  homepageBannerC = 'homepageBannerC',
  jobBannerA = 'jobBannerA',
  jobBannerB = 'jobBannerB',
  jobBannerC = 'jobBannerC'
}
export interface IPackage extends IBase {
  id: number;
  rentalPackageId: PaymentRental; //1
  type: PackageType; // basic, classic, gold, GÓI MUA THÊM
  price: number;
  discount: number;
  timeExpired: string;
  description: string; // mô tả về gói khuyến mãi
  labelName: string; //Việc làm tốt nhất , việc làm nổi bật, việc làm hấp dẫn , none
  relatedJob: boolean;
  suggestJob: boolean;
  goldenHour: boolean;
  normalHour: boolean;
  maxUploadSixImage: boolean;
  urgent: boolean; // tin tuyển dụng được gán gấp vào tiêu đề //nếu type ( line 7 = GÓI MUA THÊM và isUrgent = true thì được gán GẤP vào tiêu đề)
  hot: boolean;
  goodJob: boolean;
  maxCvView: number; // được xem full thông tin tối đa bao nhiêu cv
  detailName: string;
  title: string;
  popularJob: boolean;
  newJob: boolean;
  urgentJob: boolean;
}


export interface PaymentPackage {
  id: number;
  recruiterId: number;
  jobId: number;
  detailPackageId: number;
  bannerId: number;
  transactionCode: string; // mã giao dịc:h
  amount: number;
  description: string;
  orderType: string;
  bankCode: string;
  command: string;
  currCode: string;
  local: string;
  expiredDate: string; // ngày hết hạn gói package đã thuê
  expiredStatus: boolean;
  detailPackageName: string;
}

export interface Banner {
  id: number;
  rentalPackageId: number;
  price: number;
  discount: number;
  timeExpired: string;
  title: string;
  description: string;
  image: string;
  spotlight: boolean;
  homepageBannerA: boolean;
  homePageBannerB: boolean;
  homePageBannerC: boolean;
  jobBannerA: boolean;
  jobBannerB: boolean;
  jobBannerC: boolean;
  isDeleted: boolean;
}
export interface IBannerRequest {
  approvalStatus: JobApplyState;
  displayPosition: BannerPosition;
  imageUrl: string
}
export type BannerPackageDetail = { [key in BannerPosition]: IBannerRequest };
export interface PaymentPackageDetail {
  infoPPRes: {
    normalPaymentPackage: {
      detailPackageName: string;
      discountPrice: number;
      groupPackageId: number;
      groupPackageName: string;
      maxCvView: number;
      price: number;
      relatedJob: boolean;
      suggestJob: boolean;
      timeExpired: string;
    };
    bannerPaymentPackage: {
      [key in BannerRequestPosition]: boolean
    };
  };
  jobPurchasedPPRes: JobRecruiterView;
  banner: {
    [key in BannerRequestPosition]: IBannerRequest
  };
}


export interface PaymentPackageDetailInfoNormal {

}

export interface ITransaction extends IBase {
  id: number;
  recruiterId: number;
  detailPackageId: number;
  bannerId: number;
  transactionCode: number;
  amount: number;
  description: string;
  orderType: string;
  bankCode: string;
  command: string;
  currCode: string;
  local: string;
  expiredDate: string;
  createdAt: string;
  updatedAt: string;
}

export interface PurchasedPackage {
  amount: number;
  bankCode: string;
  bannerId: number;
  description: string;
  detailPackageId: number;
  detailPackageName: string;
  jobId: number;
  orderType: string;
  paymentId: number;
  recruiterId: number;
}
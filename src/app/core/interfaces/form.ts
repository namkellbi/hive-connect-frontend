export interface FileUpload {
  url: string | ArrayBuffer | null;
  file: File;
}

export interface IFilterOption {
  value: string;
  label: string;
}

export interface IFilterBar {
  career: IFilterOption[];
  workingForm: IFilterOption[];
  location: IFilterOption[];
}

export interface IDropDownItem<T = number> {
  value: T;
  label: string
}
interface Base {
  id: number;
  createdAt: Date;
  updatedAt?: Date;
  createdBy?: string;
  updatedBy?: string;
}

export enum RoomStatus {
  created = 'created',
  process = 'process',
  completed = 'completed'
}

export interface IRoom extends Base {
  email: string;
  phone: string;
  questionerId: number;
  supporterId: number;
  status: RoomStatus;
  title: string;
  questionerSocket: string;
  supporterSocket: string;
}

export interface IMessage extends Base {
  content: string;
  ownerId: number;
  roomId: number;
  ownerSocketId: string;
}

export interface IMessageSend extends IMessage {
  from: string;
  to: string;
}

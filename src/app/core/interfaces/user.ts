import { IBase } from "./base";
import { ICompany, JobApplyState, JobLevel } from "./job";
import { BaseResponse } from "./response";

export interface IUser {
  id: number;
  username: string;
  password: string;
  email: string;
  roleId: ROLE;
  isDeleted: number;
  lastLoginTime: string;
  avatar: string;
  verifiedEmail: boolean;
  verifiedPhone: boolean;
  createdAt: string;
  updatedAt: string;
  phone: string;
}

export enum ROLE {
  admin = 1,
  recruiter,
  candidate,
  collaborator
}

export enum Gender {
  // unknown = 'unknown',
  male = 'true',
  female = 'false'
}

export interface ICandidateProfile extends BaseResponse {
  id: number;
  userId: number;
  gender: boolean;
  birthDate: string;
  country: string;
  fullName: string;
  address: string;
  socialLink: string;
  avatarUrl: string;
  needJob: boolean;
  experienceLevel: number;
  introduction: string;
  verifiedPhone: boolean;
}

export interface ICertificate {
  certificateName: string;
  certificateUrl: string;
  status: number;
  cvId: number;
  id: number;
}

export class Certificate implements ICertificate {
  certificateName!: string;
  certificateUrl!: string;
  status!: number;
  cvId!: number;
  id!: number;
}

export interface IEducation {
  id: number;
  cvId: number;
  school: string;
  major: string;
  startDate: string;
  endDate: string;
  studying: boolean;
  description: string;
}

export class Education implements IEducation {
  id!: number;
  cvId!: number;
  school!: string;
  major!: string;
  startDate!: string;
  endDate!: string;
  studying!: boolean;
  description!: string;

}
export interface ILanguage {
  id: number;
  language: string;
  level: JobLevel;
  cvId: number;
}

export class Language implements ILanguage {
  id!: number;
  language!: string;
  level!: JobLevel;
  cvId!: number;

}

export interface IMajor {
  id: number;
  fieldId: number;
  majorId: number;
  cvId: number;
  level: JobLevel;
  status: boolean;
  fieldName: string;
  majorName: string;
}

export class Major implements IMajor {
  id!: number;
  fieldId!: number;
  majorId!: number;
  cvId!: number;
  level!: JobLevel;
  status!: boolean;
  majorName!: string;
  fieldName!: string;
}
export interface IOtherSkill {
  id: number;
  skillName: string;
  cvId: number;
  level: JobLevel;
}

export class OtherSkill implements IOtherSkill {
  id!: number;
  skillName!: string;
  cvId!: number;
  level!: JobLevel;

}

export interface IWorkExperience {
  id: number;
  cvId: number;
  companyName: string;
  position: string;
  startDate: string;
  endDate: string;
  description: string;
  working: boolean;
}

export class WorkExperiences implements IWorkExperience {
  id!: number;
  cvId!: number;
  companyName!: string;
  position!: string;
  startDate!: string;
  endDate!: string;
  working!: boolean;
  description!: string;

}
export interface ICandidateCV {
  id: number;
  candidateId: number;
  isDeleted: number;
  summary: string;
  createdAt: string;
  updatedAt: string;
  certificates: ICertificate[];
  educations: IEducation[];
  languages: ILanguage[];
  majorLevels: IMajor[];
  otherSkills: IOtherSkill[];
  workExperiences: IWorkExperience[];
  totalExperienceYear: string;

  gender: boolean;
  birthDate: string;
  country: string;
  fullName: string;
  address: string;
  socialLink: string;
  avatarUrl: string;
  experienceLevel: number;
  introduction: string;
  phoneNumber: string;
  email: string;
}

export interface IFieldJob {
  id: number;
  fieldName: string;
}

export interface IFieldMajor {
  id: number;
  fieldId: number;
  majorName: string
}

export interface IRecruiterProfile extends IBase {
  id: number;
  recruiterId: number;
  companyId: number;
  companyName: string;
  fullName: string;
  verifyAccount: boolean;
  verifyPhoneNumber: boolean;
  gender: Gender;
  position: string;
  linkedinAccount: string;
  businessLicense: string;
  userId: number;
  phone: string;
  joinedCompany: boolean;
  approvedBusinessLicense: boolean;
  verifiedPhone: boolean;
  totalCvView: number;
}

export interface ICompanyPersonal extends IBase {
  avatar: string;
  email: string;
  fullName: string;
  gender: boolean;
  linkedinAccount: string;
  phone: string;
  position: string;
  recruiterId: number;
  userName: string
}
export interface IRecruiterProfileExtend extends IRecruiterProfile {
  businessLicense: string,
  additionalLicense: string,
  businessLicenseUrl: string,
  additionalLicenseUrl: string,
  businessLicenseApprovalStatus: JobApplyState,
  additionalLicenseApprovalStatus: JobApplyState,
}

export interface ICVApply extends IBase {
  jobId: number;
  candidateId: number;
  candidateName: string;
  avatar: string;
  experienceDesc: string[];
  educations: string[];
  careerGoal: string;
  address: string;
  cvUrl: string;
  approvalStatus: JobApplyState;
  cvId: number;
}
export enum RecruiterInfoCheck {
  email = 'email', phone = 'phone', company = 'company', license = 'license', verified = ''
}
export interface IRecruiterAccountInfo {
  recruiterFullName: string;
  verifyStep: string;
  totalCreatedJob: number;
  candidateApplyPercentage: number;
  message: string;
  uncheck: RecruiterInfoCheck
}
export enum RequestJoinCompany {
  pending = 'Pending', approve = 'Approve', deny = 'Deny'
}
export interface IRequestJoinCompany extends IBase {
  id: number;
  status: RequestJoinCompany;
  senderId: number;
  companyId: number;
  approverId: number;
  fullName: string;
  email: string;
  phone: string;
  company: ICompany;
}

export enum NotificationType {
  apply = 1,
  confirmCv,
  confirmCompany,
  packageExpired,
  packageBuy,
  jobChange,
  newJobCompany
}
export interface INotification {
  id: number;
  receiverId: number;
  type: NotificationType;
  date: string;
  content: string;
  seen: boolean;
  delete: boolean;
  targetId: number;
}

export interface IProfileView extends IBase {
  viewerName: string;
  companyId: number;
  companyName: string;
  viewDate: string;
  companyAvatar: string;
}

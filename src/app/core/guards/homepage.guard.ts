import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ROLE } from '@interfaces/user';
import { StoreService } from '@services/store.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomepageGuard implements CanActivate {
  constructor(private storeService: StoreService, private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = this.storeService.getAccessToken();
    const info = this.storeService.getUserInfo();
    if (!token || !info) {
      return true;
    }
    const { roleId } = info;
    return roleId === ROLE.candidate || this.router.parseUrl('/recruiter/introduction');
  }

}

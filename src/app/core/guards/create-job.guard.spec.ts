import { TestBed } from '@angular/core/testing';

import { CreateJobGuard } from './create-job.guard';

describe('CreateJobGuard', () => {
  let guard: CreateJobGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CreateJobGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

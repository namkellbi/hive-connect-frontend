import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { StoreService } from '@services/store.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VerifyAccountGuard implements CanActivate {
  constructor(
    private storeService: StoreService,
    private router: Router
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = this.storeService.getAccessToken();
    if (token) {
      const { verifiedEmail } = this.storeService.getUserInfo();
      if (verifiedEmail) {
        return true;
      } else {
        this.router.navigate(['/auth', 'register'], {
          queryParams: {
            step: 3
          }
        })
        return false;
      }
    }
    return true;
  }

}

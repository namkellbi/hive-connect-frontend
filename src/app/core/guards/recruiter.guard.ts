import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ROLE } from '@interfaces/user';
import { StoreService } from '@services/store.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecruiterGuard implements CanActivate {
  constructor(private storeService: StoreService, private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = this.storeService.getAccessToken();
    const { roleId } = this.storeService.getUserInfo();
    if (token && roleId && roleId === ROLE.recruiter) {
      return true;
    }

    return this.router.parseUrl('/recruiter/introduction');
  }

}

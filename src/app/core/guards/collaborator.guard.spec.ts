import { TestBed } from '@angular/core/testing';

import { CollaboratorGuard } from './collaborator.guard';

describe('CollaboratorGuard', () => {
  let guard: CollaboratorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CollaboratorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

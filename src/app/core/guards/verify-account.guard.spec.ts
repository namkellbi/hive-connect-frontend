import { TestBed } from '@angular/core/testing';

import { VerifyAccountGuard } from './verify-account.guard';

describe('VerifyAccountGuard', () => {
  let guard: VerifyAccountGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(VerifyAccountGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

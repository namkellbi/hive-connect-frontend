import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { StoreService } from '@services/store.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private storeService: StoreService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = this.storeService.getAccessToken();
    const userInfo = this.storeService.getUserInfo();
    if (token && userInfo && token !== '' && userInfo.id) {
      return true;
    }
    this.storeService.clearStore();
    return this.router.parseUrl('/home');
  }

}

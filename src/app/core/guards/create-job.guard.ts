import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { RecruiterInfoCheck } from '@interfaces/user';
import { StoreService } from '@services/store.service';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreateJobGuard implements CanActivate {
  constructor(
    private storeService: StoreService,
    private router: Router
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.storeService.observerRecruiterVerifyInfo()
      .pipe(
        map(({ uncheck }) => uncheck === RecruiterInfoCheck.verified || this.router.parseUrl('/recruiter/jobs'))
      );
  }

}

import { TestBed } from '@angular/core/testing';

import { CreateJobResolver } from './create-job.resolver';

describe('CreateJobResolver', () => {
  let resolver: CreateJobResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CreateJobResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});

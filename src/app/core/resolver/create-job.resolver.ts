import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot, Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { RecruiterInfoCheck } from '@interfaces/user';
import { StoreService } from '@services/store.service';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreateJobResolver implements Resolve<boolean> {
  constructor(
    private storeService: StoreService
  ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.storeService.observerRecruiterVerifyInfo()
      .pipe(
        map(({ uncheck }) => uncheck === RecruiterInfoCheck.verified)
      );;
  }
}

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { BehaviorSubject, catchError, filter, Observable, of, switchMap, take, throwError } from 'rxjs';
import { ApiService } from '@services/api.service';
import { AuthService } from '@services/auth.service';
import { StoreService } from '@services/store.service';

@Injectable()
export class AuthenticateInterceptor implements HttpInterceptor {
  constructor(
    private storeService: StoreService,
    private authService: AuthService
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.storeService.getAccessToken();
    const headers: any = {};

    if (token) {
      headers['Authorization'] = 'Bearer ' + token;
    }

    request = request.clone({ headers: new HttpHeaders(headers) });

    return next.handle(request).pipe(catchError(err => {
      if (err instanceof HttpErrorResponse && !request.url.includes('signup') && err.status === 401) {
        this.authService.logout();
      }
      return throwError(() => err);
    }));
  }
}

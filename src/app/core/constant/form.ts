import { IDropDownItem } from "@interfaces/form";
import { JobApplyState, JobLevel, WorkStyle } from "@interfaces/job"
import { PaymentRental } from "@interfaces/payment";
import { IRequestUploadImage } from "@interfaces/request";
import { Gender, ICandidateCV } from "@interfaces/user";

const FORM_ERRORS: { [key: string]: { [key: string]: string } } = {
  email: {
    required: 'Email không được để trống',
    email: 'Email không đúng định dạng',
    pattern: 'Email không đúng định dạng',
    maxlength: 'Email quá dài'
  },
  password: {
    required: 'Mật khẩu không được để trống',
    notMatch: 'Mật khẩu không trùng',
    maxlength: 'Mật khẩu quá dài',
    pattern: 'Mật khẩu chứa ít nhất 1 ký tự đặc biệt, 1 số, 1 chữ hoa, 1 chữ thường từ 8 đến 50 ký tự'
  },
  username: {
    required: 'Tên hiển thị không được để trống',

  },
  base: {
    required: 'Không được bỏ trống',
    largerThanRange: 'Số lớn hơn mức lương tối đa',
    smallerThanRange: 'Số nhỏ hơn mức lương tối thiểu',
    min: 'Số quá nhỏ, xin nhập lại',
    pattern: 'Không đúng định dạng',
    minlength: 'Độ dài ký tự quá ngắn',
    maxlength: 'Độ dài ký tự quá dài'
  },
  phone: {
    pattern: 'Số điện thoại phải có 10 chữ số trong khoảng 0-9',
    required: 'Số điện thoại không được bỏ trống'
  },
  tax: {
    required: 'Mã số thuế không được bỏ trống',
    pattern: 'Mã số thuế phải có 10 hoặc 13 số'
  }
}


const MAP_JOB_STYLE_NAME = {
  [WorkStyle.fulltime]: 'Toàn thời gian',
  [WorkStyle.partTime]: 'Bán thời gian',
  [WorkStyle.remote]: 'Làm từ xa'
}
const MAP_GENDER_VALUE = {
  // [Gender.unknown]: 'Không xác định',
  [Gender.male]: 'Nam',
  [Gender.female]: 'Nữ'
}
const MAP_JOB_LEVEL_VALUE = {
  [JobLevel.one]: 'Chưa có kinh nghiệm',
  [JobLevel.two]: "Cơ bản",
  [JobLevel.three]: 'Thành thạo',
  [JobLevel.four]: 'Chuyên gia'
}
const MAP_JOB_APPLY_STATE = {
  [JobApplyState.approve]: 'Chấp thuận',
  [JobApplyState.reject]: 'Bị từ chối',
  [JobApplyState.pending]: 'Đang xử lý'
}
const MAP_RENTAL_PACKAGE = {
  [PaymentRental.advertisement]: 'Dịch vụ chính',
  [PaymentRental.candidate]: 'Gói mở hồ sơ ứng viên',
  [PaymentRental.banner]: 'Banner quảng cáo',
}
const DROPDOWN_LEVEL: IDropDownItem<JobLevel>[] = Object.entries(JobLevel).map(([key, value]) => ({ value: value, label: MAP_JOB_LEVEL_VALUE[value] }));
const DROPDOWN_GENDER: IDropDownItem<Gender>[] = Object.entries(Gender).map(([key, value]) => ({ value: value, label: MAP_GENDER_VALUE[value] }));
const DROPDOWN_JOB_STYLE: IDropDownItem<WorkStyle>[] = Object.entries(WorkStyle).map(([key, value]) => ({ value: value, label: MAP_JOB_STYLE_NAME[value] }));
const DROPDOWN_JOB_STATE: IDropDownItem<JobApplyState>[] = Object.entries(JobApplyState).map(([key, value]) => ({ value: value, label: MAP_JOB_APPLY_STATE[value] }));
const DROPDOWN_RENTAL_PACKAGE: IDropDownItem<PaymentRental>[] = [
  { value: PaymentRental.advertisement, label: MAP_RENTAL_PACKAGE[PaymentRental.advertisement] },
  { value: PaymentRental.banner, label: MAP_RENTAL_PACKAGE[PaymentRental.banner] },
  { value: PaymentRental.candidate, label: MAP_RENTAL_PACKAGE[PaymentRental.candidate] },
];
const DEFAULT_IMAGE_UPLOAD: IRequestUploadImage = {
  candidatePostId: 0,
  companyId: 0,
  eventId: 0,
  isAvatar: false,
  isBanner: false,
  recruiterPostId: 0,
  type: 'Avatar',
  userId: 0
}
const DEFAULT_CANDIDATE_CV: ICandidateCV = {
  candidateId: 0,
  certificates: [],
  createdAt: '',
  educations: [],
  id: 0,
  isDeleted: 0,
  languages: [],
  majorLevels: [],
  otherSkills: [],
  summary: '',
  updatedAt: '',
  workExperiences: [],
  totalExperienceYear: '',
  address: '',
  avatarUrl: '',
  birthDate: '',
  country: '',
  email: '',
  experienceLevel: 0,
  fullName: '',
  gender: false,
  introduction: '',
  phoneNumber: '',
  socialLink: ''
}
const REGEX_PHONE = /((84)+([0-9]{8})\b)/gi;
const REGEX_PHONE_WITHOUT_PREFIX = () => /^(0[3|5|7|8|9])([0-9]{8})$/g;
const REGEX_TAX = () => /^([0-9]{13}|[0-9]{10})$/gi;
const getRegexPhone = () => /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/g;
const getRegexEmail = () => /^[a-zA-Z0-9.! #$%&'*+\=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/;
const getRegexTax = () => /^([0-9]{13}|[0-9]{10})$/gi;
const getRegexPassword = () => /^(?=.*[a-z]){1}(?=.*[A-Z]){1}(?=.*[0-9]){1}(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]){1}.{4,50}$/;
const getFullNameRegex = () => /([\p{L}]+\s*)+/ug; ///([\p{L}\p{N}_\-\s]+)/ug
export {
  FORM_ERRORS,
  DROPDOWN_LEVEL,
  MAP_JOB_STYLE_NAME,
  MAP_GENDER_VALUE,
  DROPDOWN_JOB_STYLE,
  DROPDOWN_GENDER,
  DEFAULT_IMAGE_UPLOAD,
  MAP_JOB_LEVEL_VALUE,
  DEFAULT_CANDIDATE_CV,
  REGEX_PHONE,
  REGEX_PHONE_WITHOUT_PREFIX,
  DROPDOWN_JOB_STATE,
  MAP_JOB_APPLY_STATE,
  MAP_RENTAL_PACKAGE,
  DROPDOWN_RENTAL_PACKAGE,
  REGEX_TAX,
  getRegexPhone,
  getRegexEmail,
  getRegexTax,
  getRegexPassword,
  getFullNameRegex
}
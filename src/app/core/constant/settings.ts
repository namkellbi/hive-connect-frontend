import { jsPDFOptions, Point } from "jspdf";
import { environment } from "src/environments/environment"

const PREFIX = environment.PREFIX

const STORAGE_KEY = {
  ACCESS_TOKEN: PREFIX + 'jwt_access_token',
  REFRESH_TOKEN: PREFIX + 'refresh_token',
  USER_INFO: PREFIX + 'user_info',
  EXPIRED: PREFIX + 'jwt_expired',
  CHAT_TOKEN: PREFIX + 'chat_access_token',
  CHAT_ID: PREFIX + 'chat_id',
  LATEST_USER: PREFIX + 'latest_user',
  GOOGLE_INFO_TEMP: PREFIX + 'google_info_account',
  USERNAME: PREFIX + 'username_temp',
  TOKEN_TEMP: PREFIX + 'jwt_temp',
  EMAIL_TEMP: PREFIX + 'email_temp'
}
const DEFAULT_SIZE_IMAGE = 5000000;
const DEFAULT_SIZE_VIDEO = 20000000;
const DEFAULT_COVER_URL = 'https://i.pinimg.com/564x/90/b2/a7/90b2a77cb4d578ed9457ebf3953587b5.jpg';
const DEFAULT_AVATAR_URL = '/assets/images/avatar-default.png';
const DEFAULT_BANNER_URL = '/assets/images/hompage-candidate-banner.png';
const DEFAULT_COMPANY_URL = '/assets/images/company_default.webp';
const PDF_PARAMETERS: jsPDFOptions = {
  orientation: 'p',
  unit: 'mm',
  format: 'a4'
};
const PDF_IMAGE_WIDTH = 200;
const IMAGE_TYPE = 'image/png';
const PDF_POSITION: Point = {
  x: 0,
  y: 0
};
const SOCKET = {
  EVENT: {
    socketId: 'socket_id',
    sendMessage: 'send-message',
    receiveMessage: 'receive-message',
    roomChange: 'room-changed',
    messageSended: 'message-sended'
  }
}
export {
  STORAGE_KEY,
  DEFAULT_SIZE_IMAGE,
  DEFAULT_SIZE_VIDEO,
  DEFAULT_COVER_URL,
  DEFAULT_AVATAR_URL,
  PDF_PARAMETERS,
  IMAGE_TYPE,
  PDF_POSITION,
  PDF_IMAGE_WIDTH,
  DEFAULT_BANNER_URL,
  DEFAULT_COMPANY_URL,
  SOCKET
}
import { Component, OnInit } from '@angular/core';
import { ResponseStatus } from '@interfaces/response';
import { ROLE } from '@interfaces/user';
import { ChatService } from '@services/chat.service';
import { HelperService } from '@services/helper.service';
import { SearchService } from '@services/search.service';
import { StoreService } from '@services/store.service';
import { UserService } from '@services/user.service';
import { finalize, map, switchMap, throwError } from 'rxjs';

@Component({
  selector: 'hc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'hive-connect';
  loading = true;
  constructor(
    private storeService: StoreService,
    private searchService: SearchService,
    private userService: UserService,
    private helperService: HelperService,
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.getDataUser();
    this.chatService.onInitSocket();
  }

  getDataUser() {
    const userInfo = this.storeService.getUserInfo();
    this.storeService.userInfo.next(userInfo);
    if (userInfo && this.storeService.getAccessToken()) {
      this.storeService.setUserInfo(userInfo);
      this.storeService.userInfo.next(userInfo);
      const { roleId } = userInfo;
      this.userService.fetchUserInfo(userInfo.id).subscribe({
        next: ({ status, data }) => {
          if (status === ResponseStatus.success) {
            this.storeService.updateUserInfo(data);
          }
        }
      })
      if (roleId === ROLE.candidate) {
        this.userService.fetchCandidateProfile()
          .pipe(
            switchMap(({ status, data, message }) => {
              this.loading = false
              if (status === ResponseStatus.success) {
                this.storeService.candidateProfile.next(data);
                this.storeService.updateVerifyPhone(data.verifiedPhone);
                return this.userService.fetchCandidateCV(data.id)
                  .pipe(
                    map(response => ({ ...response, profile: data })),
                  );
              }
              return throwError(() => new Error(message))
            }),
          )
          .subscribe({
            next: ({ status, data, }) => {
              if (status === ResponseStatus.success) {
                this.storeService.cv.next(data);
              }
            },
            error: error => this.helperService.callApiFailedHandler(error)
          })
      }

      if (roleId === ROLE.recruiter) {
        this.userService.fetchRecruiterProfile()
          .pipe(
            finalize(() => this.loading = false)
          )
          .subscribe({
            next: ({ status, data }) => {
              if (status === ResponseStatus.success) {
                this.storeService.recruiterProfile.next(data);
                this.storeService.updateVerifyPhone(data.verifiedPhone);
                this.userService.reloadRecruiterAccountVerify();
              }
            }
          })
      }
    } else {
      this.loading = false;
    }
  }

}

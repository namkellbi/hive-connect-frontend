import { Pipe, PipeTransform } from '@angular/core';
import { DEFAULT_COVER_URL, DEFAULT_AVATAR_URL } from '@constant/settings';
import { isBase64Image } from 'src/app/core/utils/resourceHandle';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'avatarConverter'
})
export class AvatarConverterPipe implements PipeTransform {

  transform(value: unknown, isCover: boolean = false): unknown {
    if (value instanceof ArrayBuffer || value instanceof File) {
      return value;
    }
    if (isBase64Image(value as string)) {
      return value;
    }
    if (!value) {
      return isCover ? DEFAULT_COVER_URL : DEFAULT_AVATAR_URL;
    }
    return `${environment.IMAGE_PATH}${value}`;
  }

}

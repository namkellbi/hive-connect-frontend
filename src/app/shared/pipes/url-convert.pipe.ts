import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'urlConvert'
})
export class UrlConvertPipe implements PipeTransform {

  transform(value: string): string {
    const regex = /(ftp|http|https)/;
    if (regex.test(value)) {
      return value;
    }
    return `https://${value}`;
  }

}

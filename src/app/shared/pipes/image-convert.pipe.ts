import { Pipe, PipeTransform } from '@angular/core';
import { DEFAULT_AVATAR_URL, DEFAULT_BANNER_URL, DEFAULT_COMPANY_URL, DEFAULT_COVER_URL } from '@constant/settings';
import { isBase64Image, validURL } from 'src/app/core/utils/resourceHandle';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'imageConvert'
})
export class ImageConvertPipe implements PipeTransform {

  transform(url: any, type?: 'cover' | 'banner' | 'company'): unknown {
    if (url instanceof ArrayBuffer || url instanceof File) {
      return url;
    }
    if (validURL(url)) {
      return url;
    } else if (isBase64Image(url)) {
      return url;
    } else if (!url) {
      return this.resolveDefault(type);
    } else {
      return `${environment.API_URL}${url.substring(1)}`;
    }
  }

  resolveDefault(type?: 'cover' | 'banner' | 'company') {
    switch (type) {
      case 'company': {
        return DEFAULT_COMPANY_URL;
      }
      case 'banner': {
        return DEFAULT_BANNER_URL;
      }
      case 'cover': {
        return DEFAULT_COVER_URL
      }
      default: {
        return DEFAULT_AVATAR_URL;
      }
    }
  }
}

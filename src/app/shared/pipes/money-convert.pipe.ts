import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneyConvert'
})
export class MoneyConvertPipe implements PipeTransform {

  divider = [1000, 1000000, 1000000000];
  mapper = ['nghìn', 'triệu', 'tỷ'];

  transform(value: any): unknown {
    if (isNaN(value)) {
      return value;
    }
    const raw = +value;
    const { index, g } = this.getDivider(raw);
    return `${Number((raw / g).toFixed(1))} ${this.mapper[index]}`;
  }
  getDivider(value: number) {
    let index = 0;
    let g = 1;
    this.divider.forEach((_value, _index) => {
      if (value >= _value) {
        g = _value;
        index = _index;
      }
    });
    return {
      g,
      index
    };
  }
}

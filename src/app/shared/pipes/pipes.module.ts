import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageConvertPipe } from './image-convert.pipe';
import { AvatarConverterPipe } from './avatar-converter.pipe';
import { SafePipe } from './safe.pipe';
import { MoneyConvertPipe } from './money-convert.pipe';
import { UrlConvertPipe } from './url-convert.pipe';



@NgModule({
  declarations: [
    ImageConvertPipe,
    AvatarConverterPipe,
    SafePipe,
    MoneyConvertPipe,
    UrlConvertPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ImageConvertPipe,
    AvatarConverterPipe,
    SafePipe,
    MoneyConvertPipe,
    UrlConvertPipe
  ]
})
export class PipesModule { }

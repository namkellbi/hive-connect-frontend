import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from './pipes/pipes.module';
import { DirectivesModule } from './directives/directives.module';
import { BootstrapModule } from './bootstrap/bootstrap.module';
import { FontawesomeModule } from './fontawesome/fontawesome.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PipesModule,
    DirectivesModule,
    BootstrapModule,
    FontawesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule.forRoot(),
    NgxSkeletonLoaderModule.forRoot()
  ],
  exports: [
    PipesModule,
    DirectivesModule,
    BootstrapModule,
    FontawesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    NgxSkeletonLoaderModule
  ]
})
export class SharedModule { }

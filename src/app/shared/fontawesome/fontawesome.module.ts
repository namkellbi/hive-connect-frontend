import { NgModule } from '@angular/core';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faGoogle, fab, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faCalendar, faEnvelope, far } from '@fortawesome/free-regular-svg-icons';
import { fas, faCloudMoon, faGlobe, faTablet, faHome } from '@fortawesome/free-solid-svg-icons';
@NgModule({
  declarations: [],
  imports: [
    FontAwesomeModule
  ],
  exports: [
    FontAwesomeModule
  ]
})
export class FontawesomeModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faGoogle, faLinkedinIn, faCloudMoon, faCalendar, faEnvelope, faGlobe, faTablet, faHome,);
    library.addIconPacks(far, fas, fab)
  }
}

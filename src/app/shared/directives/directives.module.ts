import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonLoadingDirective } from './button-loading.directive';
import { FileHandleDirective } from './file-handle.directive';
import { InputValidateClassDirective } from './input-validate-class.directive';



@NgModule({
  declarations: [
    FileHandleDirective,
    InputValidateClassDirective,
    ButtonLoadingDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FileHandleDirective,
    InputValidateClassDirective,
    ButtonLoadingDirective,
  ]
})
export class DirectivesModule { }

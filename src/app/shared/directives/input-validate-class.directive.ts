import { AfterContentChecked, AfterViewChecked, Directive, ElementRef, OnInit, Optional } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[hcInputValidateClass]',
})
export class InputValidateClassDirective implements OnInit, AfterViewChecked, AfterContentChecked {
  prevClass = '';
  constructor(
    @Optional() private control: NgControl,
    private el: ElementRef<HTMLElement>
  ) { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    if (this.control.touched || this.control.dirty) {
      let className = 'is-valid';
      this.prevClass && this.el.nativeElement.classList.remove(this.prevClass);
      if (this.control.errors) {
        className = 'is-invalid';
      }
      this.el.nativeElement.classList.toggle(className);
      this.prevClass = className;
    }
  }
  ngAfterContentChecked(): void {
    //Called after every check of the component's or directive's content.
    //Add 'implements AfterContentChecked' to the class.

  }
}

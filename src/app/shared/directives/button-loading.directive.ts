import { AfterViewInit, Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[hcButtonLoading]'
})
export class ButtonLoadingDirective implements OnChanges, AfterViewInit {
  @Input() loading = false;
  btnContent: string = '';
  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.btnContent != '') {
      this.el.nativeElement.disabled = this.loading;
      const parent = this.el.nativeElement as HTMLElement
      if (this.loading) {
        Array.from(parent.children).forEach(item => item.classList.add('d-none'));
        parent.insertAdjacentHTML('beforeend', ' <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      } else {
        Array.from(parent.children).forEach(item => item.classList.remove('d-none'));
        parent.querySelector('.spinner-border')?.remove();
      }
    } else {
      this.btnContent = this.el.nativeElement.outerText || this.el.nativeElement.innerHTML;
    }
  }

  ngAfterViewInit(): void {
    this.btnContent = this.el.nativeElement.outerText || this.el.nativeElement.innerHTML;
  }
}

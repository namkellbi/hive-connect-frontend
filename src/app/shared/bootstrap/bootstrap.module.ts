import { NgModule } from '@angular/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  declarations: [],
  imports: [
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    ToastrModule.forRoot({
      closeButton: true,
      // disableTimeOut: true,
      positionClass: 'toast-bottom-right'
      // toastComponent: ToastComponent
    }),
    CarouselModule.forRoot(),
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    AccordionModule.forRoot(),
    PopoverModule.forRoot(),
    TabsModule.forRoot(),
  ],
  exports: [
    BsDatepickerModule,
    ModalModule,
    ToastrModule,
    CarouselModule,
    BsDropdownModule,
    PaginationModule,
    AccordionModule,
    PopoverModule,
    TabsModule
  ]
})
export class BootstrapModule {

}

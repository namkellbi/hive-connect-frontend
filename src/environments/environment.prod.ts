export const environment = {
  production: true,
  PREFIX: 'HC_',
  API_URL: 'http://localhost:8080/',
  GOOGLE_CLIENT_ID: '358900689029-cv6u8i2mrr1logtmcvg272vah25d34hl.apps.googleusercontent.com',
  SOCKET_URL: 'http://localhost:2002',
  SOCKET_ROOT: "/socket/"
};

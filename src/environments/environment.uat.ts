export const environment = {
  production: false,
  PREFIX: 'HC_',
  API_URL: 'https://hive.gogitek.online/',
  API_V1: `api/v1/`,
  GOOGLE_CLIENT_ID: '358900689029-cv6u8i2mrr1logtmcvg272vah25d34hl.apps.googleusercontent.com',
  IMAGE_PATH: 'https://hive.gogitek.online/api/v1/files/get-image/',
  SOCKET_URL: 'http://localhost:2002',
  SOCKET_ROOT: "/socket/"
};